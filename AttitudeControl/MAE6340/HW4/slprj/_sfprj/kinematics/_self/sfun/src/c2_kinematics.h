#ifndef __c2_kinematics_h__
#define __c2_kinematics_h__

/* Type Definitions */
#ifndef typedef_SFc2_kinematicsInstanceStruct
#define typedef_SFc2_kinematicsInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c2_sfEvent;
  boolean_T c2_doneDoubleBufferReInit;
  uint8_T c2_is_active_c2_kinematics;
  void *c2_fEmlrtCtx;
  real_T (*c2_wbi_B)[3];
  real_T (*c2_A_BI)[9];
  real_T (*c2_Adot_BI)[9];
} SFc2_kinematicsInstanceStruct;

#endif                                 /*typedef_SFc2_kinematicsInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c2_kinematics_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c2_kinematics_get_check_sum(mxArray *plhs[]);
extern void c2_kinematics_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
