%% Attitude Dynamics Assignment
% Name:Brian Merrell

%% Preliminaries
% This cleans all variables and sets the format to display more digits.
clearvars
close all
clc
format long

%% Addpath to Attitude Representations Folder
addpath('C:\Users\a02292333\Documents\MAE6340\AttitudeRepresentation')
addpath('C:\Users\a02292333\Documents\MAE6340\Properties')

%% Mass Properties
mass_properties;

%% Initial Satellite Attitude
% Create an initial attitude quaternion by rotating 45 degrees about an
% axis that points equally in the x, y, and z directions ([1;1;1]).
% Remember to make the axis a unit vector.
e = [1;0;0]; e = e/norm(e);
q0_BI = e2q(e,0);

%% 
% Simscape's initial attitude defined by a DCM.
A0_BI = q2A(q0_BI);
A0_IB = A0_BI';


%% Initial Satellite Angular Velocity
% wbi0_B = [5*pi/180;-10*pi/180;15*pi/180]; % rad/s
wbi0_B = [0;0;0];

%%
% Will need to project the initial angular velocity to the principle frame
A_BP = A_PB';
q_PB = A2q(A_PB);
q_PB = q_PB/norm(q_PB);

%% Initial Angular Velocity in Principal Frame
% Transform the initial angular velocity to the principal frame for
% individualtransfer functions
wbi0_P = A_PB*wbi0_B;

%% Demanded Position
e = [1;0;0]; e = e/norm(e);
qstar_BI = e2q(e,pi);

%% Parameters for design and simulation
% Estimated Control Time Delay
dt_delay = 0.01;

%% Inner Loop Control System Design
% The following section contains the calculations for the control system
% design. This includes Bode plots and the thresholds for marginal
% stability.

%%
% Transfer Function Variable 
s = tf('s');

%%
% Controller Delay
C = exp(-s*dt_delay);

%% 
% Plant for each decoupled axis
G1 = 1/J_C_P(1,1)/s;
G2 = 1/J_C_P(2,2)/s;
G3 = 1/J_C_P(3,3)/s;
G = [G1; G2; G3];
%%
% Bode Plot of Principal Open Loop Plants
figure;
bode(G1,G2,G3,{1,1000},'--');
legend('G1','G2','G3');
title('Inner Open Loop Plant Bode Plot');

%%
% Natural Crossover Frequencies
w1 = 1/J_C_P(1,1)/2/pi; % hz
w2 = 1/J_C_P(2,2)/2/pi; % hz
w3 = 1/J_C_P(3,3)/2/pi; % hz

%%
% Pade Approximation
[num, den] = pade(dt_delay,8);
C_pade = tf(num, den);

%%
% Contorl system designer configuration
config = sisoinit(1);
config.G.value = G2;
config.C.value = C_pade;
% controlSystemDesigner(config);

%%
% Calcuation for the crossover frequency for 65 degree phase margin
w_crossover = 25*pi/180/dt_delay;
display(w_crossover/2/pi,'Inner Loop Crossover frequency (Hz)')

%%
% Calculate Proportional Control Gains. These gains are caculated so the
% crossover frequency for all three axes are the same. Making all axes
% respond numerically identical. This method did not limit the demanded
% torque.
Kd1 = 1/bode(G1*C,w_crossover);
display(Kd1, 'Inner Loop Proportional Gain 1')
Kd2 = 1/bode(G2*C,w_crossover);
display(Kd2, 'Inner Loop Proportional Gain 2')
Kd3 = 1/bode(G3*C,w_crossover);
display(Kd3, 'Inner Loop Proportional Gain 3')
Kd = diag([Kd1;Kd2;Kd3]);

%%
% Bode Plot of Principal Open Loop Plants. These open loop plots behave the
% same because the dynamics are proportionally the same. And the
% proportional gain made them numerically identical.
figure;
bode(Kd1*C*G1,Kd2*C*G2,Kd3*C*G3,{1,1000},'--');
legend('Kd1*C*G1','Kd2*C*G2','Kd3*C*G3')
title('Controlled Inner Open Loop Bode Plot')

%%
% Calculate Gain and Phase Margins. Only one axis was displayed because all
% three have the same Gain and Phase Margins.
[Gm1, Pm1] = margin(Kd1*C*G1);
display(mag2db(Gm1),'Inner Gain Margin (dB)');
display(Pm1, 'Inner Phase Margin (degrees)');

[Gm2, Pm2] = margin(Kd2*C*G2);
% display(mag2db(Gm2),'Inner Gain Margin 2 (dB)');
% display(Pm2, 'Inner Phase Margin 2 (degrees)');
% 
[Gm3, Pm3] = margin(Kd3*C*G3);
% display(mag2db(Gm3),'Inner Gain Margin 3 (dB)');
% display(Pm3, 'Inner Phase Margin 3 (degrees)');

%%
% Calculate addition delay to drive the system marginally unstable
dt_unstable = Pm1*pi/180/w_crossover;
display(dt_unstable, 'Additional Allowable Delay (s)');

%%
% Calculate allowable inertial uncertianty
J_unstable = diag(J_C_P./[Gm1; Gm2; Gm3]);
display(J_unstable, 'Inertial caused Marginal Stability');

%% Inner Closed Loop Characteristics
% This section contains the Bode plots and behavior of the closed loop.
% Since the three axes have the same crossover frequency and plant
% dynamics the results in this section is the same for all three axes.

%%
% Inner Closed Loop transfer function calculation
CLTF1 = feedback(Kd1*C*G1,1);
CLTF2 = feedback(Kd2*C*G2,1);
CLTF3 = feedback(Kd3*C*G3,1);


%%
% Inner Closed Loop Bode Plot for each axis
figure;
bode(CLTF1, CLTF2, CLTF3, {1,1000}, '--');
title('Inner Closed Loop Bode Plot');
legend('CLTF1', 'CLTF2', 'CLTF3');

% Reminder: These are the same because the crossover freqency was designed 
% as the same for all axes.

%%
% Inner Loop 3dB Bandwidth Calculation
display(bandwidth(CLTF1)/2/pi, 'Closed Loop 3dB Bandwidth 1 (Hz)');

% Reminder: These are the same because the crossover freqency was designed 
% as the same for all axes.

%%
% Inner Loop Rise time and percent overshoot
stepinfo1 = stepinfo(CLTF1);
display(stepinfo1, 'Step Input Characteristics 1');

% Reminder: These are the same because the crossover freqency 
% was designed as the same for all axes.


%% Open Outer Loop Control System Design
% The following section contains the calculations for the control system
% design. This includes Open Loop Bode plots.

%%
% Contorl system designer configuration
config = sisoinit(6);
config.G1.value = G1;
config.C1.value = 516.148*(s+.05)/s/(s+50);
config.C2.value = Kd1*C_pade;
config.G2.value = 1/s;
config.OL1.View ={'bode'};
config.OL2.View = {};
% controlSystemDesigner(config);

%% 
% Outer Loop Controler Transfer Function. Taken from the Control System
% Desingner.
% Proportional Gain - Changes the response time
% This value used because of its fast response time and low overshoot. 
k = 516.148;
%%
% Zero Placement - Turns off the Integrator to buy back phase
% The Zero placement determines how well the system rejects a disturbance
% torque. The closer it is the 0 the worse the system will reject a
% disturbance. The large this value is the better the system will reject,
% but the system will also have a longer settling time. The zero placement
% also affects the tracking of a ramp the same way it rejects distrubance.
z = 0.05; % 0.05 seemed to have a very good disturbance rejection and 
          % minimal impact on the settling time.
%%
% Pole Placement - Place outside of the bandwidth
% Rejects large frequencies. Starts to slow response when p~<30
p = 50; 

%%
% Outer Loop Controller
Co = k*(s+z)/s/(s+p);

%%
% Outer Open Loop Transfer Function
% Only one of the axis inner loops are needed since they are all the same.
OLTF = Co*CLTF1/s;

%%
% Open Loop Outer Bode Plot.
% Almost resembles proportionally the inner loop
figure;
bode(OLTF, {1,1000})
title('Controlled Outer Open Loop Bode Plot');

%%
% Outer Loop Crossover, Gain Margin and Phase Margin
[Gm,Pm,outer_w_crossGain,outer_w_crossover] = margin(OLTF);
display(outer_w_crossover/2/pi, 'Outer Loop Crossover Freqency (Hz)')
display(mag2db(Gm), 'Outer Loop Gain Margin (dB)');
display(Pm, 'Outer Loop Phase Margin (degrees)');

% Recall Inner Crossover, Gain Margin and Phase Margin:
display(w_crossover/2/pi, 'Inner Loop Crossover Frequency (Hz)')
display(mag2db(Gm1),'Inner Gain Margin (dB)');
display(Pm1, 'Inner Phase Margin (degrees)');

%% Outer Closed Loop Characteristics
% Outer Closed Loop Transfer Function and Bode Plot. This also almost
% resembles the inner closed loop bode plot, but with a smaller bandwidth.
CLTF = feedback(OLTF,1);

figure;
bode(CLTF, {1,1000});
title('Outer Closed Loop Bode Plot');

%%
% Outer Closed Loop Bandwidth
% The Outer Loop is much slower than the Inner Loop.
display(bandwidth(CLTF)/2/pi, 'Outer Close Loop 3 dB Bandwith (Hz)');
% Recall the Inner Loop Bandwidth:
display(bandwidth(CLTF1)/2/pi, 'Inner Close Loop 3 dB Bandwith (Hz)');

%% Step Response
% Step Response of Outer Closed Loop
stepinfo_outer = stepinfo(CLTF);
disp(stepinfo_outer);
figure;
step(CLTF);
%% 
% Step response of Simulink/Simscape Model
sim("AngleControl",.5);
%% 
% The step response behaves quickly with little to no steady state error.
% As it was designed. It behaves almost like a linear system. I am guessing
% the bump at the peak of the response is due to nonlinearity.

%% Ramp Response
% Ramp Response of Outer Closed Loop
time = 0:.025:80;
rampSlope = 1;
ramp = time*rampSlope;
[a,b] = lsim(CLTF,ramp,time);
%%
% The control system has a small error when tracking a ramp, but this goes
% away with time as can be seen in the second plot.
figure;
plot(b,a,time,ramp);
axis([0,5,0,5]);
xlabel('Time');
ylabel('Degrees');
title('Initial Ramp Response');

figure;
plot(b,a,time,ramp);
axis([30,35,30,35]);
xlabel('Time');
ylabel('Degrees');
title('Steady State Ramp Response');
%%
% Ramp Input of Simulink/Simscape Model
% sim("AngleControlRamp",10);

%%
% The controller can track a ramp, it just takes a more time to do
% so then the step response. It seems that it take about 80-100 seconds to
% track the ramp perfectly, which is about the same as the disturbance
% rejection time. This is no coincidence.

%% Sinewave Response
% Sine Input of Simulink/Simscape Model
w_bandwidth = bandwidth(CLTF);
% sim("AngleControlSinewave",1);

%%
% The Sine response at the bandwidth frequency behaves as expected. It is
% almost completely out of phase and never reaches the desired angle. This
% is because this frequency is on the edge of our capability to control.
% For frequency smaller than this the preformance improves drastically.
%% Disturbance rejection 
% Changing the desired attitude to be constant
qstar_BI = e2q(e,0);
%%
% Constant Disturbance
TdTF = s*(s+p)/(J_C_P(1)*s^3*(s+p)+Kd(1)*s^2*(s+p)+Kd(1)*k*(s+z)); % Hand Calc
figure;
step(TdTF);

% sim("AngleControlDisturbance",100);
%%
% The constant disturbance rejection looks exactly as expected. It takes
% about 80-100 seconds to reject the disturbance complete. A faster
% rejection would require a much longer settling time.
%%
% Disturbance at 3dB frequency
w_disturbance = bandwidth(CLTF);
% sim("AngleControlDisturbance2",5);

%%
% The Sinewave disturbance is  a little unsettling because the controller
% does not seem to respond at all. It just lets the disturbance do what it
% wants to the body. 

%% Comments
% Overall I think this controller will preform well. It handles every input
% well and only lacks in rejecting a sinusoidal disturbance.


