main;

q0_PI = qXp(q_PB, q0_BI);
qstar_PI = qXp(q_PB, qstar_BI);
qe = qXp(qstar_PI, qT(q0_PI));

[e, theta] = q2e(qe);

a_max = safety*hrdotmax/max(max(J_C_P));
w_max = safety*hrmax/max(max(J_C_P));
t_max = w_max/a_max;
angle_max = 0.5*a_max*t_max^2;

if angle_max > theta/2
    t1 = sqrt(theta/a_max);
    angle1 = 0.5*a_max*t1^2;
    w1 = a_max*t1;
    t2 = t1;
    angle2 = angle1;
    w2 = w1;
    t3 = t2+t1;
    
else
    angle_coast = theta - 2*angle_max;
    t_coast = angle_coast/w_max;
    
    t1 = t_max;
    angle1 = 0.5*a_max*t1^2;
    w1 = a_max*t1;
    
    t2 = t1 + t_coast;
    angle2 = angle1+w1*t_coast;
    w2 = w1;
    
    t3 = t1 + t2;
    
end

dt = .0001;
t = 0:dt:t3;
if t(end) < t3
    t(end+1) = t(end) + dt;
end
    

for i = 1:length(t)
    if t(i) >= t3
        a(i) = 0;
        w(i) = 0;
        angle(i) = theta;
        
    elseif t(i) >= t2
        a(i) = -a_max;
        w(i) = w2-a_max*(t(i)-t2);
        angle(i) = angle2 + w2*(t(i) -t2) - 0.5*a_max*(t(i)-t2)^2;
        
    elseif t(i) >= t1
        a(i) = 0;
        w(i) = w1;
        angle(i) = angle1 + w1*(t(i)-t1);
        
    else
        a(i) = a_max;
        w(i) = a_max*t(i);
        angle(i) = 0.5*a_max*t(i)^2;
        
    end
        
    astar_P(1:3,i) = a(i)*e;
    wstar_P(1:3,i) = w(i)*e;
    
    q_small = [cos(angle(i)/2);sin(angle(i)/2*e)];
    q_small = q_small/norm(q_small);
    qstar_PI(1:4,i) = qXp(q_small,q0_PI);
    
end

figure;
plot(t,astar_P)
title('Acceleration')

figure;
plot(t,wstar_P)
title('Angular Velocity');

figure;
plot(t,angle);
title('Theta');











