%% Final Assignment
% Name:Brian Merrell

%% Preliminaries
% This cleans all variables and sets the format to display more digits.
clearvars
close all
clc
format long

%% Addpath to Attitude Representations Folder
addpath('../AttitudeRepresentation')
addpath('../Properties')

%% Mass Properties
mass_properties;

%% Reaction Wheels
wrmax = 6500*2*pi/60; % rad/s -- Max angular velocity
hrmax = 15e-3; % kg*m^2/s Max angular momentum
Jr = hrmax/wrmax; % kg*m^2 Inertia
hrdotmax = 6e-3;
wrdotmax = hrdotmax/Jr; % kg*m^2/s^2 Max angular accel
wn = 2*pi*10; % Wheel natural frequency
zeta = sqrt(2)/2; % wheel damping ratio
safety = 0.5;

%% Initial Satellite Attitude
% Initial attitude quaternion of the B frame relative to the I frame.
q0_BI = [1; 0; 0; 0];

%% 
% Simscape's initial attitude defined by a DCM.
A0_BI = q2A(q0_BI);
A0_IB = A0_BI';


%% 
% Initial Satellite Angular Velocity
wbi0_B = [0;0;0];

%%
% Will need to project the initial angular velocity to the principle frame
A_BP = A_PB';
q_PB = A2q(A_PB);
q_PB = q_PB/norm(q_PB);

%%
% Initial Angular Velocity in Principal Frame
wbi0_P = A_PB*wbi0_B;

%%
% Initial Reaction Wheel Angular Velocity
wr0_R = [0; 0; 0]; % rad/s

%% Trapezoidal Profile Parameters
q0_PI = qXp(q_PB, q0_BI);
a_max = safety*hrdotmax/max(max(J_C_P));
w_max = safety*hrmax/max(max(J_C_P));
t_max = w_max/a_max;
angle_max = 0.5*a_max*t_max^2;

%% Demanded Position
e = [1;2;3]; e = e/norm(e);
qstar_BI = e2q(e,180*pi/180);

%% Parameters for design and simulation
% Estimated Control Time Delay
dt_delay = 0.01;

%% Inner Loop Control System Design
% The following section contains the calculations for the control system
% design. This includes Bode plots and the thresholds for marginal
% stability.

%%
% Transfer Function Variable 
s = tf('s');

%%
% Controller Delay
C = exp(-s*dt_delay);

%% 
% Plant for each decoupled axis
G1 = 1/J_C_P(1,1)/s;
G2 = 1/J_C_P(2,2)/s;
G3 = 1/J_C_P(3,3)/s;
G = [G1; G2; G3];

%%
% Bode Plot of Principal Open Loop Plants
figure;
bode(G1,G2,G3,{1,1000},'--');
legend('G1','G2','G3');
title('Inner Open Loop Plant Bode Plot');

%%
% Reaction Wheel Transfer Function
Gr = wn^2/(s^2 + 2*wn*zeta*s + wn^2);

figure;
bode(Gr);
title('Reaction Wheel Closed Loop Bode');

figure;
step(Gr);
title('Reaction Wheel Closed Loop Step Response');

%%
% Pade Approximation
[num, den] = pade(dt_delay,8);
C_pade = tf(num, den);

%%
% Contorl system designer configuration
config = sisoinit(1);
config.G.value = G1;
config.C.value = Gr*C_pade;
% controlSystemDesigner(config);

%%
% Natural Crossover Frequencies
w_crossover = 2*pi*2.35; % Observed from Control System Designer
display(w_crossover/2/pi,'Inner Loop Crossover frequency (Hz)')

%%
% Calculating Proportional Control Gains. These gains are caculated so the
% crossover frequency for all three axes are the same. Making all axes
% respond numerically identical. This method did not limit the demanded
% torque.
Kd1 = 1/bode(G1*Gr*C,w_crossover);
display(Kd1, 'Inner Loop Proportional Gain 1')
Kd2 = 1/bode(G2*Gr*C,w_crossover);
display(Kd2, 'Inner Loop Proportional Gain 2')
Kd3 = 1/bode(G3*Gr*C,w_crossover);
display(Kd3, 'Inner Loop Proportional Gain 3')
Kd = diag([Kd1;Kd2;Kd3]);

%%
% Bode Plot of Principal Open Loop Plants. These open loop plots behave the
% same because the dynamics are proportionally the same. And the
% proportional gain made them numerically identical.
figure;
bode(Kd1*C*Gr*G1,Kd2*C*Gr*G2,Kd3*C*Gr*G3,{1,1000},'--');
legend('Kd1*C*G1','Kd2*C*G2','Kd3*C*G3')
title('Controlled Inner Open Loop Bode Plot')

%%
% Calculate Gain and Phase Margins. Only one axis was displayed because all
% three have the same Gain and Phase Margins.
[Gm1, Pm1] = margin(Kd1*C*G1);
display(mag2db(Gm1),'Inner Gain Margin (dB)');
display(Pm1, 'Inner Phase Margin (degrees)');

[Gm2, Pm2] = margin(Kd2*C*G2);

[Gm3, Pm3] = margin(Kd3*C*G3);


%%
% Calculate addition delay to drive the system marginally unstable
dt_unstable = Pm1*pi/180/w_crossover;
display(dt_unstable, 'Additional Allowable Delay (s)');

%%
% Calculate allowable inertial uncertianty
J_unstable = diag(J_C_P./[Gm1; Gm2; Gm3]);
display(J_unstable, 'Inertial caused Marginal Stability');

%% Inner Closed Loop Characteristics
% This section contains the Bode plots and behavior of the closed loop.
% Since the three axes have the same crossover frequency and plant
% dynamics the results in this section is the same for all three axes.

%%
% Inner Closed Loop transfer function calculation
CLTF1 = feedback(Kd1*C*G1,1);
CLTF2 = feedback(Kd2*C*G2,1);
CLTF3 = feedback(Kd3*C*G3,1);


%%
% Inner Closed Loop Bode Plot for each axis
figure;
bode(CLTF1, CLTF2, CLTF3, {1,1000}, '--');
title('Inner Closed Loop Bode Plot');
legend('CLTF1', 'CLTF2', 'CLTF3');

%%
% Reminder: These are the same because the crossover freqency was designed 
% as the same for all axes.

%%
% Inner Loop 3dB Bandwidth Calculation
display(bandwidth(CLTF1)/2/pi, 'Inner Closed Loop 3dB Bandwidth 1 (Hz)');
%%
% Reminder: These are the same because the crossover freqency was designed 
% as the same for all axes.

%%
% Inner Loop Rise time and percent overshoot
stepinfo1 = stepinfo(CLTF1);
display(stepinfo1, 'Inner Loop Step Input Characteristics 1');
%%
% Reminder: These are the same because the crossover freqency 
% was designed as the same for all axes.

%% Open Outer Loop Control System Design
% The following section contains the calculations for the control system
% design. This includes Open Loop Bode plots.

%%
% Outer Controller Proportional Gain
Kp = 1/bode(CLTF1*1/s,2*pi);

figure;
bode(Kp*CLTF1*G, {1,1000})
title('Proportional Outer Open Loop Bode Plot');
[Gm, Pm] = margin(Kp*CLTF1*1/s);
display(mag2db(Gm),'Proportional Outer Loop Gain Margin (dB)');
display(Pm, 'Proportional Outer Loop Phase Margin (degrees)');

%%
% Contorl system designer configuration
config = sisoinit(6);
config.G1.value = G1;
config.C1.value = 516.148*(s+.05)/s/(s+50);
config.C2.value = Kd1*C_pade;
config.G2.value = 1/s;
config.OL1.View ={'bode'};
config.OL2.View = {};
% controlSystemDesigner(config);

%% 
% Outer Loop Controler Transfer Function. Taken from the Control System
% Desingner.
%%
% Proportional Gain - Changes the response time.
% This value used because of its fast response time and low overshoot. 
k = 295.65;

%%
% Zero Placement - Turns off the Integrator to buy back phase.
% The Zero placement determines how well the system rejects a disturbance
% torque. The closer it is the 0 the worse the system will reject a
% disturbance. The larger this value is the better the system will reject disturbance,
% but the system will also have a longer settling time. The zero placement
% also affects the tracking of a ramp the same way it rejects distrubance.
z = 0.03; % 0.03 seemed to have a very good disturbance rejection and 
          % minimal impact on the settling time.
%%
% Pole Placement - Place outside of the bandwidth
% Rejects large frequencies. Starts to slow response when p~<30
p = 50; 

%%
% Outer Loop Controller
Co = k*(s+z)/s/(s+p);

%%
% Outer Open Loop Transfer Function
% Only one of the axis inner loops are needed since they are all the same.
OLTF = Co*CLTF1/s;

%%
% Open Loop Outer Bode Plot.
% Almost resembles proportionally the inner loop
figure;
bode(OLTF, {1,1000})
title('Controlled Outer Open Loop Bode Plot');

%%
% Outer Loop Crossover, Gain Margin and Phase Margin
[Gm,Pm,outer_w_crossGain,outer_w_crossover] = margin(OLTF);
display(outer_w_crossover/2/pi, 'Outer Loop Crossover Freqency (Hz)')
display(mag2db(Gm), 'Outer Loop Gain Margin (dB)');
display(Pm, 'Outer Loop Phase Margin (degrees)');

% Recall Inner Crossover, Gain Margin and Phase Margin:
display(w_crossover/2/pi, 'Inner Loop Crossover Frequency (Hz)')
display(mag2db(Gm1),'Inner Gain Margin (dB)');
display(Pm1, 'Inner Phase Margin (degrees)');

%% Outer Closed Loop Characteristics
% Outer Closed Loop Transfer Function and Bode Plot. This also almost
% resembles the inner closed loop bode plot, but with a smaller bandwidth.
CLTF = feedback(OLTF,1);

figure;
bode(CLTF, {1,1000});
title('Outer Closed Loop Bode Plot');

%%
% Outer Closed Loop Bandwidth
% The Outer Loop is much slower than the Inner Loop.
display(bandwidth(CLTF)/2/pi, 'Outer Close Loop 3 dB Bandwith (Hz)');
% Recall the Inner Loop Bandwidth:
display(bandwidth(CLTF1)/2/pi, 'Inner Close Loop 3 dB Bandwith (Hz)');

%% Step Response
% Step Response of Outer Closed Loop
stepinfo_outer = stepinfo(CLTF);
disp(stepinfo_outer);
figure;
step(CLTF);
%% Step Response Lead/Lag Control w/ Satruated Reaction Wheels
close all
sim("AttitudeControl",60);

%%
% The error on this step response is all across the board. There are about
% 7 oscillatory cycles before it finally settles. As for the saturations
% the controller doesn't saturate it's angular momentum often becuase once
% it maxes out to commands it in the opposite direction. This is in no way
% efficient.

%% Step Response Anti-Integrator Wind Up Control
sim("WindUp",60);

%%
% The settling time on this system is about 30% faster. However, the
% maximum error is actually greater. There is less oscillation, but it is
% still very much present. This system is also not efficient.

%% Trapezoidal Profile Response
sim("TrapezoidalProfile",20);

%%
% Once again there is about a 30% improvement in settling time. This is due
% to completely removing the oscilatory behavoir. By profiling the
% commanded attiude into a more realistic signal the oscilations have been
% removed and the error is two orders of magnitude smaller. However, with
% the current error the actual attitude will result in having a
% satellite pointing completely off target.

%% Trapezoidal Profile Feed Forward Response
sim("FeedForward",20);

%%
% The only major improvement here is the reduction of the attitude error.
% This reduction however is two orders of magnitude, which will allow the
% satellite to point accurately to it's target.


