clc
format long
% A satellite is in orbit with the following position and velocity vectors 
% as projected to an inertial coordinate frame:
r_I = [6878.06; -28.28; -12.28]; % km
v_I = [0.031; 5.38; 5.38]; % km/s

% It is desired to construct a local level frame (L) such that:
% zL points toward the Earth
% yL points in opposite direction of orbit angular momentum vector
% xL completes the right-handed frame (zL = xL X yL)
zL_I = -r_I/norm(r_I)

yL_I = cross(-r_I,v_I)/norm(cross(r_I,v_I))

xL_I = cross(yL_I,zL_I)

% What is the direction cosine matrix that can project a vector represented 
% in the I Frame to the L Frame (A_LI)?
A_LI = [xL_I';
        yL_I';
        zL_I']

% If a vector v1 has the following representation in the I Frame:
v1_I = [1;0;0];

% What is it's representation in the L Frame (v1_L)?
v1_L = A_LI*v1_I

% If a vector v2 has has the following representation in the L Frame:
v2_L = [1;0;0];

% What is it's representation in the I Frame (v2_I)?
v2_I = A_LI'*v2_L 