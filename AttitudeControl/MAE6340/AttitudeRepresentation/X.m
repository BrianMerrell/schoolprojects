function [ vX ] = X( v )
if ismatrix(v)
% Check if input is a [3,1] Matrix
    if size(v) == [3,1]
% X Calculate a Cross Product Matrix from a Vector
% Eq 2.55 from text
        vX = [0 -v(3) v(2);
              v(3) 0 -v(1);
              -v(2) v(1) 0];
    else
% Flag an Error if incorrect size
        msg = 'Incorrect size Matrix: Input Size = ';
        msg = strcat(msg,mat2str(size(v)));
        error(msg)
    end
else
% Flag Error if input isn't a Matrix
    msg = 'Variable not a matrix: Input Type = ';
    msg = strcat(msg,class(v));
    error(msg)
end
end

