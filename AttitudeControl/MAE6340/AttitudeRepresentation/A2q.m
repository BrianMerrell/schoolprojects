function [q] = A2q( A )
%  A2Q Convert a Direction Cosine Matrix to Quaternion.
%   Detailed explanation goes here
[e, angle] = A2e(A);
q = e2q(e,angle);


end