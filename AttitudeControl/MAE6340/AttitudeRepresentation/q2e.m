function [ e, angle ] = q2e( q )
%Q2E Convert a Quaternion to a Euler/Angle Axis Representation
%   Detailed explanation goes here
mag = norm(q(2:end));

if mag > 1
    mag = 1;
end

if mag == 0.0
    e = [1;0;0];
    angle = 0;
else
    e = q(2:end)/mag;
    angle = 2*asin(mag);
    if angle < 0
        angle = -angle;
        e = -e;
    end
end           
end

