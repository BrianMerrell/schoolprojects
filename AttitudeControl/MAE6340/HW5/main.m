%% Attitude Dynamics Assignment
% Name:Brian Merrell

%% Preliminaries
% This cleans all variables and sets the format to display more digits.
clearvars
close all
clc
format long

%% Addpath to Attitude Representations Folder
addpath('C:\Users\a02292333\Documents\MAE6340\AttitudeRepresentation')

%% Initial Satellite Attitude
% Create an initial attitude quaternion by rotating 45 degrees about an
% axis that points equally in the x, y, and z directions ([1;1;1]).
% Remember to make the axis a unit vector.
e = [1;1;1];
e = e/norm(e);
q0_BI = e2q(e,45*pi/180);
q0_BI = q0_BI/norm(q0_BI);
disp('q0_BI = ');
disp(q0_BI);

%%
% The initial attitude in the Simscape simulation can be specified by a
% Body to Inerital direction cosine matrix.  Convert q0_BI to A0_IB;
A0_BI = q2A(q0_BI);
A0_IB = A0_BI';
disp('A0_IB = ');
disp(A0_IB);

%% Initial Satellite Angular Velocity
% The same inital angular velocity will be used as in the attitude
% kinematics assignment.
wbi0_B = [5*pi/180;-10*pi/180;15*pi/180]; % rad/s

%% Satellite Geometry
% The position vector from the origin of the B frame to the link between
% the structure and the deployable.
rlb_B = [0.16364; -0.03142; -0.02136]; % m

%% Satellite Mass Properties
% The mass properties of the satellite will be extracted from the Solid
% Works Assembly.  The mass properties of the structure and deployable will
% be extracted separately and combined in this script.

%% Mass properties of Satellite Structure

%%
% Mass of satellite structure
ms = 3.21000120; % kg

%%
% Position vector to center of mass of satellite structure relative to
% origin of the B frame projected to the basis vectors of the B frame.
rsb_B = [0.16439646; -0.08187950; 0.09592783]; % m

%%
% Moment of inertia tensor of satellite structure about the origin of the B
% frame projected to the basis vectors of the B frame.
Jxx = 0.08636806;
Jxy = -0.04320451;
Jxz = 0.05087768;
Jyy = 0.15161865;
Jyz = -0.02530725;
Jzz = 0.11387280;

Js_B_B = [Jxx -Jxy -Jxz
          -Jxy Jyy -Jyz
          -Jxz -Jyz Jzz]; % kg*m^2
disp('Js_B_B = ');
disp(Js_B_B);      

%%
% Moment of inertia tensor of the satellite structure about the center of
% mass of the structure projected to the basis vectors of the B frame.
Js_S_B = Js_B_B - ms*(rsb_B'*rsb_B*eye(3)-rsb_B*rsb_B'); % kg*m^2
disp('Js_S_B = ');
disp(Js_S_B);  

%%
% Extract MOI and POI into vectors for SimScape.
Ms_S_B = diag(Js_S_B); % Moments of inertia of the structure
Ps_S_B = [Js_S_B(2,3);
           Js_S_B(1,3);
           Js_S_B(1,2)]; % Products of inertia of the structure

%% Mass properties of Deployable Panel

%%
% Mass of deployable panel
md = 0.05290847; % kg

%%
% Position vector to center of mass of deployable panel relative to
% origin of the B frame projected to the basis vectors of the B frame.
rdb_B = [0.16364135; 0.08256652; -0.13481916]; % m

%%
% Moment of inertia tensor of deployable panel about the origin of the B
% frame projected to the basis vectors of the B frame.
Jxx = 0.00177584;
Jxy = 0.00071486;
Jxz = -0.00116727;
Jyy = 0.00263292;
Jyz = -0.00081569;
Jzz = 0.00203193;

Jd_B_B = [Jxx -Jxy -Jxz
          -Jxy Jyy -Jyz
          -Jxz -Jyz Jzz]; % kg*m^2
disp('Jd_B_B = ');
disp(Jd_B_B);

%%
% Moment of inertia tensor of the deployable panel about the center of
% mass of the deployable projected to the basis vectors of the B frame.
Jd_D_B = Jd_B_B - md*(rdb_B'*rdb_B*eye(3)-rdb_B*rdb_B'); % kg*m^2
disp('Jd_D_B = ');
disp(Jd_D_B);  

%%
% Extract MOI and POI into vectors for SimScape.
Md_D_B = diag(Jd_D_B); % Moments of inertia of the deployable
Pd_D_B = [Jd_D_B(2,3);
           Jd_D_B(1,3);
           Jd_D_B(1,2)]; % Products of inertia of the deployable  

%% Combined Mass Properties

%%
% Position vector to combined center of mass of the satellite structure
% and deployable panel relative to the origin of the B frame projected to
% the basis vectors of the B frame.
rcb_B = (ms*rsb_B + md*rdb_B)/(ms+md); % m
disp('rcb_B = ');
disp(rcb_B);

%%
% Moment of inertia tensor of the satellite structure about combined center
% of mass projected to the basis vectors of the B frame.
rsc_B = rsb_B - rcb_B;

Js_C_B = Js_S_B + ms*(rsc_B'*rsc_B*eye(3)-rsc_B*rsc_B'); % kg*m^2
disp('Js_C_B = ');
disp(Js_C_B); 

%%
% Moment of inertia tensor of the deployable panel about combined center of
% mass projected to the basis vectors of the B frame.
rdc_B = rdb_B - rcb_B;

Jd_C_B = Jd_D_B + md*(rdc_B'*rdc_B*eye(3)-rdc_B*rdc_B'); % kg*m^2
disp('Jd_C_B = ');
disp(Jd_C_B); 

%%
% Combined moment of inertia tensor about the combined center of mass
% projected to the basis vectors of the B frame.
J_C_B = Js_C_B + Jd_C_B; % kg*m^2
disp('J_C_B = ');
disp(J_C_B); 


%% Principal Moment of Inertia Tensor
% Calculate the direction cosine matrix that projects a vector from the B
% frame to the principal frame of the combined moment of inertia tensor
% about the combined center of mass.
[e_B,e] = eig(J_C_B);
e1 = e_B(:,1);
e2 = e_B(:,2);
e3 = cross(e1,e2);

A_PB = [e1';e2';e3'];
disp('A_PB = ');
disp(A_PB); 

%%
% Combined moment of inertia tensor about the combined center of mass
% projected to the basis vectors of the principal frame. Use A_PB to
% project J_C_B to the principal frame.
J_C_P = A_PB * J_C_B * A_PB'; % kg*m^2
disp('J_C_P = ');
disp(J_C_P); 

%% Run the Simulation
sim('attitude_dynamics',60)

%% Plot output of simulation
figure
plot(tout,q_BI,'b')
hold on
plot(tout,q_BI_simscape,'r--')
title('Euler''s Equation and Simscape q^B_I');
xlabel('seconds');
ylabel('q^B_I');

figure
plot(tout,q_BI - q_BI_simscape)
title('Difference between q^B_I');
xlabel('seconds');
ylabel('q^B_I');

figure
plot(tout,wbi_B,'b')
hold on
plot(tout,wbi_B_simscape,'r--')
title('Euler''s Equation and Simscape \omega_B_I^B');
xlabel('seconds');
ylabel('radians/s');

figure
plot(tout,wbi_B-wbi_B_simscape)
title('Difference between \omega_B_I^B');
xlabel('seconds');
ylabel('radians/s');