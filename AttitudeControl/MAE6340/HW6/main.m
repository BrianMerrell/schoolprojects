%% Attitude Dynamics Assignment
% Name:Brian Merrell

%% Preliminaries
% This cleans all variables and sets the format to display more digits.
clearvars
close all
clc
format long

%% Addpath to Attitude Representations Folder
addpath('C:\Users\a02292333\Documents\MAE6340\AttitudeRepresentation')
addpath('C:\Users\a02292333\Documents\MAE6340\Properties')

%% Mass Properties
mass_properties;

%% Initial Satellite Attitude
% Create an initial attitude quaternion by rotating 45 degrees about an
% axis that points equally in the x, y, and z directions ([1;1;1]).
% Remember to make the axis a unit vector.
e = [1;1;1];
e = e/norm(e);
q0_BI = e2q(e,45*pi/180);
q0_BI = q0_BI/norm(q0_BI);

%% 
% Simscape's initial attitude defined by a DCM.
A0_BI = q2A(q0_BI);
A0_IB = A0_BI';

%% Initial Satellite Angular Velocity
wbi0_B = [5*pi/180;-10*pi/180;15*pi/180]; % rad/s
% wbi0_B = [0;0;0];

%%
% Will need to project the initial angular velocity to the principle frame
A_BP = A_PB';

%% Initial Angular Velocity in Principal Frame
% Transform the initial angular velocity to the principal frame for
% individualtransfer functions
wbi0_P = A_PB*wbi0_B;

%% Parameters for design and simulation
% Estimated Control Time Delay
dt_delay = 0.01;

% Commanded Angular Velocityof the B frame relative to the I frame 
% projected to the B frame
wbistar_B = [1;1;1]; % rad/sec
wbistar_P = A_PB*wbistar_B; % rad/sec

%% Control System Design
% The following section contains the calculations for the control system
% design. This includes Bode plots and the thresholds for marginal
% stability.
% ctrlpref

%%
% Transfer Function Variable 
s = tf('s');

%%
% Controller Delay
C = exp(-s*dt_delay);

%% 
% Plant for each decoupled axis
G1 = 1/J_C_P(1,1)/s;
G2 = 1/J_C_P(2,2)/s;
G3 = 1/J_C_P(3,3)/s;
G = [G1; G2; G3];
%%
% Bode Plot of Principal Open Loop Plants
figure
bode(G1,G2,G3,{1,1000},'--');
legend('G1','G2','G3')
title('Principal Open Loop Plant Bode Plot')

%%
% Natural Crossover Frequencies
w1 = 1/J_C_P(1,1)/2/pi; % hz
% disp(w1);
w2 = 1/J_C_P(2,2)/2/pi; % hz
% disp(w2);
w3 = 1/J_C_P(3,3)/2/pi; % hz
% disp(w3);

%%
% Pade Approximation
[num, den] = pade(dt_delay,8);
C_pade = tf(num, den);

%%
% Contorl system designer configuration
% config = sisoinit(1);
% config.G.value = G2;
% config.C.value = C_pade;
% controlSystemDesigner(config);

%%
% Calcuation for the crossover frequency for 65 degree phase margin
w_crossover = 25*pi/180/dt_delay;
display(w_crossover/2/pi,'Crossover frequency (Hz)')

%%
% Calculate Proportional Control Gains
Kd1 = 1/bode(G1*C,w_crossover);
display(Kd1, 'Proportional Gain 1')
Kd2 = 1/bode(G2*C,w_crossover);
display(Kd2, 'Proportional Gain 2')
Kd3 = 1/bode(G3*C,w_crossover);
display(Kd3, 'Proportional Gain 3')
Kd = diag([Kd1,Kd2,Kd3]);

%%
% Bode Plot of Principal Open Loop Plants
figure
bode(Kd1*C*G1,Kd2*C*G2,Kd3*C*G3,{1,1000},'--');
legend('Kd1*C*G1','Kd2*C*G2','Kd3*C*G3')
title('Open Loop Bode Plot w/ Controller Delay')

%%
% Calculate Gain and Phase Margins
[Gm1, Pm1] = margin(Kd1*C*G1);
display(mag2db(Gm1),'Gain Margin 1 (dB)');
display(Pm1, 'Phase Margin 1 (degrees)');

[Gm2, Pm2] = margin(Kd2*C*G2);
display(mag2db(Gm2),'Gain Margin 2 (dB)');
display(Pm2, 'Phase Margin 2 (degrees)');

[Gm3, Pm3] = margin(Kd3*C*G3);
display(mag2db(Gm3),'Gain Margin 3 (dB)');
display(Pm3, 'Phase Margin 3 (degrees)');

%%
% Calculate addition delay to drive the system marginally unstable
dt_unstable = Pm1*pi/180/w_crossover;
display(dt_unstable, 'Additional Allowable Delay (s)');

%%
% Calculate allowable inertial uncertianty
J_unstable = diag(J_C_P./[Gm1; Gm2; Gm3]);
display(J_unstable, 'Inertial caused Marginal Stability');

%% Closed Loop Characteristics
% This section contains the Bode plots and behavoir of the closed loop.
% Since the three axis have the same crossover frequency and plant
% dynamics the results in this section is the same for all three axis.

%%
% Closed Loop transfer function calculation
CLTF1 = feedback(Kd1*C*G1,1);
CLTF2 = feedback(Kd2*C*G2,1);
CLTF3 = feedback(Kd3*C*G3,1);


%%
% Closed Loop Bode Plot for each axis
figure
bode(CLTF1, CLTF2, CLTF3, {1,1000}, '--');
title('Closed Loop Bode Plot')
legend('CLTF1', 'CLTF2', 'CLTF3')

% Reminder: These are the same because the crossover freqency was designed 
% as the same for all axis.

%%
% 3dB Bandwidth Calculation
display(bandwidth(CLTF1)/2/pi, 'Closed Loop 3dB Bandwidth 1 (Hz)')

% Reminder: These are the same because the crossover freqency was designed 
% as the same for all axis.

%%
% Rise time and percent overshoot
stepinfo1 = stepinfo(CLTF1);
display(stepinfo1, 'Step Input Characteristics 1')

% Reminder: These are the same because the crossover freqency was designed 
% as the same for all axis.

%% Run the Simulation
sim("VelocityControl",1);

%% Plot output of simulation
% Results from Simscape Model

figure
plot(Result_Simscape(:,1), Result_Simscape(:,2))
hold on
grid on
plot(Result_Simscape(:,1), Result_Simscape(:,3))
plot(Result_Simscape(:,1), Result_Simscape(:,4))
title('Step Response Simscape model')
legend('Axis 1', 'Axis 2', 'Axis 3', 'Location', 'southeast')

%%
% Results from the SISO Models
figure
plot(Result_Siso(:,1), Result_Siso(:,2))
hold on
grid on
plot(Result_Siso(:,1), Result_Siso(:,3))
plot(Result_Siso(:,1), Result_Siso(:,4))
title('Step Response SISO model')
legend('Axis 1', 'Axis 2', 'Axis 3', 'Location', 'southeast')

%% Comments
% It was interesting to see the results of setting the three different axis
% to the same crossover frequency. I am guessing they behave exactly the
% same because the plant dynamics are proportionally the same. And that
% axis 2 and 3 seem to already attenuate the oscillating  response due to
% our controller time delay.

