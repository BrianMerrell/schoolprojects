function [ sun ] = eclipse(r_I, rEarth, s_I)

if r_I'*s_I/norm(s_I) < -sqrt(norm(r_I)^2 - rEarth^2)   
    sun = 0;
else
    sun = 1;
end

end