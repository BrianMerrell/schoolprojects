function [r_I, v_I] = orbel2rv(a,e,theta,omega,I,w)

uEarth = 3.986004418e14;

p = a*(1-e^2);

r = p/(1+e*cos(theta));

r_F = [r*cos(theta);
       r*sin(theta);
       0];
v_F = [-sqrt(uEarth/p*sin(theta));
       sqrt(uEarth/p*(e+cos(theta)));
       0];
   
A_IF = [cos(omega)*cos(w)-cos(I)*sin(omega)*sin(w) cos(w)*sin(omega) + cos(omega)*cos(I)*sin(w) sin(I)*sin(w);
        -cos(omega)*sin(w)-cos(I)*cos(w)*sin(omega) cos(omega)*cos(I)*cos(w) - sin(omega)*sin(w) cos(w)*sin(I);
        sin(omega)*sin(I) -cos(omega)*sin(I) cos(I)];

r_I = A_IF'*r_F;
v_I = A_IF'*v_F;
end
