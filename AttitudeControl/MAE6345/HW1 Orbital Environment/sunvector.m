function [ s_I ] = sunvector( JD )

TUT1 = (JD-2451545)/36525;

phiSun = 280.46 + 36000.771*TUT1;

Msun = 357.5277233 + 35999.05034*TUT1;

phiEliptic = phiSun + 1.914666471*sind(Msun) + 0.019994643*sind(2*Msun);

e = 23.439291 - 0.0130042*TUT1;

s_I = [cosd(phiEliptic);
        cosd(e)*sind(phiEliptic);
        sind(e)*sind(phiEliptic)];


rsun = 1.000140612 - 0.016708617*cosd(Msun) - 0.000139589*cosd(2*Msun);


% convert AU to m
s_I = rsun*149597870700*s_I;

end