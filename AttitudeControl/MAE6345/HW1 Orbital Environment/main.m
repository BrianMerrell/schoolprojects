%% HW1
% Name: Brian Merrell
%% Prelims
clearvars
close all
clc
format long

%% Earth Parameters
uEarth = 3.986004418e14; % Earth Gravitational param eq. C.104a (m^3/s^2)
rEarth = 6.378137e6; % Earth Mean Radis at equator eq. C.104b (m)

%% Orbit Elements
a = rEarth + 500e3; % orbit at 500 km (m)
e = 0; % Circular
theta = 0*pi/180; % True Anomaly (rads)
omega = 0*pi/180; % Right Ascension of Ascending Node (rads)
I = 45*pi/180; % Inclination (rads)
w = 0*pi/180; % Argument of Perigee (rads)

%% Initial Position and Velocity
[r0_I, v0_I] = orbel2rv(a,e,theta,omega,I,w);
display(r0_I,'Initial Position');
display(v0_I,'Initial Velocity');


%% Simulation
sim('simulation',3600);

%% Results
figure;
plot(r_I)
title('Position Vector')
display(r_I.data(end,:),'Final Position');

figure;
plot(v_I)
title('Velocity Vector')
display(v_I.data(:,end)','Final Velocity Vector');

figure;
plot(B_I)
title('Magnetic field Vector')
display(B_I.data(1,:),'Initial Magnetic field');

figure;
plot(s_I)
title('Sun Vector')
display(s_I.data(1,:),'Initial Sun Vector');

figure;
plot(sun)
title('Illumination Status')

figure;
plot(rho)
title('Density')
display(rho.data(1,:),'Initial Density');



