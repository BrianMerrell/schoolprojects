function [ phi, theta, psi ] = A2zyx( A )
%A2zyx Convert a DCM to a ZYX (phi, theta, psi) Euler Angle Sequence
% Using table B.2 on page 362 of the text, the angles can be derived as
% follows
theta = asin(-A(1,3));
phi = atan2(A(1,2),A(1,1));
psi = atan2(A(2,3),A(3,3));
end

