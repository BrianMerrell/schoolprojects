function [ e, angle ] = A2e( A )
%A2E Convert a DCM to Euler Axis/Angle Representation
%   Detailed explanation goes here

% Hint: Use eq 2.113, 2.114, and 2.115.  Handle angle = 0 and angle = pi.
% When angle = pi normalize the column with the largest magnitude
% When angle = 0 choose any unit vector for e (e is undefined)

% Eq 2.113
angle = acos((trace(A)-1)/2);

if angle == 0
    
    e = [0;0;1];
    
elseif angle == pi
        
        % Eq 2.115
        eeT = (A + eye(3))/2;
        
        [val, i] = max([norm(eeT(:,1)),norm(eeT(:,2)),norm(eeT(:,3))]);
        
        if val == 0
            
            e = [0;0;0];
            
        else
            
            e = eeT(:,i)/val;
            
        end
        
else
    
    % Eq 2.114
    e = 1/(2*sin(angle))*[A(2,3)-A(3,2);A(3,1)-A(1,3);A(1,2)-A(2,1)];
        
end

if angle < 0
    
    angle = -angle;
    e = -e;
    
end

end

