%% Discrete Time Inner Controller
% Name: Brian Merrell

%% Preliminaries
% This cleans all variables and sets the format to display more digits.
clearvars
close all
clc
format long


%% Addpath to Attitude Representations Folder
addpath('../AttitudeRepresentation')
addpath('../Properties')

%% Mass Properties
mass_properties;

A_BP = A_PB';

%% Parameters for the design and simulation
dt_delay = 0.014; % seconds

dt_sample = 0.01;
dt_sim = 0.001;

wbi0_B = [0;0;0];

wbistar_B = [10;10;10]*pi/180;

q0_BI = [1;0;0;0];

A0_BI = q2A(q0_BI);
A0_IB = A0_BI';

%% Inner Loop Controller Design
s = tf('s');
G1 = 1/(J_C_P(1,1)*s);
G2 = 1/(J_C_P(2,2)*s);
G3 = 1/(J_C_P(3,3)*s);

G1d = c2d(G1,dt_delay,'zoh');
G2d = c2d(G2,dt_delay,'zoh');
G3d = c2d(G3,dt_delay,'zoh');

z = tf('z',dt_delay);


Kd1 = 1/bode(z^-1*G1d,2*pi*3.3);
Kd2 = 1/bode(z^-1*G2d,2*pi*3.3);
Kd3 = 1/bode(z^-1*G3d,2*pi*3.3);

Kd = diag([Kd1;Kd2;Kd3]);

C1d = Kd1*z^-1;
C2d = Kd2*z^-1;
C3d = Kd3*z^-1;

figure

margin(C1d*G1d)

figure

margin(C2d*G2d)

figure

margin(C3d*G3d)

% sim('discrete',0.2);
% 
% figure
% plot(wbi_B*180/pi);
% 
% hold on
% 
% plot(w123_B*180/pi,'--');
