/** 
 *  @file    threadFunction.cpp
 *  @author  Bryan Bingham
 *  @date    02/23/2018
 *  @version 1.0.3
 *  
 *  @brief An empty function that can be called by a thread
 *
 */

#include "thread.hpp"
#include "example.hpp"
#include "controller.hpp"
#include <cmath>



void* controller(void *pArg)
{
  // Push Cleanup Handler to cleanup thread
  pthread_cleanup_push(&Thread::cleanup,pArg);

  const double A_PB[3][3] = { {0.008031085662960, -0.073245535352114, 0.997281601762539},
                              {0.012127083086920, 0.997247555958304, 0.073145375732109},
                              {-0.999894212163906, 0.011506680067277, 0.008897235242039} };

  const double A_BP[3][3] = { {0.008031085662960, 0.012127083086920, -0.999894212163906},
                              {-0.073245535352114, 0.997247555958304, 0.011506680067277},
                              { 0.997281601762539, 0.073145375732109, 0.008897235242039} };

  const double J_C_B[3][3] = { {0.039940934349015, 0.000002112428714, -0.000264398339716},
                               {0.000002112428714, 0.038351474350995, 0.002296039610626},
                               {-0.000264398339716, 0.002296039610626, 0.007260057763141} };

  const double Kd[3] = {0.146477528026054, 0.795890217170406, 0.825299266487932};

  double wbi_B[3];

  double wbi_P[3];

  double wbistar_B[3];

  double wbistar_P[3];

  double Tc_B[3];

  double Tc_P[3];

  double h_B[3];

  double T1_B[3];

  double T_B[3];

  double t;





  // Code goes here...
  while(true)
  {
      controllerMutex.wait();

      controllerMutex.unlock();

      receiveMutex.lock();

      wbi_B[0] = receiveBuffer[0];
      wbi_B[1] = receiveBuffer[1];
      wbi_B[2] = receiveBuffer[2];

      wbistar_B[0] = receiveBuffer[3];
      wbistar_B[1] = receiveBuffer[4];
      wbistar_B[2] = receiveBuffer[5];

      t = receiveBuffer[6];

      receiveMutex.unlock();

      matMult(A_PB,wbistar_B,wbistar_P);

      matMult(A_PB,wbi_B,wbi_P);

      Tc_P[0] = Kd[0]*(wbistar_P[0] - wbi_P[0]);
      Tc_P[1] = Kd[1]*(wbistar_P[1] - wbi_P[1]);
      Tc_P[2] = Kd[2]*(wbistar_P[2] - wbi_P[2]);

      matMult(A_BP,Tc_P,Tc_B);

      matMult(J_C_B,wbi_B,h_B);

      cross(wbi_B,h_B,T1_B);

      T_B[0] = Tc_B[0] + T1_B[0];
      T_B[1] = Tc_B[1] + T1_B[1];
      T_B[2] = Tc_B[2] + T1_B[2];

      sendMutex.lock();

      sendBuffer[0] = T_B[0];
      sendBuffer[1] = T_B[1];
      sendBuffer[2] = T_B[2];
      sendBuffer[3] = t;

      sendMutex.unlock();

      sendMutex.signal();

      std::cout << t << std::endl;

  }


  // Run clean up handler to cleanup thread (TLPI pg 676)
  pthread_cleanup_pop(1);
}


void matMult(const double (&A)[3][3], const double (&b)[3], double (&vout)[3])
{
    vout[0] = A[0][0]*b[0] + A[0][1]*b[1] + A[0][2]*b[2];
    vout[1] = A[1][0]*b[0] + A[1][1]*b[1] + A[1][2]*b[2];
    vout[2] = A[2][0]*b[0] + A[2][1]*b[1] + A[2][2]*b[2];
}

void cross(const double (&a)[3], const double (&b)[3], double (&vout)[3])
{
    vout[0] = a[1]*b[2] - a[2]*b[1];
    vout[1] = a[2]*b[0] - a[0]*b[2];
    vout[2] = a[0]*b[1] - a[1]*b[0];
}
