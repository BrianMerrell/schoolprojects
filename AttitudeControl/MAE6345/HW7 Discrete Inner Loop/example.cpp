#include <iostream>
#include "example.hpp"
#include "receiveUdp.hpp"
#include "threadManager.hpp"

Mutex sendMutex;
double sendBuffer[SNDNUM];

Mutex receiveMutex;
double receiveBuffer[RCVNUM];

Mutex controllerMutex;

int main(int argc, char* argv[])
{

    ThreadManager manager;

    std::cout << manager.create(receiveFunction) << std::endl;
    std::cout << manager.create(sendFunction) << std::endl;
    std::cout << manager.create(controller) << std::endl;

    std::cout << manager.wait() << std::endl;

    

    return EXIT_STATUS;
};

// g++ -std=c++11 example.cpp arguments.cpp -o example
// ./example 123 asd
