#include "arguments.hpp"
#include <sstream>

Arguments::Arguments(int argc, char* argv[])
{
    doubleArray = new double[argc];
    m_size = argc;

    for (int i = 0; i < argc; i++)
    {
        std::stringstream streamTown;
        
        streamTown << argv[i];

        streamTown >> doubleArray[i];

        stringStore.push_back(std::string(argv[i]));
    }

}

Arguments::~Arguments()
{
    delete[] doubleArray;

}

std::string Arguments::getString(int n)
{
    return stringStore[n];
}

double Arguments::getDouble(int n)
{
    return doubleArray[n];
}

int Arguments::getSize()
{
    return m_size;
}