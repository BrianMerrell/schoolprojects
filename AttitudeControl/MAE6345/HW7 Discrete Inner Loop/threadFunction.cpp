/** 
 *  @file    threadFunction.cpp
 *  @author  Bryan Bingham
 *  @date    02/23/2018
 *  @version 1.0.3
 *  
 *  @brief An empty function that can be called by a thread
 *
 */

#include "thread.hpp"

void* threadFunction(void *pArg)
{
  // Push Cleanup Handler to cleanup thread
  pthread_cleanup_push(&Thread::cleanup,pArg);

  // Code goes here...

  // Run clean up handler to cleanup thread (TLPI pg 676)
  pthread_cleanup_pop(1);
}