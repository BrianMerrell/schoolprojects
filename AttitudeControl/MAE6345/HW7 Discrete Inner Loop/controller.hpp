#ifndef CONTROLLER_HPP_
#define CONTROLLER_HPP_

void matMult(const double (&A)[3][3], const double (&b)[3], double (&vout)[3]);

void cross(const double (&a)[3], const double (&b)[3], double (&vout)[3]);

#endif
