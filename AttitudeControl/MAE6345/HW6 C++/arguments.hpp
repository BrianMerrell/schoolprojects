#ifndef ARGUMENTS_HPP_
#define ARGUMENTS_HPP_

#include <vector>
#include <string>

class Arguments
{
    public:
        Arguments(int argc, char* argv[]);
        ~Arguments();
        
        double getDouble(int n);
        std::string getString(int n);
        int getSize();

    private:
        double* doubleArray;
        std::vector<std::string> stringStore;
        int m_size;

};

#endif