#include <iostream>
#include "arguments.hpp"

int main(int argc, char* argv[])
{
    Arguments myArgs(argc,argv);

    for (int i = 0; i < argc; i++)
    {
        std::cout << "String " << i << " is " << myArgs.getString(i) <<std::endl;
        std::cout << "Double " << i << " is " << myArgs.getDouble(i) <<std::endl;
    }
    

    return EXIT_SUCCESS;
};

// g++ -std=c++11 example.cpp arguments.cpp -o example
// ./example 123 asd