#ifndef CONTROLLER_HPP_
#define CONTROLLER_HPP_

void matMult(const double (&A)[3][3], const double (&b)[3], double (&vout)[3]);

void cross(const double (&a)[3], const double (&b)[3], double (&vout)[3]);

void qXp(const double (&q)[4], const double (&p)[4], double (&vout)[4]);

void qT(const double (&q)[4], double (&var)[4]);

void q2theta(const double (&q)[4], double(&var)[3]);


#endif
