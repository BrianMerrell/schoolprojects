%% Preliminaries
% This cleans all variables and sets the format to display more digits.
clearvars
close all
clc
format long


%% Addpath to Attitude Representations Folder
addpath('../AttitudeRepresentation')
addpath('../Properties')


%% qXp

syms q0 q1 q2 q3 p0 p1 p2 p3 real
q = [q0;q1;q2;q3];

p = [p0;p1;p2;p3];

x = qXp(q,p);

q = qT(q)


