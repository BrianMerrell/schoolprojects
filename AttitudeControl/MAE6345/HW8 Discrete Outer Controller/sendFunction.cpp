/** 
 *  @file    threadFunction.cpp
 *  @author  Bryan Bingham
 *  @date    02/23/2018
 *  @version 1.0.3
 *  
 *  @brief An empty function that can be called by a thread
 *
 */

#include "thread.hpp"
#include "example.hpp"
#include "sendUdp.hpp"

void* sendFunction(void *pArg)
{
  // Push Cleanup Handler to cleanup thread
  pthread_cleanup_push(&Thread::cleanup,pArg);

  // Code goes here...
  SendUdp sendUdp;

  sendUdp.initialize(LOCALIP, REMOTEIP, SNDPORT);

  double localBuffer[RCVNUM];

  while(true)
  {
      sendMutex.wait();

      std::memcpy(localBuffer, sendBuffer, sizeof(localBuffer));

      sendMutex.unlock();

      sendUdp.sendDoubles(sendBuffer, SNDNUM);


  }





  // Run clean up handler to cleanup thread (TLPI pg 676)
  pthread_cleanup_pop(1);
}
