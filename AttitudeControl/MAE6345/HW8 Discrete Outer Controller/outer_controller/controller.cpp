/** 
 *  @file    threadFunction.cpp
 *  @author  Bryan Bingham
 *  @date    02/23/2018
 *  @version 1.0.3
 *  
 *  @brief An empty function that can be called by a thread
 *
 */

#include "thread.hpp"
#include "example.hpp"
#include "controller.hpp"
#include <cmath>



void* controller(void *pArg)
{
  // Push Cleanup Handler to cleanup thread
  pthread_cleanup_push(&Thread::cleanup,pArg);

  const double A_PB[3][3] = { {0.008031085662960, -0.073245535352114, 0.997281601762539},
                              {0.012127083086920, 0.997247555958304, 0.073145375732109},
                              {-0.999894212163906, 0.011506680067277, 0.008897235242039} };

  const double A_BP[3][3] = { {0.008031085662960, 0.012127083086920, -0.999894212163906},
                              {-0.073245535352114, 0.997247555958304, 0.011506680067277},
                              { 0.997281601762539, 0.073145375732109, 0.008897235242039} };

  const double q_PB[4] = {0.709608320988294,0.0217157458000863,-0.703619079305931,-0.0300773736418892};                              

  const double J_C_B[3][3] = { {0.039940934349015, 0.000002112428714, -0.000264398339716},
                               {0.000002112428714, 0.038351474350995, 0.002296039610626},
                               {-0.000264398339716, 0.002296039610626, 0.007260057763141} };

  const double Kd[3] = {0.146477528026054, 0.795890217170406, 0.825299266487932};
//   const double Kd[3] = {0.245964941826709, 1.336458183073283, 1.385841833944817};

  const double K = 4.809900265324782;

  const double zd = 0.999937170120808;

  const double pd = 0.284609543336029;

  double wbi_B[3];

  double wbi_P[3];

  double wbistar_P[3];
  double wbistar_P1[3] = {0,0,0};
  double wbistar_P2[3] = {0,0,0};

  double Tc_B[3];

  double Tc_P[3];

  double h_B[3];

  double T1_B[3];

  double T_B[3];

  double q_BI[4];

  double q_PI[4];
  double q_IP[4];

  double qstar_BI[4];

  double qstar_PI[4];

  double qe[4];

  double theta[3];
  double theta1[3] = {0,0,0};
  double theta2[3] = {0,0,0};

  
  double t;





  // Code goes here...
  while(true)
  {
      controllerMutex.wait();

      controllerMutex.unlock();

      receiveMutex.lock();

      wbi_B[0] = receiveBuffer[0];
      wbi_B[1] = receiveBuffer[1];
      wbi_B[2] = receiveBuffer[2];

      q_BI[0] = receiveBuffer[3];
      q_BI[1] = receiveBuffer[4];
      q_BI[2] = receiveBuffer[5];
      q_BI[3] = receiveBuffer[6];

      qstar_BI[0] = receiveBuffer[7];
      qstar_BI[1] = receiveBuffer[8];
      qstar_BI[2] = receiveBuffer[9];
      qstar_BI[3] = receiveBuffer[10];

      t = receiveBuffer[11];

      receiveMutex.unlock();

      matMult(A_PB,wbi_B,wbi_P);

      qXp(q_PB,q_BI,q_PI);

      qXp(q_PB,qstar_BI,qstar_PI);

      qT(q_PI,q_IP);

      qXp(qstar_PI,q_IP,qe);

      q2theta(qe,theta);

      wbistar_P[0] = (1.0+pd)*wbistar_P1[0] - pd*wbistar_P2[0] + K*theta1[0] - K*zd*theta2[0];
      wbistar_P[1] = (1.0+pd)*wbistar_P1[1] - pd*wbistar_P2[1] + K*theta1[1] - K*zd*theta2[1];
      wbistar_P[2] = (1.0+pd)*wbistar_P1[2] - pd*wbistar_P2[2] + K*theta1[2] - K*zd*theta2[2];

      Tc_P[0] = Kd[0]*(wbistar_P[0] - wbi_P[0]);
      Tc_P[1] = Kd[1]*(wbistar_P[1] - wbi_P[1]);
      Tc_P[2] = Kd[2]*(wbistar_P[2] - wbi_P[2]);

      matMult(A_BP,Tc_P,Tc_B);

      matMult(J_C_B,wbi_B,h_B);

      cross(wbi_B,h_B,T1_B);

      T_B[0] = Tc_B[0] + T1_B[0];
      T_B[1] = Tc_B[1] + T1_B[1];
      T_B[2] = Tc_B[2] + T1_B[2];
      
    //   std::cout << "T_B[0] = " << T_B[0] << std::endl;
    //   std::cout << "T_B[1] = " << T_B[1] << std::endl;
    //   std::cout << "T_B[2] = " << T_B[2] << std::endl;

      sendMutex.lock();

      sendBuffer[0] = T_B[0];
      sendBuffer[1] = T_B[1];
      sendBuffer[2] = T_B[2];
      sendBuffer[3] = t;

      sendMutex.unlock();
      
      sendMutex.signal();

      std::memcpy(wbistar_P2,wbistar_P1,sizeof(wbistar_P2));
      std::memcpy(wbistar_P1,wbistar_P,sizeof(wbistar_P1));
      std::memcpy(theta2,theta1,sizeof(theta2));
      std::memcpy(theta1,theta,sizeof(theta1));

    //   std::cout << t << std::endl;

  }


  // Run clean up handler to cleanup thread (TLPI pg 676)
  pthread_cleanup_pop(1);
}


void matMult(const double (&A)[3][3], const double (&b)[3], double (&vout)[3])
{
    vout[0] = A[0][0]*b[0] + A[0][1]*b[1] + A[0][2]*b[2];
    vout[1] = A[1][0]*b[0] + A[1][1]*b[1] + A[1][2]*b[2];
    vout[2] = A[2][0]*b[0] + A[2][1]*b[1] + A[2][2]*b[2];
}

void cross(const double (&a)[3], const double (&b)[3], double (&vout)[3])
{
    vout[0] = a[1]*b[2] - a[2]*b[1];
    vout[1] = a[2]*b[0] - a[0]*b[2];
    vout[2] = a[0]*b[1] - a[1]*b[0];
}

void qXp(const double (&q)[4], const double (&p)[4], double (&var)[4])
{
    var[0] = p[0]*q[0] - p[1]*q[1] - p[2]*q[2] - p[3]*q[3];
    var[1] = p[0]*q[1] + p[1]*q[0] + p[2]*q[3] - p[3]*q[2];
    var[2] = p[0]*q[2] + p[2]*q[0] - p[1]*q[3] + p[3]*q[1];
    var[3] = p[0]*q[3] + p[1]*q[2] - p[2]*q[1] + p[3]*q[0];
}


void qT(const double (&q)[4], double (&var)[4])
{
    var[0] = q[0];
    var[1] = -q[1];
    var[2] = -q[2];
    var[3] = -q[3];
}

void q2theta(const double (&q)[4], double(&var)[3])
{
    double mag = sqrt(q[1]*q[1]+q[2]*q[2]+q[3]*q[3]);

    if (mag == 0 || q[0] == 0)
    {
        var[0] = 0;
        var[1] = 0;
        var[2] = 0;
    }
    else
    {
        double angle = 2*asin(mag);

        var[0] = angle*q[1]/mag;
        var[1] = angle*q[2]/mag;
        var[2] = angle*q[3]/mag;
    }
    
}