%% Discrete Time Outer Controller
% Name: Brian Merrell

%% Preliminaries
% This cleans all variables and sets the format to display more digits.
clearvars
close all
clc
format long


%% Addpath to Attitude Representations Folder
addpath('../AttitudeRepresentation')
addpath('../Properties')

%% Mass Properties
mass_properties;

A_BP = A_PB';

%% Parameters for the design and simulation
dt_delay = 0.019; % seconds

dt_sample = 0.01;
dt_sim = 0.001;

wbi0_B = [0;0;0];

wbistar_B = [10;10;10]*pi/180;

q0_BI = [1;0;0;0];

A0_BI = q2A(q0_BI);
A0_IB = A0_BI';

%% Inner Loop Controller Design
s = tf('s');
G1 = 1/(J_C_P(1,1)*s);
G2 = 1/(J_C_P(2,2)*s);
G3 = 1/(J_C_P(3,3)*s);

G1d = c2d(G1,dt_sample,'zoh');
G2d = c2d(G2,dt_sample,'zoh');
G3d = c2d(G3,dt_sample,'zoh');

z = tf('z',dt_sample);

C_delay = c2d(exp(-s*dt_delay),dt_sample,'zoh');

Kd1 = 1/bode(C_delay*G1d,2*pi*3.3);
Kd2 = 1/bode(C_delay*G2d,2*pi*3.3);
Kd3 = 1/bode(C_delay*G3d,2*pi*3.3);

Kd = diag([Kd1;Kd2;Kd3]);

C1d = Kd1*C_delay;
C2d = Kd2*C_delay;
C3d = Kd3*C_delay;

figure

margin(C1d*G1d)

CLTF1 = feedback(C1d*G1d,1);

%% Outer Loop Controller Design

G = 1/s;

Gd = c2d(G,dt_sample,'foh');


% Contorl system designer configuration
config = sisoinit(6);
config.G1.value = G1d;
config.C1.value = 1;
config.G2.value = Gd;
config.OL1.View = {'bode'};
config.OL2.View = {};
% controlSystemDesigner(config);


wp = 2*pi*20;
wz = 2*pi*.001;
w_crossover = 2*pi*1.07;

zd = exp(-wz*dt_sample);
pd = exp(-wp*dt_sample);

Cd = 1/(z-1)*(z-zd)/(z-pd);

K = 1/bode(Cd*CLTF1*Gd,w_crossover);

figure 
bode(Cd)

figure
margin(Cd*CLTF1*Gd);







