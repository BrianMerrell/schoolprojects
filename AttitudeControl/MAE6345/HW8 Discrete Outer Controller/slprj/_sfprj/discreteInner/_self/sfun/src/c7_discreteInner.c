/* Include files */

#include "discreteInner_sfun.h"
#include "c7_discreteInner.h"
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "discreteInner_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);
static const mxArray* sf_opaque_get_hover_data_for_msg(void *chartInstance,
  int32_T msgSSID);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c7_debug_family_names[4] = { "nargin", "nargout", "e", "q" };

static const char * c7_b_debug_family_names[5] = { "nargin", "nargout", "e",
  "angle", "q" };

/* Function Declarations */
static void initialize_c7_discreteInner(SFc7_discreteInnerInstanceStruct
  *chartInstance);
static void initialize_params_c7_discreteInner(SFc7_discreteInnerInstanceStruct *
  chartInstance);
static void enable_c7_discreteInner(SFc7_discreteInnerInstanceStruct
  *chartInstance);
static void disable_c7_discreteInner(SFc7_discreteInnerInstanceStruct
  *chartInstance);
static void c7_update_debugger_state_c7_discreteInner
  (SFc7_discreteInnerInstanceStruct *chartInstance);
static const mxArray *get_sim_state_c7_discreteInner
  (SFc7_discreteInnerInstanceStruct *chartInstance);
static void set_sim_state_c7_discreteInner(SFc7_discreteInnerInstanceStruct
  *chartInstance, const mxArray *c7_st);
static void finalize_c7_discreteInner(SFc7_discreteInnerInstanceStruct
  *chartInstance);
static void sf_gateway_c7_discreteInner(SFc7_discreteInnerInstanceStruct
  *chartInstance);
static void mdl_start_c7_discreteInner(SFc7_discreteInnerInstanceStruct
  *chartInstance);
static void initSimStructsc7_discreteInner(SFc7_discreteInnerInstanceStruct
  *chartInstance);
static void init_script_number_translation(uint32_T c7_machineNumber, uint32_T
  c7_chartNumber, uint32_T c7_instanceNumber);
static const mxArray *c7_sf_marshallOut(void *chartInstanceVoid, void *c7_inData);
static void c7_emlrt_marshallIn(SFc7_discreteInnerInstanceStruct *chartInstance,
  const mxArray *c7_b_q, const char_T *c7_identifier, real_T c7_y[4]);
static void c7_b_emlrt_marshallIn(SFc7_discreteInnerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId,
  real_T c7_y[4]);
static void c7_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData);
static const mxArray *c7_b_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData);
static real_T c7_c_emlrt_marshallIn(SFc7_discreteInnerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId);
static void c7_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData);
static const mxArray *c7_c_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData);
static void c7_d_emlrt_marshallIn(SFc7_discreteInnerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId,
  real_T c7_y[3]);
static void c7_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData);
static const mxArray *c7_d_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData);
static int32_T c7_e_emlrt_marshallIn(SFc7_discreteInnerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId);
static void c7_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData);
static uint8_T c7_f_emlrt_marshallIn(SFc7_discreteInnerInstanceStruct
  *chartInstance, const mxArray *c7_b_is_active_c7_discreteInner, const char_T
  *c7_identifier);
static uint8_T c7_g_emlrt_marshallIn(SFc7_discreteInnerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId);
static void init_dsm_address_info(SFc7_discreteInnerInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc7_discreteInnerInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c7_discreteInner(SFc7_discreteInnerInstanceStruct
  *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    initSimStructsc7_discreteInner(chartInstance);
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  chartInstance->c7_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c7_is_active_c7_discreteInner = 0U;
}

static void initialize_params_c7_discreteInner(SFc7_discreteInnerInstanceStruct *
  chartInstance)
{
  (void)chartInstance;
}

static void enable_c7_discreteInner(SFc7_discreteInnerInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c7_discreteInner(SFc7_discreteInnerInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c7_update_debugger_state_c7_discreteInner
  (SFc7_discreteInnerInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c7_discreteInner
  (SFc7_discreteInnerInstanceStruct *chartInstance)
{
  const mxArray *c7_st;
  const mxArray *c7_y = NULL;
  const mxArray *c7_b_y = NULL;
  uint8_T c7_hoistedGlobal;
  const mxArray *c7_c_y = NULL;
  c7_st = NULL;
  c7_st = NULL;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_createcellmatrix(2, 1), false);
  c7_b_y = NULL;
  sf_mex_assign(&c7_b_y, sf_mex_create("y", *chartInstance->c7_q, 0, 0U, 1U, 0U,
    1, 4), false);
  sf_mex_setcell(c7_y, 0, c7_b_y);
  c7_hoistedGlobal = chartInstance->c7_is_active_c7_discreteInner;
  c7_c_y = NULL;
  sf_mex_assign(&c7_c_y, sf_mex_create("y", &c7_hoistedGlobal, 3, 0U, 0U, 0U, 0),
                false);
  sf_mex_setcell(c7_y, 1, c7_c_y);
  sf_mex_assign(&c7_st, c7_y, false);
  return c7_st;
}

static void set_sim_state_c7_discreteInner(SFc7_discreteInnerInstanceStruct
  *chartInstance, const mxArray *c7_st)
{
  const mxArray *c7_u;
  real_T c7_dv0[4];
  int32_T c7_i0;
  chartInstance->c7_doneDoubleBufferReInit = true;
  c7_u = sf_mex_dup(c7_st);
  c7_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c7_u, 0)), "q",
                      c7_dv0);
  for (c7_i0 = 0; c7_i0 < 4; c7_i0++) {
    (*chartInstance->c7_q)[c7_i0] = c7_dv0[c7_i0];
  }

  chartInstance->c7_is_active_c7_discreteInner = c7_f_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c7_u, 1)),
     "is_active_c7_discreteInner");
  sf_mex_destroy(&c7_u);
  c7_update_debugger_state_c7_discreteInner(chartInstance);
  sf_mex_destroy(&c7_st);
}

static void finalize_c7_discreteInner(SFc7_discreteInnerInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c7_discreteInner(SFc7_discreteInnerInstanceStruct
  *chartInstance)
{
  int32_T c7_i1;
  int32_T c7_i2;
  uint32_T c7_debug_family_var_map[4];
  real_T c7_b_e[4];
  real_T c7_nargin = 1.0;
  real_T c7_nargout = 1.0;
  real_T c7_b_q[4];
  int32_T c7_i3;
  real_T c7_angle;
  real_T c7_c_e[3];
  uint32_T c7_b_debug_family_var_map[5];
  real_T c7_b_nargin = 2.0;
  real_T c7_b_nargout = 1.0;
  real_T c7_A;
  real_T c7_b_A;
  real_T c7_x;
  int32_T c7_i4;
  real_T c7_d0;
  real_T c7_b[3];
  int32_T c7_i5;
  int32_T c7_i6;
  int32_T c7_i7;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 6U, chartInstance->c7_sfEvent);
  for (c7_i1 = 0; c7_i1 < 4; c7_i1++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c7_e)[c7_i1], 0U);
  }

  chartInstance->c7_sfEvent = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 6U, chartInstance->c7_sfEvent);
  for (c7_i2 = 0; c7_i2 < 4; c7_i2++) {
    c7_b_e[c7_i2] = (*chartInstance->c7_e)[c7_i2];
  }

  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 4U, 4U, c7_debug_family_names,
    c7_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_nargin, 0U, c7_b_sf_marshallOut,
    c7_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_nargout, 1U, c7_b_sf_marshallOut,
    c7_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(c7_b_e, 2U, c7_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c7_b_q, 3U, c7_sf_marshallOut,
    c7_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, 4);
  for (c7_i3 = 0; c7_i3 < 3; c7_i3++) {
    c7_c_e[c7_i3] = c7_b_e[c7_i3];
  }

  c7_angle = c7_b_e[3];
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 5U, 5U, c7_b_debug_family_names,
    c7_b_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_b_nargin, 0U, c7_b_sf_marshallOut,
    c7_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_b_nargout, 1U, c7_b_sf_marshallOut,
    c7_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c7_c_e, 2U, c7_c_sf_marshallOut,
    c7_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c7_angle, 3U, c7_b_sf_marshallOut,
    c7_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c7_b_q, 4U, c7_sf_marshallOut,
    c7_sf_marshallIn);
  CV_SCRIPT_FCN(0, 0);
  _SFD_SCRIPT_CALL(0U, chartInstance->c7_sfEvent, 5);
  c7_A = c7_angle;
  c7_b_A = c7_angle;
  c7_x = muDoubleScalarSin(c7_b_A / 2.0);
  for (c7_i4 = 0; c7_i4 < 3; c7_i4++) {
    c7_b[c7_i4] = c7_c_e[c7_i4];
  }

  c7_d0 = muDoubleScalarCos(c7_A / 2.0);
  c7_b_q[0] = c7_d0;
  for (c7_i5 = 0; c7_i5 < 3; c7_i5++) {
    c7_b_q[c7_i5 + 1] = c7_x * c7_b[c7_i5];
  }

  _SFD_SCRIPT_CALL(0U, chartInstance->c7_sfEvent, -5);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_EML_CALL(0U, chartInstance->c7_sfEvent, -4);
  _SFD_SYMBOL_SCOPE_POP();
  for (c7_i6 = 0; c7_i6 < 4; c7_i6++) {
    (*chartInstance->c7_q)[c7_i6] = c7_b_q[c7_i6];
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 6U, chartInstance->c7_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_discreteInnerMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
  for (c7_i7 = 0; c7_i7 < 4; c7_i7++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c7_q)[c7_i7], 1U);
  }
}

static void mdl_start_c7_discreteInner(SFc7_discreteInnerInstanceStruct
  *chartInstance)
{
  sim_mode_is_external(chartInstance->S);
}

static void initSimStructsc7_discreteInner(SFc7_discreteInnerInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c7_machineNumber, uint32_T
  c7_chartNumber, uint32_T c7_instanceNumber)
{
  (void)(c7_machineNumber);
  (void)(c7_chartNumber);
  (void)(c7_instanceNumber);
  _SFD_SCRIPT_TRANSLATION(c7_chartNumber, c7_instanceNumber, 0U,
    sf_debug_get_script_id(
    "/home/brian/Documents/schoolProjects/AttitudeControl/MAE6345/AttitudeRepresentation/e2q.m"));
}

static const mxArray *c7_sf_marshallOut(void *chartInstanceVoid, void *c7_inData)
{
  const mxArray *c7_mxArrayOutData;
  int32_T c7_i8;
  const mxArray *c7_y = NULL;
  real_T c7_u[4];
  SFc7_discreteInnerInstanceStruct *chartInstance;
  chartInstance = (SFc7_discreteInnerInstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  c7_mxArrayOutData = NULL;
  for (c7_i8 = 0; c7_i8 < 4; c7_i8++) {
    c7_u[c7_i8] = (*(real_T (*)[4])c7_inData)[c7_i8];
  }

  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", c7_u, 0, 0U, 1U, 0U, 1, 4), false);
  sf_mex_assign(&c7_mxArrayOutData, c7_y, false);
  return c7_mxArrayOutData;
}

static void c7_emlrt_marshallIn(SFc7_discreteInnerInstanceStruct *chartInstance,
  const mxArray *c7_b_q, const char_T *c7_identifier, real_T c7_y[4])
{
  emlrtMsgIdentifier c7_thisId;
  c7_thisId.fIdentifier = (const char *)c7_identifier;
  c7_thisId.fParent = NULL;
  c7_thisId.bParentIsCell = false;
  c7_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_b_q), &c7_thisId, c7_y);
  sf_mex_destroy(&c7_b_q);
}

static void c7_b_emlrt_marshallIn(SFc7_discreteInnerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId,
  real_T c7_y[4])
{
  real_T c7_dv1[4];
  int32_T c7_i9;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), c7_dv1, 1, 0, 0U, 1, 0U, 1, 4);
  for (c7_i9 = 0; c7_i9 < 4; c7_i9++) {
    c7_y[c7_i9] = c7_dv1[c7_i9];
  }

  sf_mex_destroy(&c7_u);
}

static void c7_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData)
{
  const mxArray *c7_b_q;
  emlrtMsgIdentifier c7_thisId;
  real_T c7_y[4];
  int32_T c7_i10;
  SFc7_discreteInnerInstanceStruct *chartInstance;
  chartInstance = (SFc7_discreteInnerInstanceStruct *)chartInstanceVoid;
  c7_b_q = sf_mex_dup(c7_mxArrayInData);
  c7_thisId.fIdentifier = (const char *)c7_varName;
  c7_thisId.fParent = NULL;
  c7_thisId.bParentIsCell = false;
  c7_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_b_q), &c7_thisId, c7_y);
  sf_mex_destroy(&c7_b_q);
  for (c7_i10 = 0; c7_i10 < 4; c7_i10++) {
    (*(real_T (*)[4])c7_outData)[c7_i10] = c7_y[c7_i10];
  }

  sf_mex_destroy(&c7_mxArrayInData);
}

static const mxArray *c7_b_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData)
{
  const mxArray *c7_mxArrayOutData;
  real_T c7_u;
  const mxArray *c7_y = NULL;
  SFc7_discreteInnerInstanceStruct *chartInstance;
  chartInstance = (SFc7_discreteInnerInstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  c7_mxArrayOutData = NULL;
  c7_u = *(real_T *)c7_inData;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", &c7_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c7_mxArrayOutData, c7_y, false);
  return c7_mxArrayOutData;
}

static real_T c7_c_emlrt_marshallIn(SFc7_discreteInnerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId)
{
  real_T c7_y;
  real_T c7_d1;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), &c7_d1, 1, 0, 0U, 0, 0U, 0);
  c7_y = c7_d1;
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static void c7_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData)
{
  const mxArray *c7_nargout;
  emlrtMsgIdentifier c7_thisId;
  real_T c7_y;
  SFc7_discreteInnerInstanceStruct *chartInstance;
  chartInstance = (SFc7_discreteInnerInstanceStruct *)chartInstanceVoid;
  c7_nargout = sf_mex_dup(c7_mxArrayInData);
  c7_thisId.fIdentifier = (const char *)c7_varName;
  c7_thisId.fParent = NULL;
  c7_thisId.bParentIsCell = false;
  c7_y = c7_c_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_nargout), &c7_thisId);
  sf_mex_destroy(&c7_nargout);
  *(real_T *)c7_outData = c7_y;
  sf_mex_destroy(&c7_mxArrayInData);
}

static const mxArray *c7_c_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData)
{
  const mxArray *c7_mxArrayOutData;
  int32_T c7_i11;
  const mxArray *c7_y = NULL;
  real_T c7_u[3];
  SFc7_discreteInnerInstanceStruct *chartInstance;
  chartInstance = (SFc7_discreteInnerInstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  c7_mxArrayOutData = NULL;
  for (c7_i11 = 0; c7_i11 < 3; c7_i11++) {
    c7_u[c7_i11] = (*(real_T (*)[3])c7_inData)[c7_i11];
  }

  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", c7_u, 0, 0U, 1U, 0U, 1, 3), false);
  sf_mex_assign(&c7_mxArrayOutData, c7_y, false);
  return c7_mxArrayOutData;
}

static void c7_d_emlrt_marshallIn(SFc7_discreteInnerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId,
  real_T c7_y[3])
{
  real_T c7_dv2[3];
  int32_T c7_i12;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), c7_dv2, 1, 0, 0U, 1, 0U, 1, 3);
  for (c7_i12 = 0; c7_i12 < 3; c7_i12++) {
    c7_y[c7_i12] = c7_dv2[c7_i12];
  }

  sf_mex_destroy(&c7_u);
}

static void c7_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData)
{
  const mxArray *c7_b_e;
  emlrtMsgIdentifier c7_thisId;
  real_T c7_y[3];
  int32_T c7_i13;
  SFc7_discreteInnerInstanceStruct *chartInstance;
  chartInstance = (SFc7_discreteInnerInstanceStruct *)chartInstanceVoid;
  c7_b_e = sf_mex_dup(c7_mxArrayInData);
  c7_thisId.fIdentifier = (const char *)c7_varName;
  c7_thisId.fParent = NULL;
  c7_thisId.bParentIsCell = false;
  c7_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_b_e), &c7_thisId, c7_y);
  sf_mex_destroy(&c7_b_e);
  for (c7_i13 = 0; c7_i13 < 3; c7_i13++) {
    (*(real_T (*)[3])c7_outData)[c7_i13] = c7_y[c7_i13];
  }

  sf_mex_destroy(&c7_mxArrayInData);
}

const mxArray *sf_c7_discreteInner_get_eml_resolved_functions_info(void)
{
  const mxArray *c7_nameCaptureInfo = NULL;
  const char * c7_data[4] = {
    "789cc592c14ec240108607c1460f184e5e7c074b544cbc4910828928512e464d2ced6057babbd06e0d1c0c3e880fe063f8165e7c189742616968da04c5499af6"
    "eb9fcebf33fd2173dec800c08ebc4623800f0d82ca4fb93041d880c58aea9925f7b11ed626e402ce2bfaabe4f7299b9c091c88093884e1a54fdbe84a6006c559",
    "1b8b53c20c265ac31e828b1e775ed00a940e71b045285e7005ea4402ad29d20cc6d2f8b962a3d9bdf129b8b6373faea30228fb798c993f97b09f6845f7538870"
    "929f96e097f43fb6614be1c2e9aaf3853ed9d8f9b201e3413fd57c69fd348517fdb4802deeb71d9cfb7daee8771feb37e150bfab3ee836a7a8b75d6230fd8c9b",
    "3e45263cdd336dce9da6cb9fd19458168208dfc28accbfcb1dbd51ae1e1f1e9566efafb127632ebf3404e14c97ebdba769f6f77bf978bbfadefb2aaf378febce"
    "fffff90d62faa5cde36e8c5f98c750ef3f356bdd629f356a2746e7f6bc58ef967ac3cafc1ccd049fa47380c2cbe6fcabfe3f9533a498",
    "" };

  c7_nameCaptureInfo = NULL;
  emlrtNameCaptureMxArrayR2016a(c7_data, 1728U, &c7_nameCaptureInfo);
  return c7_nameCaptureInfo;
}

static const mxArray *c7_d_sf_marshallOut(void *chartInstanceVoid, void
  *c7_inData)
{
  const mxArray *c7_mxArrayOutData;
  int32_T c7_u;
  const mxArray *c7_y = NULL;
  SFc7_discreteInnerInstanceStruct *chartInstance;
  chartInstance = (SFc7_discreteInnerInstanceStruct *)chartInstanceVoid;
  c7_mxArrayOutData = NULL;
  c7_mxArrayOutData = NULL;
  c7_u = *(int32_T *)c7_inData;
  c7_y = NULL;
  sf_mex_assign(&c7_y, sf_mex_create("y", &c7_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c7_mxArrayOutData, c7_y, false);
  return c7_mxArrayOutData;
}

static int32_T c7_e_emlrt_marshallIn(SFc7_discreteInnerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId)
{
  int32_T c7_y;
  int32_T c7_i14;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), &c7_i14, 1, 6, 0U, 0, 0U, 0);
  c7_y = c7_i14;
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static void c7_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c7_mxArrayInData, const char_T *c7_varName, void *c7_outData)
{
  const mxArray *c7_b_sfEvent;
  emlrtMsgIdentifier c7_thisId;
  int32_T c7_y;
  SFc7_discreteInnerInstanceStruct *chartInstance;
  chartInstance = (SFc7_discreteInnerInstanceStruct *)chartInstanceVoid;
  c7_b_sfEvent = sf_mex_dup(c7_mxArrayInData);
  c7_thisId.fIdentifier = (const char *)c7_varName;
  c7_thisId.fParent = NULL;
  c7_thisId.bParentIsCell = false;
  c7_y = c7_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c7_b_sfEvent),
    &c7_thisId);
  sf_mex_destroy(&c7_b_sfEvent);
  *(int32_T *)c7_outData = c7_y;
  sf_mex_destroy(&c7_mxArrayInData);
}

static uint8_T c7_f_emlrt_marshallIn(SFc7_discreteInnerInstanceStruct
  *chartInstance, const mxArray *c7_b_is_active_c7_discreteInner, const char_T
  *c7_identifier)
{
  uint8_T c7_y;
  emlrtMsgIdentifier c7_thisId;
  c7_thisId.fIdentifier = (const char *)c7_identifier;
  c7_thisId.fParent = NULL;
  c7_thisId.bParentIsCell = false;
  c7_y = c7_g_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c7_b_is_active_c7_discreteInner), &c7_thisId);
  sf_mex_destroy(&c7_b_is_active_c7_discreteInner);
  return c7_y;
}

static uint8_T c7_g_emlrt_marshallIn(SFc7_discreteInnerInstanceStruct
  *chartInstance, const mxArray *c7_u, const emlrtMsgIdentifier *c7_parentId)
{
  uint8_T c7_y;
  uint8_T c7_u0;
  (void)chartInstance;
  sf_mex_import(c7_parentId, sf_mex_dup(c7_u), &c7_u0, 1, 3, 0U, 0, 0U, 0);
  c7_y = c7_u0;
  sf_mex_destroy(&c7_u);
  return c7_y;
}

static void init_dsm_address_info(SFc7_discreteInnerInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc7_discreteInnerInstanceStruct
  *chartInstance)
{
  chartInstance->c7_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c7_e = (real_T (*)[4])ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c7_q = (real_T (*)[4])ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c7_discreteInner_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3951121034U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(629425474U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(4230464496U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2903223579U);
}

mxArray* sf_c7_discreteInner_get_post_codegen_info(void);
mxArray *sf_c7_discreteInner_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("oZ30m4FTfee3kozQTLm4pC");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,1,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(4);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,1,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(4);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c7_discreteInner_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c7_discreteInner_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c7_discreteInner_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("pre");
  mxArray *fallbackReason = mxCreateString("hasBreakpoints");
  mxArray *hiddenFallbackType = mxCreateString("none");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c7_discreteInner_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c7_discreteInner_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c7_discreteInner(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x2'type','srcId','name','auxInfo'{{M[1],M[5],T\"q\",},{M[8],M[0],T\"is_active_c7_discreteInner\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 2, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c7_discreteInner_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc7_discreteInnerInstanceStruct *chartInstance =
      (SFc7_discreteInnerInstanceStruct *)sf_get_chart_instance_ptr(S);
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _discreteInnerMachineNumber_,
           7,
           1,
           1,
           0,
           2,
           0,
           0,
           0,
           0,
           1,
           &chartInstance->chartNumber,
           &chartInstance->instanceNumber,
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_discreteInnerMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_discreteInnerMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _discreteInnerMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"e");
          _SFD_SET_DATA_PROPS(1,2,0,1,"q");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,47);
        _SFD_CV_INIT_SCRIPT(0,1,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_SCRIPT_FCN(0,0,"e2q",0,-1,221);

        {
          unsigned int dimVector[1];
          dimVector[0]= 4U;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[1];
          dimVector[0]= 4U;
          _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c7_sf_marshallOut,(MexInFcnForType)
            c7_sf_marshallIn);
        }
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _discreteInnerMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc7_discreteInnerInstanceStruct *chartInstance =
      (SFc7_discreteInnerInstanceStruct *)sf_get_chart_instance_ptr(S);
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(0U, (void *)chartInstance->c7_e);
        _SFD_SET_DATA_VALUE_PTR(1U, (void *)chartInstance->c7_q);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "sCUeyDEazVdBzrlbO2r7V0E";
}

static void sf_opaque_initialize_c7_discreteInner(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc7_discreteInnerInstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c7_discreteInner((SFc7_discreteInnerInstanceStruct*)
    chartInstanceVar);
  initialize_c7_discreteInner((SFc7_discreteInnerInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c7_discreteInner(void *chartInstanceVar)
{
  enable_c7_discreteInner((SFc7_discreteInnerInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c7_discreteInner(void *chartInstanceVar)
{
  disable_c7_discreteInner((SFc7_discreteInnerInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c7_discreteInner(void *chartInstanceVar)
{
  sf_gateway_c7_discreteInner((SFc7_discreteInnerInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c7_discreteInner(SimStruct* S)
{
  return get_sim_state_c7_discreteInner((SFc7_discreteInnerInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c7_discreteInner(SimStruct* S, const mxArray
  *st)
{
  set_sim_state_c7_discreteInner((SFc7_discreteInnerInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_terminate_c7_discreteInner(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc7_discreteInnerInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_discreteInner_optimization_info();
    }

    finalize_c7_discreteInner((SFc7_discreteInnerInstanceStruct*)
      chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc7_discreteInner((SFc7_discreteInnerInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c7_discreteInner(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    initialize_params_c7_discreteInner((SFc7_discreteInnerInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

static void mdlSetWorkWidths_c7_discreteInner(SimStruct *S)
{
  /* Set overwritable ports for inplace optimization */
  ssSetInputPortDirectFeedThrough(S, 0, 1);
  ssSetStatesModifiedOnlyInUpdate(S, 1);
  ssSetBlockIsPurelyCombinatorial_wrapper(S, 1);
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_discreteInner_optimization_info
      (sim_mode_is_rtw_gen(S), sim_mode_is_modelref_sim(S), sim_mode_is_external
       (S));
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,7);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,7,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_set_chart_accesses_machine_info(S, sf_get_instance_specialization(),
      infoStruct, 7);
    sf_update_buildInfo(S, sf_get_instance_specialization(),infoStruct,7);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,7,1);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,7,1);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=1; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 1; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,7);
    sf_register_codegen_names_for_scoped_functions_defined_by_chart(S);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(2879082311U));
  ssSetChecksum1(S,(169309575U));
  ssSetChecksum2(S,(1002991695U));
  ssSetChecksum3(S,(845400099U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSetStateSemanticsClassicAndSynchronous(S, true);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c7_discreteInner(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c7_discreteInner(SimStruct *S)
{
  SFc7_discreteInnerInstanceStruct *chartInstance;
  chartInstance = (SFc7_discreteInnerInstanceStruct *)utMalloc(sizeof
    (SFc7_discreteInnerInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc7_discreteInnerInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c7_discreteInner;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c7_discreteInner;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c7_discreteInner;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c7_discreteInner;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c7_discreteInner;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c7_discreteInner;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c7_discreteInner;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c7_discreteInner;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c7_discreteInner;
  chartInstance->chartInfo.mdlStart = mdlStart_c7_discreteInner;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c7_discreteInner;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
  mdl_start_c7_discreteInner(chartInstance);
}

void c7_discreteInner_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c7_discreteInner(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c7_discreteInner(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c7_discreteInner(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c7_discreteInner_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
