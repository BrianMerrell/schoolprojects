#ifndef EXAMPLE_HPP_
#define EXAMPLE_HPP_

#include "mutex.hpp"
#include <cstring>
#include <iostream>

#define LOCALIP "10.128.1.9"
#define REMOTEIP "10.128.1.8"

#define RCVPORT 12345
#define SNDPORT 12346

#define RCVNUM 12
#define SNDNUM 4

extern Mutex receiveMutex;
extern double receiveBuffer[RCVNUM];

extern Mutex sendMutex;
extern double sendBuffer[SNDNUM];

extern Mutex controllerMutex;

void* receiveFunction(void *pArg);
void* sendFunction(void *pArg);
void* controller(void *pArg);



#endif
