%% Mission Planning
% Brian Merrell

%% Preliminaries
clearvars
close all
clc
format long


%% Addpath to Attitude Representations Folder
addpath ../AttitudeRepresentation
addpath ../Properties

%% Load projected orbital environment
load OrbitalEnvironment.mat


%%% DCM to Deployable Solar Panel
A_DB = euler2A(1,1,1,45*pi/180,0,0);