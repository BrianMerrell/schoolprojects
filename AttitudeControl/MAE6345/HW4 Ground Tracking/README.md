## Logan Utah Satellite Ground Tracking
This is a project that I worked on for an Attitude Control class. The objective was to design a simulation and controller for launching a 3U Cubesat with a single deployed panel as shown:

![3U_Cubesat](../images/3U_Cubesat.png)

The satellite was designed to be released into orbit on the 1st of March 2019 at 00:00:00.00 UTC with the following orbital elements.

* Semi-major axis = 6878137 m
* Eccentricity = 0
* True Anomaly = 0 degrees
* Right Ascensions of the Ascending Node = 0 degrees
* Inclination = 0 degrees
* Argument of Perigee = 0 degrees

### Satellite Structure Properties

The mass of the satellite structure is 3.21000120 kg.

The center of mass of the structure relative to origin of the B frame projected to the axes of the B frame is:
```
[0.16439646; -0.08187950; 0.09592783] meters.
```
The inertia tensor of the structure relative to the origin of the B frame projected to the axes of the B frame is:
```
Jxx = 0.08636806 kg*m^2
Jxy = -0.04320451 kg*m^2
Jxz = 0.05087768 kg*m^2
Jyy = 0.15161865 kg*m^2
Jyz = -0.02530725 kg*m^2
Jzz = 0.11387280 kg*m^2

[ Jxx -Jxy -Jxz
 -Jxy  Jyy -Jyz
 -Jxz -Jyz  Jzz]
```

### Deployable Panel Properties

The mass of the deployable panel is 0.05290847 kg.

The center of mass of the deployable relative to the origin of the B frame projected to the axes of the B frame is:
```
[0.16364135; 0.08256652; -0.13481916] meters.
```
The inertia tensor of the deployable relative to the origin of the B frame projected to the axes of the B frame is:
```
Jxx = 0.00177584 kg*m^2
Jxy = 0.00071486 kg*m^2
Jxz = -0.00116727 kg*m^2
Jyy = 0.00263292 kg*m^2
Jyz = -0.00081569 kg*m^2
Jzz = 0.00203193 kg*m^2

[ Jxx -Jxy -Jxz
 -Jxy  Jyy -Jyz
 -Jxz -Jyz  Jzz] 
```
### Control Actuator Properties

The satellite will be controlled with a Blue Canyon Technologies XACT.

The XACT contains 3 reaction wheels that are assumed to be aligned with the axes of the Body frame of the satellite

Each of the wheels has the following properties:

* Maximum Speed 6500 RPM
* Maximum Angular Momentum 15 mN-m-s
* Maximum Torque 6 mN-m

The reaction wheel acceleration command to acceleration output is modeled as a 2nd order system with the following parameters:

* Natural Frequency of 10 Hz
* Damping Ratio of sqrt(2)/2
The XACT also contains 3 torque coils that are assumed to be aligned with the axes of the Body frame of the satellite.

Each of the torque coils can generate a maximum magnetic moment of 0.17 A-m^2.

The torque coil magnetic moment command to magnetic moment output is modeled as a 1st order system with a time constant of 1/(100*2*pi).

### Control System Delays

The delays in the control system (combined communications, and computational) are assumed to be 10 ms.  Any command sent to the actuators will be delayed by this amount.

### Satellite Mission

The satellite's mission is to point its positive z body axis at Utah State University (41.7452 degrees N, 111.8097 degrees W, 1456.334 m altitude) when it passes over Logan Utah.  The ground antenna can communicate with the satellite when it is 10 degrees above the horizon and the satellite's z face must be pointed at USU during this time.  There is a secondary pointing contraint during ground contacts to align the satellite positive y body axis as closely as possible with orbit angular momentum vector.

When the satellite is not passing over USU is to be in a sun pointing orientation.  The unit normal vector of the deployable solar panel is to be pointed directly at the sun.  The unit normal vector of the panel is found by rotating 45 degrees about the positive x body axis.  The unit normal vector is the y basis vector of this new rotated frame.  During sun point there is secondary constraint to align the satellite positive x body axis as closely as possible with orbit angular momentum vector.

### Impelmentation and Control Design

##### Inner Loop

The inner loop contorller was designed so that all three principal axes of rotation would have the same cross over frequency. This was done by calculating the proportional control gain based of a common crossover frequency. This crossover frequency was found with MATlab's control system designer so that it would allow for about 65 degrees of phase margin. This allows for fast response and minimal overshoot.

In the inner loop we also consider the dynamics of the reaction wheels. This can be seen at about 103 in the main file. 

##### Reaction Wheels

The reaction wheels are modeled as a second order system. The characteristics for this second order system are taken from Blue Canyon's data sheet for the XACT. This system is used to calculate the inner loop controller.

##### Outer Loop

The outer loop is a little more complicated. The outer loop uses a proportional gain with a zero and pole placement to buy back phase and ignore high frequency disturbances. These are calculated by viewing the open loop bode plots in MATlab's control system designer.

##### Torque Coils

The controller for the torque coils are rather simple. This controller just applies a counter torque that can be viewed as a disturbance to reduce to momentum stored on the reaction wheels.

##### Trajectory Calculations

To help avoid instability a trapezoidal trajectory is calculated to provide a more realistic input signal. Since the reaction wheels cannot respond perfectly to a step input the burden is eased by creating a trajectory.

### How to Run
This project relies on MATlab, Simulink and Simscape. This project also relies on a few files that I have loaded above this repo, "AttituleRepresentation" and "Properties". These files contain my functions for representing a satellite's attitude and the basic properties, frames and orbital environment described above.

After cloning the repo you will need to run "missionPlan.m", "missionPlanning.slx", and "main.m" in that order. I did not have the main set up to run all of the files because the computation time is fairly long. It can be fairly easily implemented though by adding the following code to line 210 in the main file.
```
missionPlan;
simulation('missionPlanning',3600);
```

### Error Plots

![error](error.jpg)






