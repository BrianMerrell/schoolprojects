%% Momentum Dumping
% Name:Brian Merrell

%% Preliminaries
% This cleans all variables and sets the format to display more digits.
clearvars
close all
clc
format long

%% Addpath to Attitude Representations Folder
addpath('C:\Users\a02292333\Documents\MAE6345\AttitudeRepresentation')
addpath('C:\Users\a02292333\Documents\MAE6345\Properties')

%% Mass Properties
mass_properties;
load orbital_environment;
load Structure_Definition;

%% Reaction Wheels
wrmax = 6500*2*pi/60; % rad/s -- Max angular velocity
hrmax = 15e-3; % kg*m^2/s Max angular momentum
Jr = hrmax/wrmax; % kg*m^2 Inertia
hrdotmax = 6e-3;
wrdotmax = hrdotmax/Jr; % kg*m^2/s^2 Max angular accel
wn = 2*pi*10; % Wheel natural frequency
zeta = sqrt(2)/2; % wheel damping ratio
safety = 0.5;


%% Torque Coil Patameters
tau = 1/(100*2*pi);
Mmax = 0.17; % A*m^2

%% Initial Satellite Attitude
% Initial attitude quaternion of the B frame relative to the I frame.
q0_BI = [1; 0; 0; 0];

%% 
% Simscape's initial attitude defined by a DCM.
A0_BI = q2A(q0_BI);
A0_IB = A0_BI';

%% 
% Initial Satellite Angular Velocity
wbi0_B = [0;0;0];

%%
% Initial Angular Velocity in Principal Frame
wbi0_P = A_PB*wbi0_B;

%%
% Initial Reaction Wheel Angular Velocity
wr0_R=[-2000;2000;2000]*2*pi/60; % rad/s

%% Trapezoidal Profile Parameters
q0_PI = qXp(q_PB, q0_BI);
a_max = safety*hrdotmax/max(max(J_C_P));
w_max = safety*hrmax/max(max(J_C_P));
t_max = w_max/a_max;
angle_max = 0.5*a_max*t_max^2;
hrstar_B = [0;0;0];

%% Demanded Position
% e = [1;2;3]; e = e/norm(e);
% qstar_BI = e2q(e,180*pi/180);

%% Parameters for design and simulation
% Estimated Control Time Delay
dt_delay = 0.01;

%% Inner Loop Control System Design
% The following section contains the calculations for the control system
% design. This includes Bode plots and the thresholds for marginal
% stability.

%%
% Transfer Function Variable 
s = tf('s');

%%
% Controller Delay
C = exp(-s*dt_delay);

%% 
% Plant for each decoupled axis
G1 = 1/J_C_P(1,1)/s;
G2 = 1/J_C_P(2,2)/s;
G3 = 1/J_C_P(3,3)/s;
G = [G1; G2; G3];

%%
% Reaction Wheel Transfer Function
Gr = wn^2/(s^2 + 2*wn*zeta*s + wn^2);

%%
% Pade Approximation
[num, den] = pade(dt_delay,8);
C_pade = tf(num, den);

%%
% Contorl system designer configuration
config = sisoinit(1);
config.G.value = G1;
config.C.value = Gr*C_pade;
% controlSystemDesigner(config);

%%
% Natural Crossover Frequencies
w_crossover = 2*pi*2.35; % Observed from Control System Designer
% display(w_crossover/2/pi,'Inner Loop Crossover frequency (Hz)')

%%
% Calculating Proportional Control Gains.
Kd1 = 1/bode(G1*Gr*C,w_crossover);
Kd2 = 1/bode(G2*Gr*C,w_crossover);
Kd3 = 1/bode(G3*Gr*C,w_crossover);
Kd = diag([Kd1;Kd2;Kd3]);

% display(Kd1, 'Inner Loop Proportional Gain 1')
% display(Kd2, 'Inner Loop Proportional Gain 2')
% display(Kd3, 'Inner Loop Proportional Gain 3')

%%
% Calculate Gain and Phase Margins. Only one axis was displayed because all
% three have the same Gain and Phase Margins.
[Gm1, Pm1] = margin(Kd1*C*G1);
% display(mag2db(Gm1),'Inner Gain Margin (dB)');
% display(Pm1, 'Inner Phase Margin (degrees)');

[Gm2, Pm2] = margin(Kd2*C*G2);

[Gm3, Pm3] = margin(Kd3*C*G3);

%% Inner Closed Loop Characteristics

%%
% Inner Closed Loop transfer function calculation
CLTF1 = feedback(Kd1*C*G1,1);
CLTF2 = feedback(Kd2*C*G2,1);
CLTF3 = feedback(Kd3*C*G3,1);

%% Open Outer Loop Control System Design
% The following section contains the calculations for the control system
% design. This includes Open Loop Bode plots.

%%
% Outer Controller Proportional Gain
Kp = 1/bode(CLTF1*1/s,2*pi);
[Gm, Pm] = margin(Kp*CLTF1*1/s);

%%
% Contorl system designer configuration
config = sisoinit(6);
config.G1.value = G1;
config.C1.value = 516.148*(s+.05)/s/(s+50);
config.C2.value = Kd1*C_pade;
config.G2.value = 1/s;
config.OL1.View ={'bode'};
config.OL2.View = {};
% controlSystemDesigner(config);

%% 
% Outer Loop Controler Transfer Function. Taken from the Control System
% Desingner.
%%
% Proportional Gain 
k = 295.65;

%%
% Zero Placement 
z = 0.03; 

%%
% Pole Placement
p = 50; 

%%
% Outer Loop Controller
Co = k*(s+z)/s/(s+p);

%%
% Outer Open Loop Transfer Function
% Only one of the axis inner loops are needed since they are all the same.
OLTF = Co*CLTF1/s;

%% Outer Closed Loop Characteristics
% Outer Closed Loop Transfer Function and Bode Plot. This also almost
% resembles the inner closed loop bode plot, but with a smaller bandwidth.
CLTF = feedback(OLTF,1);

%% Momentum Dumping Transfer Function
Gm = 1/(tau*s+1)*(1/s)*C_pade;
Km = 1/bode(Gm,2*pi*.001);

display(Km,'Calculated Proportional Gain');
figure
margin(Km*Gm);


%% Trapezoidal Profile Feed Forward Response
% close all
sim("MomentumDumping",3600);

figure
plot(angle)
title('Error Angle');

figure
plot(Mstar_B);
title('Commanded Magnetic Moment');

figure
plot(wr_B*60/pi/2);
title('Angular Velocity of the Reaction Wheels');



