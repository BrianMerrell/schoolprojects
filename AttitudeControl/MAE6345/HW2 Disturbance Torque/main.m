%% Disturbance Torques
% Name:Brian Merrell

%% Preliminaries
% This cleans all variables and sets the format to display more digits.
clearvars
close all
clc
format long

%% Addpath to Attitude Representations Folder
addpath('C:\Users\a02292333\Documents\MAE6345\AttitudeRepresentation')
addpath('C:\Users\a02292333\Documents\MAE6345\Properties')

%% Mass Properties
mass_properties;

%% Structure Definition
load Structure_Definition.mat

%% Orbital Environment
load orbital_environment.mat

%% Distrubance Parameters

%%
% Magnetic Moment
M0_B = [0.001; -0.002; 0.003]; % A*m^2

%%
% Solar Irradiance
Psr = 1366/299792458; % W/m^2

%%
% Earth's Gravitational Parameter
uEarth = 3.986004418e14; % m^3/s^2

%% Initial Attitude State
% Initial attitude of the B frame relative to the I frame.
q0_BI = [1;0;0;0];

%% Simulation
sim('disturbances',3600);

%% Aero Disturbance
figure
plot(Ta_B);
title('Aerodynamic Disturbances');
display(Ta_B.data(1,:), 'Initial Aerodynamic Disturbance');

%% Solar Disturbance
figure
plot(Ts_B);
title('Solar Disturbances');
display(Ts_B.data(1,:), 'Initial Solar Disturbance');

%% Gravity Gradient Disturbance
figure
plot(Tg_B);
title('Gravity Gradient Disturbances');
display(Tg_B.data(1,:), 'Initial Gravity Gradient Disturbance');

%% Magnetic Disturbance
figure
plot(Tm_B);
title('Magnetic Disturbances');
display(Tm_B.data(1,:), 'Initial Magnetic Disturbance');

