## Project 3 5930 Mobile Robotics

### Installition and Launch

To install this project, unzip the src file into a project folder. In a terminal window naviagte to this project folder and run:

```
catkin_make
source devel/setup.bash
roslaunch turtlebot_launch proj3.launch
```

These commands will build and run our code.

### Usage

To manipulate the robot use the 2-D Nav Goal button and click on a location.

### Code changes

The only code changes from Dr. Droge's example code was in Unicycle.cpp and Unicycle.h.

The .h file had some variables declared and the .cpp file has the velocity controller implemented.

### Controller

Our controller mirrors Dr. Droge's Jerk controller, but the control inputs are accelerations rather then jerk.


