# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/brian/Documents/schoolProjects/MobileRobotics/proj3/src/low_level_control/dynamic_models/include".split(';') if "/home/brian/Documents/schoolProjects/MobileRobotics/proj3/src/low_level_control/dynamic_models/include" != "" else []
PROJECT_CATKIN_DEPENDS = "geometry_msgs;nav_msgs;roscpp;rospy;sensor_msgs;std_msgs;tf".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-ldynamic_models".split(';') if "-ldynamic_models" != "" else []
PROJECT_NAME = "dynamic_models"
PROJECT_SPACE_DIR = "/home/brian/Documents/schoolProjects/MobileRobotics/proj3/devel"
PROJECT_VERSION = "0.0.0"
