# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/brian/Documents/schoolProjects/MobileRobotics/proj3/src/low_level_control/controllers/include".split(';') if "/home/brian/Documents/schoolProjects/MobileRobotics/proj3/src/low_level_control/controllers/include" != "" else []
PROJECT_CATKIN_DEPENDS = "geometry_msgs;nav_msgs;roscpp;rospy;sensor_msgs;std_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lcontrollers".split(';') if "-lcontrollers" != "" else []
PROJECT_NAME = "controllers"
PROJECT_SPACE_DIR = "/home/brian/Documents/schoolProjects/MobileRobotics/proj3/devel"
PROJECT_VERSION = "0.0.0"
