# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/brian/Documents/schoolProjects/MobileRobotics/proj3/src/turtle/turtlebot3_sim/turtlebot3_fake/include".split(';') if "/home/brian/Documents/schoolProjects/MobileRobotics/proj3/src/turtle/turtlebot3_sim/turtlebot3_fake/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;std_msgs;sensor_msgs;geometry_msgs;nav_msgs;tf;turtlebot3_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "turtlebot3_fake"
PROJECT_SPACE_DIR = "/home/brian/Documents/schoolProjects/MobileRobotics/proj3/devel"
PROJECT_VERSION = "1.2.0"
