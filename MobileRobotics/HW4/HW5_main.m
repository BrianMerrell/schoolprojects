%% HW 4 Linear Controls
% Name: Brian Merrell

%% Preliminaries
clc
clearvars all
close all

%% Problem 1

%% 1.1
% $\bar{A} = A - BK$

%% 1.2
% Stable - For any bounded input there is a bounded output, "BIBO".
%
% Unstable - Not stable
%
% Asymptotically Stable - For a "BIBO" system x(t) converges to
% x_equilibrium as t approaches infinity
%
% Globally Asymptotically Stable - If all x(t) converges to x_equilibrium

%% 1.3
% A system is stable if $\lambda_i < 0$ where $\lambda_i$ represents the
% eigenvalues of $A$

%% 1.4

A_a = [1 1; 2 -1];
B_a = [0; 1];
display(eig(A_a), 'Eq 1 Eigenvalues')
display('Unstable')
A_b = [1 1; 0 -1];
B_b = [1; 0];
display(eig(A_b), 'Eq 2 Eigenvalues')
display('Unstable')
A_c = [1 0; 0 0];
B_c = [0; 1];
display(eig(A_c), 'Eq 3 Eigenvalues')
display('Unstable')

%% Problem 2

%% 2.1
% 1) If a system is controllable then a new state is reachable by some
% action trajectory. -LaVelle pg 867
%
% 2) The eigenvalues of the A matrix determine the stability of the system.
% This also effects the controllability of the system. A system is called
% "completely controllable" if the eigenvalues can be chosen arbitrarily
% and still have a stable system. Controllability also refers to how
% sensitive the system is to changes in desired outputs.

%% 2.2
% Matrix: $\Gamma = [B, AB, A^2B]$
%
% Test: $rank(Gamma) >= 3$ then the system is completely controllable.

%% 2.3
a = [B_a A_a*B_a];
if rank(a) >= length(A_a)
    disp('Eq 1 is completely controllable')
else
    disp('Eq 1 is not completely controllable')
end

b = [B_b A_b*B_b];
if rank(b) >= length(A_b)
    disp('Eq 2 is completely controllable')
else
    disp('Eq 2 is not completely controllable')
end

c = [B_c A_c*B_c];
if rank(c) >= length(A_c)
    disp('Eq 3 is completely controllable')
else
    disp('Eq 3 is not completely controllable')
end

%% 2.4

K_place = place(A_a, B_a, [-1 -2])
eig(A_a - B_a*K_place)
% Since these are our pole values that we chose, which are both negative
% the system is stable.

Q = [1 0; 0 2];
R = 1;

K_lqr = lqr(A_a, B_a, Q, R)
eig(A_a - B_a*K_lqr)
% Because both of these eigenvalues are negative the system is stable.

%% Problem 3

%% 3.1

Controllers(1)

%% 3.2

% Controllers(2)

%% Problem 4

%% 4.1

% Controllers(3)

%% 4.2

% Controllers(4)

%% Functions
function Controllers(type)
switch type
    case 1
        % Better Unicycle
        A = [0 0; 0 0];
        B = [1 0; 0 1];
        poles = [-1, -2];
        K = place(A, B, poles);
        veh = BetterUnicycle;
        u = @(t,x)constantRadiusBetterUnicycle(t,x,veh,K);
        x0 = [0; 0; 0; 0; 0];
    case 2
        % Continuous Steering Bicycle - LaValle (13.48)
        A = [0 0 0; 0 0 1; 0 0 0];
        B = [1 0; 0 0; 0 1];
        poles = [ -1, -2, -3];
        K = place(A,B,poles);
        
        veh = ContinuousSteeringBicycle13_48;
        u = @(t,x)constantRadiusContinuousSteeringBicycle13_48(t,x,veh,K);
        x0 = [0; 0; 0; 0; 0; 0];
        
    case 3
        % Differential Drive - State feedback - LQR
        % Calculate gain
        A = [0 0; 0 0];
        B = [1 0; 0 1];
        Q = diag([1 1]);
        R = diag([1 1]);
        K = lqr(A, B, Q, R);
        
        qd = [4; 3];
        eps = 0.1;
        
        veh = DifferentialDrive;
        u = @(t,x)constantRadiusDiffDrive(t,x,veh, K, qd, eps);
        x0 = [0; 0; 0];
    case 4
        % Smooth Differential Drive - State feedback - LQR
        % Calculate gain
        A = [0 0 1 0; 0 0 0 1; 0 0 0 0; 0 0 0 0];
        B = [0 0; 0 0; 1 0; 0 1];
        Q = diag([1 1 1 1]);
        R = diag([1 1]);
        K = lqr(A, B, Q, R);
        
        qd = [4; 3; 0; 0];
        eps = 0.1;
        
        veh = SmoothDifferentialDrive;
        u = @(t,x)constantRadiusSmoothDiffDrive(t,x,veh, K, qd, eps);
        x0 = [0; 0; 0; 0; 0];
end

% Select the integration mode
integrator = @(t0, dt, tf, x0, veh, u) integrateODE(t0, dt, tf, x0, veh, u);

% Integrate the state
t0 = 0;
dt = 0.1;
tf = 10;
[tmat, xmat] = integrator(t0, dt, tf, x0, veh, u);

% Plot the velocities
veh.plotVelocitiesAndInput(tmat, xmat, u);

% Plot the state
figure;
for k = 1:length(tmat)
    veh.plotState(tmat(k), xmat(:,k));
    pause(dt);
end

end


function [tmat, xmat] = integrateODE(t0, dt, tf, x0, veh, u)
% Input parameters
%   t0: initial time
%   dt: time step for return data
%   tf: final time
%   x0: initial state
%   veh: instantiation of VehicleKinematics class
%   u: function handle which takes input arguments of (t,x)

% Integrate forward in time
[tmat, xmat] = ode45(@(t,x)veh.kinematics(t, x, u(t,x)), [t0:dt:tf], x0);

% Transpose the outputs
tmat = tmat';
xmat = xmat';
end
function u = constantRadiusBetterUnicycle(t, x, veh, K)
% Define desired values
v_d = 5;
w_d = 0.5;

% Extract states
v = x(veh.v_ind);
w = x(veh.w_ind);

% Simple feedback control to calculate inputs

u_v = -(v-v_d);
u_w = -(w-w_d);
u = K*[u_v; u_w];
end
function u = constantRadiusContinuousSteeringBicycle13_48(t, x, veh,K)
% Define desired values
v_d = 5;
phi_d = 0.0997;

% Extract states
v = x(veh.v_ind);
phi = x(veh.phi_ind);
phi_dot = x(veh.phi_dot_ind);

% Simple feedback control to calculate inputs
u_v = -(v-v_d);
phi_err = -(phi-phi_d);
phi_err = atan2(sin(phi_err), cos(phi_err)); % Angle trick to get error angle between -pi and pi
u_phi = phi_err - phi_dot;

u = K*[u_v; u_phi; 0];
end
function u = constantRadiusDiffDrive(t, x, veh, K, q_d, eps)
% Form the velocity state
[v, w] = veh.getVelocities(t, x, [0; 0]);
x_pos = x(veh.x_ind);
y_pos = x(veh.y_ind);
%     x_vel = [v; w];
th = x(veh.th_ind);
c = cos(th);
s = sin(th);

% Form espilon variables
w_hat_e = [0 -eps*w; w/eps 0];
R_e = [c -eps*s; s eps*c];
R_e_inv = [1 0; 0 1/eps] * [c s; -s c];

% Calculate current values of espilon state
q_eps = [x_pos; y_pos] + eps * [c; s];
q_eps_dot = R_e*[v; w];
q = [q_eps];

% Calculate point control
u_point = -K*(q - q_d);

% Calculate the control inputs
u = R_e_inv*u_point;
u = inv([1/8 1/8; 1/4 -1/4])*u;
end
function u = constantRadiusSmoothDiffDrive(t, x, veh, K, q_d, eps)
% Form the velocity state
[v, w] = veh.getVelocities(t, x, 0);
x_pos = x(veh.x_ind);
y_pos = x(veh.y_ind);
%     x_vel = [v; w];
th = x(veh.th_ind);
c = cos(th);
s = sin(th);

% Form espilon variables
w_hat_e = [0 -eps*w; w/eps 0];
R_e = [c -eps*s; s eps*c];
R_e_inv = [1 0; 0 1/eps] * [c s; -s c];

% Calculate current values of espilon state
q_eps = [x_pos; y_pos] + eps * [c; s];
q_eps_dot = R_e*[v; w];
q = [q_eps; q_eps_dot];

% Calculate point control
u_point = -K*(q - q_d);

% Calculate the control inputs
u = R_e_inv*u_point - w_hat_e*[v; w];
u = inv([1/8 1/8; 1/4 -1/4])*u;
end









