function [ q ] = e2q( e, angle )
%E2Q Convert an Euler Axis/Angle Representation to a Quaternion
%   Detailed explanation goes here

q = [cos(angle/2); sin(angle/2)*e]; 
% Hint: Compare eq 2.124, but q1 IS THE SCALAR

end

