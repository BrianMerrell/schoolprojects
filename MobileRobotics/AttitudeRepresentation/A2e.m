function [ e, angle ] = A2e( A )
%A2E Convert a DCM to Euler Axis/Angle Representation
%   Detailed explanation goes here

angle = acos((trace(A)-1)/2);
switch angle
    case 0
        e = [1; 0; 0];
    case pi()
        A = (A + eye(3))/2;
        [M,I] = max([norm(A(:,1)) norm(A(:,2)) norm(A(:,3))]);
        e = normc(A(:,I));         
    otherwise
        e = 1/(2*sin(angle))*[A(2,3)-A(3,2); A(3,1)-A(1,3); A(1,2)-A(2,1)];
end
end

