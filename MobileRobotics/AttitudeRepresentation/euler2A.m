function [ A ] = euler2A( e1, e2, e3, phi, theta, psi )
%EULER2A Convert a User Specified Euler Angle Sequence to a DCM
% This code assumes the values for e1, e2, e3 are 1, 2, or 3 with no chance
% of error.

% Check which case of e1 and assing it the designated rotation matrix
e = [0;0;0];
e(e1) = 1;
A1 = e2A(e,psi);

% if e1 == 1
%     A1 = [1 0 0;
%           0 cos(phi) -sin(phi);
%           0 sin(phi) cos(phi)];
% elseif e1 == 2
%     A1 = [cos(phi) 0 sin(phi);
%           0 1 0;
%           -sin(phi) 0 cos(phi)];
% else
%     A1 = [cos(phi) -sin(phi) 0;
%           sin(phi) cos(phi) 0;
%           0 0 1];
% end
% Check which case of e2 and assing it the designated rotation matrix
e = [0;0;0];
e(e2) = 1;
A2 = e2A(e,theta);
% if e2 == 1
%     A2 = [1 0 0;
%           0 cos(theta) -sin(theta);
%           0 sin(theta) cos(theta)];
% elseif e2 == 2
%     A2 = [cos(theta) 0 sin(theta);
%           0 1 0;
%           -sin(theta) 0 cos(theta)];
% else
%     A2 = [cos(theta) -sin(theta) 0;
%           sin(theta) cos(theta) 0;
%           0 0 1];
% end
% Check which case of e3 and assing it the designated rotation matrix
e = [0;0;0];
e(e3) = 1;
A3 = e2A(e,phi);
% if e3 == 1
%     A3 = [1 0 0;
%           0 cos(psi) -sin(psi);
%           0 sin(psi) cos(psi)];
% elseif e3 == 2
%     A3 = [cos(psi) 0 sin(psi);
%           0 1 0;
%           -sin(psi) 0 cos(psi)];
% else
%     A3 = [cos(psi) -sin(psi) 0;
%           sin(psi) cos(psi) 0;
%           0 0 1];
% end

% Assuming an intrisic rotation Azyx = Ax*Ay*Az
A = A3*A2*A1;
end
