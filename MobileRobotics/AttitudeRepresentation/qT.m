function [qout] = qT( q ) 
%  QT Calculate the conjugate of a quaternion.
%   Detailed explanation goes here
qout = [q(1); -q(2:end)];
% Hint: Compare 2.91, but q1 IS THE SCALAR

end

