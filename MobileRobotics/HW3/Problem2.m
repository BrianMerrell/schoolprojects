%% Problem 2
% Brian Merrell
%% Preliminaries
clc
clearvars
close all

%% 2.1 
%% 2.1 -- 7a
T_W1 = tfm(pi/4,0,0);
T_12 = tfm(pi/2,10,0);
T_23 = tfm(-pi/4,10,0);

P_C3 = [11;1;1];
P_B2 = [11;0;1];
P_A1 = [5;0;1];

P_CW = T_W1*T_12*T_23*P_C3
P_BW = T_W1*T_12*P_B2
P_AW = T_W1*P_A1

%% 2.1 -- 7b
% Since the lengths are all equal in length theta2 = theta3 = 60deg and
% theta 1 if the joint is continous will range from -inf to inf.

%% 2.1 -- LaValle #8
T_W1 = tfm(pi/4,0,0);
T_12 = tfm(pi/2,1,0);
T_23 = tfm(pi/4,2,0);
P_3 = [1;0;1];

P_W = T_W1*T_12*T_23*P_3


%% 2.2
Transform = ['T1';'T2';'T3';'T4';'T5';'T6';'T7'];
Joint_Angle_theta_f = [0;-pi/2;0;-pi/2;pi/2;-pi/2;pi/2];
Joint_Offset_d = [1100; 0; 0; 1200;0;372;0];
Link_Twist_alpha = [0;-pi/2;0;0;0;0;pi/2];
Link_Normal_a = [0;600;1400;65;0;0;0];

T = table(Transform, Joint_Angle_theta_f, Joint_Offset_d, Link_Twist_alpha, Link_Normal_a)



%% tfm function
function A = tfm(theta,dx,dy)
A = [cos(theta) -sin(theta) dx;
     sin(theta) cos(theta) dy;
     0 0 1];
end


