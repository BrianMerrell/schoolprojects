%% Problem 1
% Brian Merrell
%% Preliminaries

clc
clearvars
close all

addpath ../AttitudeRepresentation

%% 1.1
% My Functions assume clockwise as a positive rotation. That is why I
% transpose the matrix
A = euler2A(3,2,1,pi/4,pi/4,pi/4)';
display(A, 'Initial Rotation Matrix A');

q = A2q(A);
q = [q(1); -q(2:4)];
display(q, 'Quaternion of A, q');

[e, angle] = q2e(q);

% My Functions assume clockwise as a positive rotation. So I set the angle
% to the negative angle.
% angle = -angle;
display(e, 'Axis of Rotation');

display(e*angle, 'Exponential Coordinates, [zeta_1; zeta_2; zeta_3]');

display(X(e)*angle, 'Exponential matrix');

R = expm(X(e)*angle);
display(R, 'Resulting Matrix')

display(A-R,'Due to numerical errors the matrices are not identical, but are numerically close. A-R');

%% 1.2
% Solve for a different zeta values that results in the same DCM. This can
% be done by looking at a negative rotation.

A1 = euler2A(3,2,1,-pi/4,-pi/4,-pi/4)'
q = A2q(A1);
[e, angle] = q2e(q);
% Since my functions assume clockwise positive rotations the below equation
% is written with as -angle.
display(e*-angle, 'New Exponential Coordinates, [zeta_1; zeta_2; zeta_3]');

