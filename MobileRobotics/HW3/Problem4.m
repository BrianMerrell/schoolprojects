%% Problem 4
clearvars;
%% 4.1
z1 = .1;
z2 = -.3;

beta = 90-z1*180/pi;
alpha = 90+z2*180/pi;

L = 1;
l = 180-alpha-beta;
r1 = L*sind(alpha)/sind(l)
r2 = L*sind(beta)/sind(l)

%% 4.2
v1 = 2;

w1 = v1/L*tand(alpha+beta)

%% 4.3
v2 = v1+(r2-r1)*-w1