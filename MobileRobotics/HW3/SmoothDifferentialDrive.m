classdef SmoothDifferentialDrive < VehicleKinematics
    %SimpleBicycle Implements a unicycle with direct control over the
    %velocities
    
    properties (GetAccess = public, SetAccess = public)
       % Vehicle properties
       L = 1
       ul_ind = 5;
       ur_ind = 4;
       
       % Plotting properties
       h_wheel = [];
       h_wheel_line = [];
    end
    
    methods
        function obj = SmoothDifferentialDrive()
            obj = obj@VehicleKinematics(5);
            obj.plot_path = true;
        end
        
        function xdot = kinematics(obj, t, x, u)
            %kinematics Gives the unicycle dynamics
            %   [x; y; theta] = [vcos(theta); vsin(theta); omega]
            %   u = [v; phi]
            vr = x(obj.ur_ind);
            vl = x(obj.ul_ind);
            
            % Extract inputs
            v = obj.r*(vl+vr)/2; % Translational velocity
            w = obj.r/obj.L*(vr-vl); % Rotational velocity
            obj.plotVelocity(t,v,w,u);
            % Calculate dynamics
            theta = x(obj.th_ind);  % Orientation
            
            xdot = zeros(5,1);
            xdot(obj.x_ind) = v * cos(theta); % \dot{x}
            xdot(obj.y_ind) = v * sin(theta); % \dot{y}
            xdot(obj.th_ind) = w; % \dot{theta}  
            xdot(obj.ur_ind) = u(1);
            xdot(obj.ul_ind) = u(2);
        end 
        
    end 
end

