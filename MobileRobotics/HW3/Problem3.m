
function Problem3()
    %% Problem 3
    close all
    %% Initial Variables
    t0 = 0;
    dt = 0.1;
    tf = 12;
    
    % Select the integration mode
    integrator = @(t0, dt, tf, x0, veh, u) integrateODE(t0, dt, tf, x0, veh, u);
    %integrator = @(t0, dt, tf, x0, veh, u) integratorEuler(t0, dt, tf, x0, veh, u);
    
    %% Simple Unicycle
    veh = SimpleUnicycle;
    u = @(t,x)constantRadiusUnicycle(t,x);
    
    x0 = [0;0;0];
    
    
    [tmat, xmat] = integrator(t0, dt, tf, x0, veh, u);
    
    % Plot the state
    for k = 1:length(tmat)
       veh.plotState(tmat(k), xmat(:,k));
       pause(dt);
    end
    
    %% Simple Bicycle
    veh = SimpleBicycle;
    u = @(t,x)constantRadiusBicycle(t,x);
    
    [tmat, xmat] = integrator(t0, dt, tf, x0, veh, u);
    
    % Plot the state
    for k = 1:length(tmat)
       veh.plotState(tmat(k), xmat(:,k));
       pause(dt);
    end
    %% Differntial Drive
    veh = DifferentialDrive;
    u = @(t,x)constantRadiusDiffDrive(t,x);
    
    
    [tmat, xmat] = integrator(t0, dt, tf, x0, veh, u);
    
    % Plot the state
    for k = 1:length(tmat)
       veh.plotState(tmat(k), xmat(:,k));
       pause(dt);
    end
    %% Better Unicycle
    veh = BetterUnicycle;
    u = @(t,x)constantRadiusBetterUnicycle(t,x);
    
    x0 = [0;0;0;5;.5];
    [tmat, xmat] = integrator(t0, dt, tf, x0, veh, u);
    
    % Plot the state
    for k = 1:length(tmat)
       veh.plotState(tmat(k), xmat(:,k));
       pause(dt);
    end
    %% Continuous Steering Car
    veh = ContinuousSteeringCar;
    u = @(t,x)constantRadiusContinuousSteeringCar(t,x);
    
    x0 = [0; 0; 0; 5; atan(1/10)]; % ContinuousSteering R = 10
    
    [tmat, xmat] = integrator(t0, dt, tf, x0, veh, u);
    
    % Plot the state
    for k = 1:length(tmat)
       veh.plotState(tmat(k), xmat(:,k));
       pause(dt);
    end
    
    %% Smooth Differential Drive
    veh = SmoothDifferentialDrive;
    u = @(t,x)constantRadiusSmoothDiffDrive(t,x);
    
    x0 = [0;0;0;25;2.5*190/21];
    [tmat, xmat] = integrator(t0, dt, tf, x0, veh, u);
    
    % Plot the state
    for k = 1:length(tmat)
       veh.plotState(tmat(k), xmat(:,k));
       pause(dt);
    end
    %% 3.3
    %% Better Unicycle - Time Varying
    veh = BetterUnicycle;
    u = @(t,x)constantRadiusBetterUnicycleAccel(t,x);
    
    x0 = [0;0;0;0;0];
    [tmat, xmat] = integrator(t0, dt, tf, x0, veh, u);
    
    % Plot the state
    for k = 1:length(tmat)
       veh.plotState(tmat(k), xmat(:,k));
       pause(dt);
    end
    %% Continuous Steering Car - Time Varying
    veh = ContinuousSteeringCar;
    u = @(t,x)AccelRadiusContinuousSteeringCar(t,x);
    
    x0 = [0; 0; 0; 0; 0]; % ContinuousSteering R = 10
    
    [tmat, xmat] = integrator(t0, dt, tf, x0, veh, u);
    
    % Plot the state
    for k = 1:length(tmat)
       veh.plotState(tmat(k), xmat(:,k));
       pause(dt);
    end
    
    %% Smooth Differential Drive - Time Varying
    veh = SmoothDifferentialDrive;
    u = @(t,x)constantRadiusSmoothDiffDriveAccel(t,x);
    
    x0 = [0;0;0;0;0];
    [tmat, xmat] = integrator(t0, dt, tf, x0, veh, u);
    
    % Plot the state
    for k = 1:length(tmat)
       veh.plotState(tmat(k), xmat(:,k));
       pause(dt);
    end
    
end

function [tmat, xmat] = integrateODE(t0, dt, tf, x0, veh, u)
% Input parameters
%   t0: initial time
%   dt: time step for return data
%   tf: final time
%   x0: initial state
%   veh: instantiation of VehicleKinematics class
%   u: function handle which takes input arguments of (t,x)

    % Integrate forward in time
    [tmat, xmat] = ode45(@(t,x)veh.kinematics(t, x, u(t,x)), [t0:dt:tf], x0);
    
    % Transpose the outputs
    tmat = tmat';
    xmat = xmat';
end

function [tmat, xmat] = integratorEuler(t0, dt, tf, x0, veh, u)
% Input parameters
%   t0: initial time
%   dt: time step for return data
%   tf: final time
%   x0: initial state
%   veh: instantiation of VehicleKinematics class
%   u: function handle which takes input arguments of (t,x)

    % Initialize state data
    tmat = [t0:dt:tf]';
    len = length(tmat);
    xmat = zeros(veh.dimensions, len);
    xmat(:,1) = x0;
    
    % Loop through and calculate the state
    x = x0;
    for k = 1:len
        % Calculate state update equation
        t = tmat(k);
        xdot = veh.kinematics(t, x, u(x,t));
        
        % Update the state
        x = x + dt * xdot;
        
        % Store the state
        xmat(:,k) = x;
    end

end

function u = constantRadiusUnicycle(t, x)
    u = [5; .5]; % [v, w]
end

function u = constantRadiusBicycle(t, x)
    u = [5; atan(1/10)]; % .7854 corresponds to a steering angle that will produce w = 1 for L and v = 1
end

function u = constantRadiusDiffDrive(t, x)
    u = [25; 2.5*190/21];
end

function u = constantRadiusBetterUnicycle(t, x)
    u = [0; 0]; %[1; .1];
end

function u = constantRadiusContinuousSteeringCar(t, x)
    u = [0; 0];
end

function u = constantRadiusSmoothDiffDrive(t, x)
    u = [0; 0];%[10; 190/21];
end

function u = constantRadiusBetterUnicycleAccel(t, x)
    u = [1*t/12; .1*t/12]; %[1; .1];
end

function u = AccelRadiusContinuousSteeringCar(t, x)
    u = [5*t/24; atan(1/10)*sqrt(t)/48];
end

function u = constantRadiusSmoothDiffDriveAccel(t, x)
    u = [10*t/12; 190/21*t/12];%[10; 190/21];
end


