% HW03 directions
% 1) Replace the fi, ti, ai, and di with the correct DH parameters. 
%    Some values are already specified to set the base and gripper.
%    Parameters not used have the value nan.
% 2) Save the file. (It should be data_KUKAKR1000_HW03.m)
% 3) Open demo_robot_serial.m. Make sure that demo_robot = 'data_KUKAKR1000_HW03'.
%    You may first want to try out an example by uncommenting the line:
%    %demo_robot = 'data_SPHERICAL';
% 4) Run program to output a figure. You may want to save the figure as a *.png file.
% 5) The variables under "%options" allow you to change the look of the model.


%Denavit-Hartenberg data - logically indexed from frame 0 (base) to frame n+1 (gripper)
dh.t = [nan,    0,    -pi/2,   0,   0,   0,   0, pi/2];            %joint angles      (radians) 0...n+1 (first element ignored)
dh.d = [nan,    1100,   0,    0,   1200,   0,   372,    0];            %joint offsets               0...n+1 (first element ignored)
dh.f = [  0,    -pi/2,  0,  -pi/2,  pi/2,  -pi/2,  pi/2,  nan];            %link twist angles (radians) 0...n+1 (last  element ignored)
dh.a = [  0,    600,  1400,   65,   0,  0,     0,  nan];            %link common normals         0...n+1 (last  element ignored)
dh.j = ['B',   'P',   'P',   'R',   'R',   'R',   'R',  'G'];            %R/P/G/B/N - Revolute/Prismatic/Gripper/Base/None joint type
dh.T{1} = eye(4);                                                        %frame 0 (base) location
                                                       %frame 0 (base) location
%options
joint_centered = ['CCCCCCMC'];                                           %C/T/M     - Center/Top/center of RPG joint at o(i)/o(i)/(Midpoint d(i))(0...n+1)
frame_type     = ['FFFFFFFN'];                                           %F/P/N     - ith frame (0..n+1) is Fancy/Plain/None                                                                                %            first argument (i = 0) is ignored
%alpha_val      = 1;                                                      %set transparency, 0 --> 1 = transparent --> opaque
alpha_val      = 0.25;                                                   %set transparency, 0 --> 1 = transparent --> opaque   
link_type      = 'L';                                                    %S/L       - links and offsets are solids/lines
R_rad_ratio    = 40       ;                                              %ratio of total link & offset distance to R joint radius 
                                                                         %(main robot scale factor sets relative joint sizes)
