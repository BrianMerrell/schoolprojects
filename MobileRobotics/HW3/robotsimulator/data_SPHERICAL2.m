%Denavit-Hartenberg data - logically indexed from frame 0 (base) to frame n+1 (gripper)
dh.t = [nan,  pi/6,  pi/3,  pi/2, pi/2];            %joint angles        0...n+1 (first element ignored)
dh.d = [nan,    20,     0,    15,    0];            %joint offsets       0...n+1 (first element ignored)
dh.a = [  0,     0,     0,     0,  nan];            %link common normals 0...n+1 (last  element ignored)
dh.f = [  0,  pi/2,  pi/2,  pi/2,  nan];            %link twist angles   0...n+1 (last  element ignored)
dh.j = ['B',   'R',   'R',   'N',  'G'];            %R/P/G/B/N - Revolute/Prismatic/Gripper/Base/None joint type
dh.T{1}        = eye(4);                            %frame 0 (base) location

%options
joint_centered = ['CMCMC'];                         %C/T/M     - Center/Top/center of RPG joint at o(i)/o(i)/(Midpoint d(i))(0...n+1)
frame_type     = ['FFNFN'];                         %F/P/N     - ith frame (0..n+1) is Fancy/Plain/None
alpha_val      = 1;                                 %set transparency, 0 --> 1 = transparent --> opaque
%alpha_val      = 0.25;                             %set transparency, 0 --> 1 = transparent --> opaque   
link_type      = 'L';                               %S/L       - links and offsets are solids/lines
R_rad_ratio    = 40       ;                         %ratio of total link & offset distance to R joint radius 
                                                    %(main robot scale factor sets relative joint sizes)
                                                    
   