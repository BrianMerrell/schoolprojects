%Denavit-Hartenberg data - logically indexed from frame 0 (base) to frame n+1 (gripper)
dh.t = [nan,     0,     0,     0,   pi,   pi/2, pi/2, pi/2];                    %joint angles        0...n+1 (first element ignored)
%Note that the dh.t values are dummies. Only length(dh.t)is used.
dh.d = [nan,   100,    50,   -50,  100,     0,    80,    0];                    %joint offsets       0...n+1 (first element ignored)
dh.a = [  0,     0,   100,    10,    0,     0,     0,  nan];                    %link common normals 0...n+1 (last  element ignored)
dh.f = [  0, -pi/2,     0, -pi/2, pi/2, -pi/2,  pi/2,  nan];                    %link twist angles   0...n+1 (last  element ignored)
dh.j = ['B',   'R',   'R',   'R',  'R',   'R',   'R',  'G'];                    %R/P/G/B/N - Revolute/Prismatic/Gripper/Base/None joint type
dh.T{1}        = eye(4);                                                        %frame 0 (base) location

%options
joint_centered = ['CMCCMCMC'];                                                  %C/T/M     - Center/Top/center of RPG joint at o(i)/o(i)/(Midpoint d(i))(0...n+1)
%frame_type     = ['FNNNNNFN'];                                                  %F/P/N     - ith frame (0..n+1) is Fancy/Plain/None
%frame_type     = ['PPPPPPPN'];                                                  %F/P/N     - ith frame (0..n+1) is Fancy/Plain/None
frame_type     = ['FFFFFFFN'];                                                  %F/P/N     - ith frame (0..n+1) is Fancy/Plain/None
alpha_val      = 1;                                                             %set transparency, 0 --> 1 = transparent --> opaque
%alpha_val      = 0.25;                                                          %set transparency, 0 --> 1 = transparent --> opaque   
%link_type      = 'L';                                                           %S/L       - links and offsets are Solids/Lines
link_type      = 'S';                                                           %S/L       - links and offsets are Solids/Lines
%R_rad_ratio    =  40;                                                           %ratio of total link & offset distance to R joint radius 
                                                                                %(main robot scale factor sets relative joint sizes)export_movie   = 'N';                                                           %Y/N   - export/no export of avi movie
export_movie   = 'N'                                                            %export movie to .avi file
movie_name     = '6R_robot';                                                    %exported movie name (.avi appended)                                                                            
optimal_axes   = 'N';                                                           %Y/N   - determine optimal size of axes (requires an extra motion generation)

%BEGIN{GENERATE JOINT DISPLACEMENTS}*******************************************
%The joint angles are saved in a matrix q below.
% size(q) = [length(dh.t), number of poses] so each row is a robot pose.
%
%This is simple test data that moves each joint sequentially through 2*pi
%Replace this section with desired joint angle data for animation
n_inc  = 9;                                                                     %number increments for each joint to move
q_inc  = linspace(0,2*pi,n_inc);                                                %simple motion - rotate each joint sequentially by 2pi
N      = length(dh.t);                                                          %last joint index; logically indexed from 1 instead of 0
n      = N-1;                                                                   %last joint index; logically indexed from 0 
n_pose = (n-1)*n_inc;                                                           %number of poses
q      = repmat(dh.t,[n_pose,1]);                                               %array of initial poses
c_pose = 0;                                                                     %pose counter
for i = 1:(n-1)                                                                 %for each moving joint (base is 0th joint, gripper is nth joint)
    I = i+1;                                                                    %joint vector index
        for j = 1:n_inc                                                         %draw robot thru motion range
            c_pose = c_pose+1;                                                  %pose displacement counter
            q(c_pose,I) = q(c_pose,I)+q_inc(j);                                 %update ith joint to jth displacement
        end %j
end %i
%END{GENERATE JOINT DISPLACEMENTS}---------------------------------------------

