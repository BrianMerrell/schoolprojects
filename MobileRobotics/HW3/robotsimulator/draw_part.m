function draw_part(pt,unit,I,T,part,varargin)

%DESCRIPTION
%   This function forms a planar polygon profile,
%   extrudes it into a part along the normal direction, 
%   and displaces the part using a homogenous transformation matrix.
%
%INPUT ARGUMENTS
%   part ----------- property constants for all joint and link types (struct)
%   pt ------------- property constants for current joint or link    (struct)
%      .type ------- 'd' round offset ,  'p' square offset           (character)
%                    'a' common normal,  'B' base      joint
%                    'R' revolute joint, 'P' prismatic joint
%                    'G' gripper  joint, 'N' no part
%                    'H' polygon link with cutout for gripper (NEEDS REVISION)
%   unit ----------- property variables for current joint or link    (struct)
%      .top -------- local z coord for top                           (N vector)
%      .bot -------- local z coord for bottom                        (N vector)
%   I -------------- current joint/frame index (I=1..N),(i=0..n)     (scalar) 
%   T -------------- displacement matrix from base to Ith frame      (4x4)
%   varargin: for platforms (NEEDS REVISION)
%   (1) x ------ x coordinates of polygon vertices                   (vector)
%   (2) y ------ y coordinates of polygon vertices                   (vector)
%
%OUTPUT ARGUMENTS
%   (none)
%
%FUNCTION CALLS 
%   patch_cylinder - draw cylinder
%   rot -------- elementary rotation matrix 

%PROGRAM

%BEGIN{INITIALIZATIONS}********************************************************
if length(varargin) == 2                                  %x and y polygon coordinates inputted
    x = varargin{1};                                      %x coordinate
    y = varargin{2};                                      %y coordinate
    n = length(x);                                        %number of vertices
end %if
%END{INITIALIZATIONS}----------------------------------------------------------

%BEGIN{DRAW PARTS}*************************************************************
%test for valid parts
part_cases = {'a','d','p','R','P','B','G','H'};           %non-null parts (excludes 'N')
if ~ismember(pt.type,part_cases);                         %if no part,return
    return;                                               %early exit
end %if

%select and draw parts
switch pt.type                                            %set up for extrusion along local z axis
    case {'a'}                                            %common normal a(i)
        T = T*rot('y',pi/2);                              %rotate z into x axis
        patch_cylinder(part,pt,unit.bot(I),unit.top(I),T);%extrude polygon up into a solid

    case {'R','P','B','G','d','p'}                        %R,P,B joint or offset d (ready to extrude along local z axis)
        patch_cylinder(part,pt,unit.bot(I),unit.top(I),T);%extrude polygon up into a solid
             
    case {'H'} %NEEDS REVISION                            %general polygon link with cutout for gripper
        %add points near gripper (nth coordinate) to form cutout
        j2g(1,:) = x(n)-x(1:n-1);                         %x coord of vectors from joint vertices to gripper vertex
        j2g(2,:) = y(n)-y(1:n-1);                         %y coord of vectors from joint vertices to gripper vertex
        dir_j2g  = j2g./repmat(sqrt(dot(j2g,j2g)),2,1);   %make into unit vectors
        p = [x(1:n-1);y(1:n-1)]+(j2g-radius/2*dir_j2g);   %n-1 new points to be hidden by gripper
        p = fliplr(p);                                    %reverse order of new points
        x = [x(1:n-1),p(1,:),x(1)];                       %add on new points and close polygon
        y = [y(1:n-1),p(2,:),y(1)];                       %add on new points and close polygon        
        patch_cylinder(part,pt,unit.bot(I),unit.top(I),T);%extrude polygon up into a solid
end %switch

%END{DRAW PARTS}---------------------------------------------------------------




