function z=Y(R)

%Forms 6x6 rotation matrix from 3x3 rotation matrix R
 z = [[R,zeros(3)];[zeros(3),R]];