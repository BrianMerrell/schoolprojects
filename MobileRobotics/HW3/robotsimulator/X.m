function z=X(a)

%Forms shifting matrix for twists from vector a

%Form 3x3 skew-symmetric matrix from vector a
y = [ 0    -a(3)  a(2) ;
      a(3)  0    -a(1) ;
     -a(2)  a(1)  0   ];

%Form shifting matrix
 z = [[eye(3),y];[zeros(3),eye(3)]];