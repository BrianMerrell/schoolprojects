function patch_cylinder(part,pt,zmin,zmax,T)

%DESCRIPTION
%   This function takes an x,y planar polygon profile as a base, 
%   forms a cone along the z direction above the origin,      
%   and displaces the cone using a homogenous transformation matrix.
%
%   The function is a modification of patch_cylinder where the top of
%   where the cylinder top has been replaced by a single point (apex).
%
%   Uses the "patch" function method by specifying the coordinates of each unique vertex and a
%   matrix that specifies how to connect these vertices to form the faces.
%
%INPUT ARGUMENTS
%   x ---------- x coordinates of polygon vertices (vector)
%   y ---------- y coordinates of polygon vertices (vector)
%   zmin ------- z coordinates of base points      (scalar)
%   zmax ------- z coordinate  of apex point       (scalar)
%                (apex at [0 0 zmax])
%   T ---------- homogeneous transformation matrix (4x4)
%   edge ------- edge drawing options (scalar)
%   bst_colors - bottom/side/top RGB colors of cylinders (3x1 or 3x3)
%                (top color not used but kept for uniformity with patch_cylinder)
%
%OUTPUT ARGUMENTS
%   (none)
%
%FUNCTION CALLS 
%   (none)

%PROGRAM
x = pt.x;
y = pt.y;
edge = (part.edges=='Y');
bst_colors = pt.color;
%BEGIN{INITIALIZATIONS}********************************************************
%set colors
if  size(pt.color) == [3 3];                          %separate colors for bottom, side, and top of extrusion
    bottom_color = pt.color(1,:);                     %color for bottom of extrusion
    side_color   = pt.color(2,:);                     %color for side   of extrusion
    top_color    = pt.color(3,:);                     %color for top    of extrusion
else                                                  %one color       for bottom, side, and top of extrusion    
    bottom_color = pt.color;                          %color for bottom of extrusion
    side_color   = pt.color;                          %color for side   of extrusion
    top_color    = pt.color;                          %color for top    of extrusion
end %if size(pt.color)
    
%set edges (default is black)
if part.edges == 'Y'                                  %draw edges
    b_edgecol = pt.edgecolor{1};                      %bottom edge color
    s_edgecol = pt.edgecolor{2};                      %side   edge color
    t_edgecol = pt.edgecolor{3};                      %top    edge color
else                                                  %draw no edges
    b_edgecol = 'none';                               %no    bottom edge
    s_edgecol = 'none';                               %no    side   edge
    t_edgecol = 'none';                               %no    top    edge
end %if edge
        
%reshape polygon data
n = length(pt.x);                                     %number of vertices
pt.x = reshape(pt.x,n,1);                             %make x a column vector
pt.y = reshape(pt.y,n,1);                             %make y a column vector
zbottom(1:n,:) = zmin;                                %make z coords for extrusion bottom
ztop(1:n,:)    = zmax;                                %make z coords for extrusion top

%extract rotation and translation
Rt = T(1:3,1:3)';                                     %rotation matrix transposed
pv = repmat(T(1:3,4)',[n,1]);                         %position vector transposed and replicated n times
%END{INITIALIZATIONS}----------------------------------------------------------

%BEGIN{FORM CONE PATCHES}******************************************************
%create vertices for top and bottom of extrusion
vertex_matrix_bottom = [pt.x,pt.y,zbottom]*Rt+pv;     %all bottom vertices
vertex_matrix_top    = [0,0,zmax]*Rt+T(1:3,4)';       %one top vertex

%create face matrix for top and bottom of extrusion
face_matrix_bottom   = 1:n;                           %bottom is a single patch
face_matrix_top      = (n+1)*ones(1,n);               %top    is a single point

%create vertex matrix (2n x 3) for sides of extrusion (n rectangular patches)
vertex_matrix_side = [vertex_matrix_bottom; vertex_matrix_top];

%create face matrix for sides of extrusion (n triangular patches)
face_matrix_side = [face_matrix_bottom;               %bottom vertices (unshifted)
                    face_matrix_bottom(:,[2:n,1]);    %bottom vertices shifted by one
                    face_matrix_top;                  %top    vertex
                    ]'; %end face_matrix_side
%END{FORM CONE PATCHES}--------------------------------------------------------
                
%BEGIN{DRAW CONE PATCHES}******************************************************
%draw cone
if part.existalpha == 'Y'                             %transparency supported
    %draw bottom face
    patch('Vertices'         ,vertex_matrix_bottom,...
          'Faces'            ,face_matrix_bottom,...
          'FaceVertexCData'  ,repmat(bottom_color,[length(face_matrix_bottom),1]),...
          'FaceColor'        ,'interp',...
          'EdgeColor'        ,b_edgecol,...
          'FaceAlpha'        ,part.facealpha,...
          'EdgeAlpha'        ,part.edgealpha...
          ) %end patch

    %draw side faces
    patch('Vertices'         ,vertex_matrix_side,...
          'Faces'            ,face_matrix_side,...
          'FaceVertexCData'  ,repmat(side_color,[length(vertex_matrix_side),1]),...
          'FaceColor'        ,'interp',...
          'EdgeColor'        ,s_edgecol,...
          'FaceAlpha'        ,part.facealpha,...
          'EdgeAlpha'        ,part.edgealpha...
          ) %end patch
else                                                  %transparency not supported
    %draw bottom face
    patch('Vertices'         ,vertex_matrix_bottom,...
          'Faces'            ,face_matrix_bottom,...
          'FaceVertexCData'  ,repmat(bottom_color,[length(face_matrix_bottom),1]),...
          'FaceColor'        ,'interp',...
          'EdgeColor'        ,b_edgecol...
          ) %end patch

       
    %draw side faces
    patch('Vertices'         ,vertex_matrix_side,...
          'Faces'            ,face_matrix_side,...
          'FaceVertexCData'  ,repmat(side_color,[length(vertex_matrix_side),1]),...
          'FaceColor'        ,'interp',...
          'EdgeColor'        ,s_edgecol...
          ) %end patch
end %if rel_min
%END{DRAW CONE PATCHES}--------------------------------------------------------
               
