function fr = init_frame(part)

%BEGIN{USER OPTIONS} - DEFAULTS GOOD FOR MOST APPLICATIONS*********************                                    
%basic part options - good for most applications
fr.default.sides      = 20;                                                     %number of sides for cylindrical looking parts
fr.default.xcolor     = [0 0 1];                                                %x axis shaft color (blue)
fr.default.ycolor     = [0 1 0];                                                %y axis shaft color (green)
fr.default.zcolor     = [1 0 0];                                                %z axis shaft color (red)
fr.default.edgecolor  = {'k','none','k'};                                       %{bottom, side, top} edge colors for default

%set default user inputs if not specified

fr.scale.height       = 1.5;                                                    %ratio of coordinate axes height to R joint height
%fr.scale.height       = 8;                                                    %ratio of coordinate axes height to R joint height
%part_fr_scale_height  = part.fr.scale.height;
if exist('part_fr_scale_height','var')==1,fr.scale.height=part.fr.scale.height;end; %ratio of coordinate axes height to R joint height
if exist('part.fr.sh.scale.rad','var')~=1                                       %links and offsets are solid style
    fr.sh.scale.rad   = 3/20;                                                   %ratio of shaft radius to R joint radius
else                                                                            %links and offsets are line style
    fr.sh.scale.rad   = part.fr.sh.scale.rad                                    %ratio of shaft radius to R joint radius
end %if exist
fr.ah.scale.rad       = 1.60;                                                   %ratio of arrowhead radius to shaft radius
fr.ah.scale.height    = 4;                                                      %ratio of arrowhead height to shaft radius
fr.height             = fr.scale.height*part.R.ht;                              %height of coordinate axes
fr.sides              = fr.default.sides;                                       %number of sides for cylindrical looking parts
fr.edges              = 'Y';                                                    %Y/N - Edges/None drawn
fr.edgecolor          = fr.default.edgecolor;                                   %edge color

fr.facealpha          = 1;                                                      %0 to 1, - opaque faces (default)
fr.edgealpha          = 1;                                                      %0 to 1, - opaque faces (default)
fr.existalpha         = 'Y';                                                    %'Y'/'N' - transparency supported (default - will check below)

%check if transparency is supported
rel_min = 12;                                                                   %mininum release number to support transparency
ver_str = version;                                                              %version string
rel_num = str2num(ver_str(find(ver_str=='R')...                                 %release number 
    +1:(find(ver_str==')')-1)));                                                %(located between 'R' and ')')
if rel_num < rel_min                                                            %check if transparency supported
    fr.existalpha     = 'N';                                                    %transparency not supported
end %rel_num

%more options and set parameters
fr.sh.xcolor          = fr.default.xcolor;                                      %x axis shaft color
fr.sh.ycolor          = fr.default.ycolor;                                      %y axis shaft color
fr.sh.zcolor          = fr.default.zcolor;                                      %z axis shaft color
    
%shaft style parameters
fr.sh.sides           = fr.default.sides;                                       %shaft sides
fr.sh.rad             = part.R.rad*fr.sh.scale.rad;                             %shaft radius
fr.sh.bot             = -fr.sh.rad;                                             %shaft start - extend for better symmetry around origin
fr.sh.top             = fr.height-fr.ah.scale.height*fr.sh.rad;                 %shaft end   - allow space for arrowhead
t                     = linspace(0,2*pi,fr.sh.sides+1)+(pi/fr.sh.sides);        %generate evenly spaced vertices, offset by 1/2 a side
fr.sh.x               = fr.sh.rad*cos(t);                                       %shaft local x coords
fr.sh.y               = fr.sh.rad*sin(t);                                       %shaft local y coords

%arrowhead colors
fr.ah.xcolor          = fr.default.xcolor;                                      %x axis shaft color
fr.ah.ycolor          = fr.default.ycolor;                                      %y axis shaft color
fr.ah.zcolor          = fr.default.zcolor;                                      %z axis shaft color
   
%arrowhead style parameters
fr.ah.sides           = fr.default.sides;                                       %arrowhead sides
fr.ah.rad             = fr.sh.rad*fr.ah.scale.rad;                              %arrowhead radius
fr.ah.bot             = fr.sh.top;                                              %arrowhead start - at end of axis
fr.ah.top             = fr.height;                                              %arrowhead end   - at length fr.height
fr.ah.x               = fr.sh.x*fr.ah.scale.rad;	                            %arrowhead bottom local x coords
fr.ah.y               = fr.sh.y*fr.ah.scale.rad;                                %arrowhead bottom local y coords

%END{USER OPTIONS} - DEFAULTS GOOD FOR MOST APPLICATIONS-----------------------                                    

