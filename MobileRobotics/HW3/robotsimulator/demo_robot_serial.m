%clean up
clear all
clc

%BEGIN{USER DATA}**************************************************************
demo_robot = 'data_KUKAKR1000_HW03';
%demo_robot = 'data_SPHERICAL2';
%demo_robot = 'data_SPHERICAL';

%END{USER DATA}----------------------------------------------------------------
                                    
%BEGIN{SET PARAMETERS}*********************************************************                                    
eval(demo_robot);                                                               %input demo_robot parameters
N      = length(dh.t);                                                          %last joint index; logically indexed from 1 instead of 0

%set default user inputs if not specified
if exist('joint_centered','var')~=1,joint_centered  =  repmat('C',1,N)    ;end; %C/T/M     - Center/Top/center of RPG joint at o(i)/o(i)/(Midpoint d(i))(0...n+1)
if exist('frame_type'    ,'var')~=1,frame_type =[repmat('F',1,N-1),'N']   ;end; %F/P/N     - ith frame (0..n+1) is Fancy/Plain/None
if exist('alpha_val'     ,'var')~=1,alpha_val  =1                         ;end; %set transparency, 0 --> 1 = transparent --> opaque
if exist('link_type'     ,'var')~=1,link_type  ='S'                       ;end; %S/L       - links and offsets are solids/lines
if exist('R_rad_ratio'   ,'var')~=1&link_type  =='S',R_rad_ratio=40       ;end; %ratio of total link distance to R joint radius (main robot scale factor)
if exist('R_rad_ratio'   ,'var')~=1&link_type  =='L',R_rad_ratio=80       ;end; %ratio of total link distance to R joint radius (main robot scale factor)

%set joint options and parameters
joint.centered = joint_centered;                                                %set placement of joints on axes
joint.type     = dh.j;                                                          %set joint types

%set part options and parameters
R_rad  = sum(abs([dh.d(2:N),dh.a(1:N-1)]))/R_rad_ratio;                         %R joint radius - other parts sized to this dimension
part           = init_part(R_rad,link_type);                                    %set basic part options and parameters
part.facealpha = alpha_val;                                                     %face transparency of all parts
part.edgealpha = alpha_val;                                                     %edge transparency of all parts
if alpha_val < 0.75                                                             %don't draw edge for visual effect
    part.edgealpha = 0;                                                         %transparent edge
end %alpha_val

%set frame options and parameters
%next line doesn't work - need to fix
if exist('frame_scale_height','var')==1,part.fr.scale.height=frame_scale_height;end; %ratio of coordinate axes height to R joint height
frame          = init_frame(part);                                              %set basic frame options and parameters
frame.type     = frame_type;                                                    %F/P/N     - ith frame (0..n+1) is Fancy/Plain/None
%END{SET PARAMETERS}-----------------------------------------------------------

%BEGIN{NEW FIGURE}*************************************************************
figure                                                                          %open new window
hold on                                                                         %keep open to plot multiple links, joints, frames, etc.
axis equal                                                                      %maintain realistic dimensions
grid                                                                            %looks good
view(3)                                                                         %usually a good viewpoint
%END{NEW FIGURE}---------------------------------------------------------------

%BEGIN{DRAW ROBOT}*************************************************************
draw_robot_serial(dh,joint,frame,part)                                          %draw robot
%END{DRAW ROBOT}---------------------------------------------------------------

%BEGIN{FIGURE LIGHTING}********************************************************
lighting phong                                                                  %smooth lighting (use Gouraud for speed)
material shiny                                                                  %also try default, dull, metal
%lightangle(-37.5,30);                                                           %good light angle for view(3)
h1=camlight(0,0);                                                               %good light angle for view(3)
%END{FIGURE LIGHTING}----------------------------------------------------------
