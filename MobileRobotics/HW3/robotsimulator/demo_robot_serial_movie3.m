%demo_robot_serial_movie
%clean up
clear all
clc

%BEGIN{USER DATA}**************************************************************
demo_robot = 'data_PUMA_movie';
%END{USER DATA}----------------------------------------------------------------
                                    
%BEGIN{SET PARAMETERS}*********************************************************                                    
eval(demo_robot);                                                               %input demo_robot parameters
N      = length(dh.t);                                                          %last joint index; logically indexed from 1 instead of 0

%set default user inputs if not specified
if exist('joint_centered','var')~=1,joint_centered  =  repmat('C',1,N)    ;end; %C/T/M     - Center/Top/center of RPG joint at o(i)/o(i)/(Midpoint d(i))(0...n+1)
if exist('frame_type'    ,'var')~=1,frame_type =[repmat('F',1,N-1),'N']   ;end; %F/P/N     - ith frame (0..n+1) is Fancy/Plain/None
if exist('alpha_val'     ,'var')~=1,alpha_val  =1                         ;end; %set transparency, 0 --> 1 = transparent --> opaque
if exist('link_type'     ,'var')~=1,link_type  ='S'                       ;end; %S/L       - links and offsets are solids/lines
if exist('R_rad_ratio'   ,'var')~=1&link_type  =='S',R_rad_ratio=40       ;end; %ratio of total link distance to R joint radius (main robot scale factor)
if exist('R_rad_ratio'   ,'var')~=1&link_type  =='L',R_rad_ratio=80       ;end; %ratio of total link distance to R joint radius (main robot scale factor)

if exist('export_movie'  ,'var')~=1,export_movie = 'N'                    ;end; %Y/N   - export/no export of avi movie
if exist('movie_name'    ,'var')~=1,movie_name = '6R_robot'               ;end; %exported movie name (.avi appended) 
if exist('optimal_axes'  ,'var')~=1,optimal_axes = 'N'                    ;end; %determine optimal size of axes (requires an extra motion generation)

%set joint options and parameters
joint.centered = joint_centered;                                                %set placement of joints on axes
joint.type     = dh.j;                                                          %set joint types

%set part options and parameters
R_rad  = sum(abs([dh.d(2:N),dh.a(1:N-1)]))/R_rad_ratio;                         %R joint radius - other parts sized to this dimension
part           = init_part(R_rad,link_type);                                    %set basic part options and parameters
part.facealpha = alpha_val;                                                     %face transparency of all parts
part.edgealpha = alpha_val;                                                     %edge transparency of all parts
if alpha_val < 0.75                                                             %don't draw edge for visual effect
    part.edgealpha = 0;                                                         %transparent edge
end %alpha_val

%set frame options and parameters
frame          = init_frame(part);                                              %set basic frame options and parameters
frame.type     = frame_type;                                                    %F/P/N     - ith frame (0..n+1) is Fancy/Plain/None    
%END{SET PARAMETERS}-----------------------------------------------------------

%BEGIN{SET AXES DIMENSIONS}****************************************************
figure                                                                          %open new window
%set(gcf,'Renderer','OpenGL')                                                   %alternative renderer (greater speed on some systems)
hold on                                                                         %keep open to plot multiple links, joints, frames, etc.
view(3)                                                                         %usually a good viewpoint
grid                                                                            %good background
if optimal_axes ~= 'Y'                
    %approximate size of figure axes from robot dimensions
    axis equal                                                                  %maintain realistic dimensions
    len = sum(dh.a(1:length(dh.a)-1)) + sum(dh.d(2:length(dh.d)));              %add up common normals and offsets
    axis_dims = 1.3*len*[-1/2, 1/2,-1/2, 1/2, -1/4, 1/2];                           %estimate coordinate axis dimensions
else
    %optimize figure axes by executing motion - use for final output
    frame_type_opt = char('P'*(frame_type == 'F'));                             %use plain axes for optimal workspace (faster)
    frame.type = frame_type_opt;                                                %update frame type to use plain axes
    for i = 1:size(q,1)                                                         %for each pose
        dh.t = q(i,:);                                                          %set pose
        cla                                                                     %clear axes
        draw_robot_serial(dh,joint,frame,part)                                  %draw robot
        drawnow                                                                 %dump buffer (else following 'axis' commands may cause warnings)
        axis equal                                                              %1:1:1 aspect ratios
        axis tight                                                              %best detail for figure dimensions
        if (i==1)                                                               %initialize axis_dims
            axis_dims = axis;                                                   %get axis dimensions
        end %if            
        
        %adjust axis values to capture all configurations
        axis_dims_current = axis;                                               %get current axis dimensions
        axis_dims(1) = min(axis_dims(1),axis_dims_current(1));                  %minimum x axis value
        axis_dims(2) = max(axis_dims(2),axis_dims_current(2));                  %maximum x axis value
        axis_dims(3) = min(axis_dims(3),axis_dims_current(3));                  %minimum y axis value
        axis_dims(4) = max(axis_dims(4),axis_dims_current(4));                  %maximum y axis value
        axis_dims(5) = min(axis_dims(5),axis_dims_current(5));                  %minimum z axis value
        axis_dims(6) = max(axis_dims(6),axis_dims_current(6));                  %maximum z axis value
    end %for i
    frame.type = frame_type;                                                    %restore original frame type 
end %if optimal_axes   
axis(axis_dims)                                                                 %set axis dimensions
%END{SET AXES DIMENSIONS}------------------------------------------------------

%BEGIN{DRAW ROBOT}*************************************************************
%draw robot in each position
for i=1:size(q,1)                                                               %for each pose (row)
        dh.t = q(i,:);                                                          %set pose
        cla                                                                     %clear previous position from figure buffer
        draw_robot_serial(dh,joint,frame,part)                                  %draw current robot position

        %figure lighting
        lighting phong                                                          %smooth lighting (use Gouraud for speed)
        material shiny                                                          %also try default, dull, metal
        h1=camlight(0,0);                                                       %good light angle

        %interactive user figure setup
        if i==1                                                                 %first pose
            pause;                                                              %allow user to reset figure
            camlight(h1,0,0);                                                   %update lighting angle after moving figure
        end %if i
    
        drawnow                                                                 %output current position from figure buffer
        if export_movie == 'Y'                                                  %generate movie
            M(:,i) = getframe;                                                  %save current frame to movie
        end %if export_movie
end %for i
%END{DRAW ROBOT}---------------------------------------------------------------

%BEGIN{EXPORT MOVIE}***********************************************************
%export movie to avi file
if export_movie == 'Y'                                                          %export movie
    movie2avi(M,[movie_name,'.avi'],'quality',100)                              %save movie, best quality (still sucks)
end %if export_movie
%END{EXPORT MOVIE}-------------------------------------------------------------
