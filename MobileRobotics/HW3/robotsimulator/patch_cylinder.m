function patch_cylinder(part,pt,zmin,zmax,T)

%DESCRIPTION
%   This function takes a planar polygon profile,
%   extrudes it into a cylinder along the normal direction, 
%   and displaces the cylinder using a homogenous transformation matrix.
%
%   Uses the "patch" function method by specifying the coordinates of each unique vertex and a
%   matrix that specifies how to connect these vertices to form the faces.
%
%INPUT ARGUMENTS
%   part ----------- property constants of all joint and link types  (struct)
%      .existalpha - 'Y'/'N' - transparency supported                (string)
%      .facealpha -- (0 to 1)- transparency of faces                 (scalar)
%      .edgealpha -- (0 to 1)- transparency of edges                 (scalar)
%   pt ------------- property constants for current joint or link    (struct)
%      .color ------ {bottom, side, top} colors                      (3x1 - one face colorspec, 3x3 - different face colorspecs) 
%      .edgecolor -- {bottom, side, top} edge colors                 (3x1 cell array of characters)
%      .x ---------- local x coords of polygon cross section         (vector)
%      .y ---------- local y coords of polygon cross section         (vector)
%   zmin ----------- local z coord for top                           (scalar)
%   zmin ----------- local z coord for bottom                        (scalar)
%   T -------------- part location (directed along z axis)           (4x4)
%
%OUTPUT ARGUMENTS
%   (none)
%
%SUBPROGRAM CALLS 
%   (none)

%PROGRAM

%BEGIN{INITIALIZATIONS}********************************************************
%set colors
if  size(pt.color) == [3 3];                          %separate colors for bottom, side, and top of extrusion
    bottom_color = pt.color(1,:);                     %color for bottom of extrusion
    side_color   = pt.color(2,:);                     %color for side   of extrusion
    top_color    = pt.color(3,:);                     %color for top    of extrusion
else                                                  %one color       for bottom, side, and top of extrusion    
    bottom_color = pt.color;                          %color for bottom of extrusion
    side_color   = pt.color;                          %color for side   of extrusion
    top_color    = pt.color;                          %color for top    of extrusion
end %if size(pt.color)
     
%set edges (default is black)
if part.edges == 'Y'                                  %draw edges
    b_edgecol = pt.edgecolor{1};                      %bottom edge color
    s_edgecol = pt.edgecolor{2};                      %side   edge color
    t_edgecol = pt.edgecolor{3};                      %top    edge color
else                                                  %draw no edges
    b_edgecol = 'none';                               %no    bottom edge
    s_edgecol = 'none';                               %no    side   edge
    t_edgecol = 'none';                               %no    top    edge
end %if edge
    
%reshape polygon data
n = length(pt.x);                                     %number of vertices
pt.x = reshape(pt.x,n,1);                             %make x a column vector
pt.y = reshape(pt.y,n,1);                             %make y a column vector
zbottom(1:n,:) = zmin;                                %make z coords for extrusion bottom
ztop(1:n,:)    = zmax;                                %make z coords for extrusion top

%extract rotation and translation
Rt = T(1:3,1:3)';                                     %rotation matrix transposed
pv = repmat(T(1:3,4)',[n,1]);                         %position vector transposed and replicated n times
%END{INITIALIZATIONS}----------------------------------------------------------

%BEGIN{FORM CYLINDER PATCHES}**************************************************
%create vertices for top and bottom of extrusion
vertex_matrix_bottom = [pt.x,pt.y,zbottom]*Rt+pv;     %all bottom vertices
vertex_matrix_top    = [pt.x,pt.y,ztop]   *Rt+pv;     %all top vertices

%create face matrix for top and bottom of extrusion
face_matrix_bottom   = 1:n;                           %bottom is a single patch
face_matrix_top      = 1:n;                           %top    is a single patch

%create vertex matrix (2n x 3) for sides of extrusion (n rectangular patches)
vertex_matrix_side   = [vertex_matrix_bottom; vertex_matrix_top];

%create face matrix for sides of extrusion (n rectangular patches)
face_matrix_side     = [face_matrix_bottom;           %bottom vertices (unshifted)
                        face_matrix_bottom(:,[2:n,1]);%bottom vertices shifted by one
                        face_matrix_top(:,[2:n,1])+n; %top vertices above the bottom vertices shifted by one
                        face_matrix_top+n]';          %top vertices above the bottom vertices (unshifted)
%END{FORM CYLINDER PATCHES}----------------------------------------------------

%BEGIN{DRAW CYLINDER PATCHES}**************************************************
%draw cylinder
if part.existalpha == 'Y'                             %transparency supported
    %draw bottom face
    patch('Vertices'         ,vertex_matrix_bottom,...
          'Faces'            ,face_matrix_bottom,...
          'FaceVertexCData'  ,repmat(bottom_color,[length(face_matrix_bottom),1]),...
          'FaceColor'        ,'interp',...
          'EdgeColor'        ,b_edgecol,...
          'FaceAlpha'        ,part.facealpha,...
          'EdgeAlpha'        ,part.edgealpha...
          ) %end patch

    %draw top face
    patch('Vertices'         ,vertex_matrix_top,...
          'Faces'            ,face_matrix_top,...
          'FaceVertexCData'  ,repmat(bottom_color,[length(face_matrix_top),1]),...
          'FaceColor'        ,'interp',...
          'EdgeColor'        ,t_edgecol,...
          'FaceAlpha'        ,part.facealpha,...
          'EdgeAlpha'        ,part.edgealpha...
          ) %end patch
                
    %draw side faces
    patch('Vertices'         ,vertex_matrix_side,...
          'Faces'            ,face_matrix_side,...
          'FaceVertexCData'  ,repmat(side_color,[length(vertex_matrix_side),1]),...
          'FaceColor'        ,'interp',...
          'EdgeColor'        ,s_edgecol,...
          'FaceAlpha'        ,part.facealpha,...
          'EdgeAlpha'        ,part.edgealpha...
          ) %end patch
else                                                  %transparency not supported
    %draw bottom face
    patch('Vertices',vertex_matrix_bottom,...
          'Faces'            ,face_matrix_bottom,...
          'FaceVertexCData'  ,repmat(bottom_color,[length(face_matrix_bottom),1]),...
          'FaceColor'        ,'interp',...
          'EdgeColor'        ,b_edgecol...
          ) %end patch

    %draw top face
    patch('Vertices'         ,vertex_matrix_top,...
          'Faces'            ,face_matrix_top,...
          'FaceVertexCData'  ,repmat(bottom_color,[length(face_matrix_top),1]),...
          'FaceColor'        ,'interp',...
          'EdgeColor'        ,t_edgecol...
          ) %end patch
                
    %draw side faces
    patch('Vertices'         ,vertex_matrix_side,...
          'Faces'            ,face_matrix_side,...
          'FaceVertexCData'  ,repmat(side_color,[length(vertex_matrix_side),1]),...
          'FaceColor'        ,'interp',...
          'EdgeColor'        ,s_edgecol...
          ) %end patch
end %if rel_min
%END{DRAW CYLINDER PATCHES}----------------------------------------------------

               
