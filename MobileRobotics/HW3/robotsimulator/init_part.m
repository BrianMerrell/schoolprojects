function pt = init_part(R_rad,link_type)

%BEGIN{USER OPTIONS} - DEFAULTS GOOD FOR MOST APPLICATIONS*********************                                    
%basic part options - good for most applications
pt.default.color      = [1.0000 0.6324 0.4027];                                 %color of default part (nice copper color with lighting)
pt.default.sides      = 20;                                                     %number of sides for cylindrical looking parts
pt.default.edgecolor  = {'k','none','k'};                                       %{bottom, side, top} edge colors for default
pt.edges              = 'Y';                                                    %'Y'/'N' - edges/no edges on all parts
pt.facealpha          = 1;                                                      %0 to 1, - opaque faces (default)
pt.edgealpha          = 1;                                                      %0 to 1, - opaque faces (default)
pt.existalpha         = 'Y';                                                    %'Y'/'N' - transparency supported (default)

%check if transparency is supported
rel_min = 12;                                                                   %mininum release number to support transparency
ver_str = version;                                                              %version string
rel_num = str2num(ver_str(find(ver_str=='R')+1:(find(ver_str==')')-1)));        %release number (located between 'R' and ')')
if rel_num < rel_min                                                            %check if transparency supported
    pt.existalpha     = 'N';                                                    %transparency not supported
end %rel_num

%more options
pt.R.scale.rad        = 1;                                                      %ratio of R joint           radius to R joint radius (included for uniformity)
pt.P.scale.rad        = 1.25;                                                   %ratio of P joint (outer)   radius to R joint radius
pt.G.scale.rad        = 1;                                                      %ratio of G joint           radius to R joint radius 
pt.B.scale.rad        = 2;                                                      %ratio of B joint (bottom)  radius to R joint radius
pt.a.scale.rad        = 9/16;                                                   %ratio of a link            radius to R joint radius
pt.d.scale.rad        = pt.a.scale.rad;                                         %ratio of d offset          radius to R joint radius
pt.p.scale.rad        = pt.a.scale.rad*pt.P.scale.rad;                          %ratio of p offset          radius to R joint radius
    
pt.R.scale.ht         = 3;                                                      %ratio of R joint height           to R joint radius
pt.P.scale.ht         = pt.R.scale.ht;                                          %ratio of P joint height           to R joint radius
pt.G.scale.ht         = 9/16;                                                   %ratio of G joint height           to R joint radius
pt.B.scale.ht         = 0.25;                                                   %ratio of B joint height (bottom)  to R joint radius

pt.R.color            = pt.default.color;                                       %color of R joint
pt.P.color            = pt.default.color;                                       %color of P joint
pt.G.color            = pt.default.color;                                       %color of G joint
pt.B.color            = pt.default.color;                                       %color of B joint (bottom)
pt.a.color            = pt.default.color;                                       %color of a link
pt.d.color            = pt.default.color;                                       %color of d offset
pt.p.color            = pt.default.color;                                       %color of p offset

pt.R.sides            = pt.default.sides;                                       %number of sides of R joint
pt.P.sides            = 4;                                                      %number of sides of P joint
pt.G.sides            = pt.default.sides;                                       %number of sides of G joint
pt.B.sides            = pt.default.sides;                                       %number of sides of B joint (bottom)
pt.a.sides            = pt.default.sides;                                       %number of sides of a link
pt.d.sides            = pt.default.sides;                                       %number of sides of d offset
pt.p.sides            = pt.P.sides;                                             %number of sides of p offset

pt.R.edgecolor        = pt.default.edgecolor;                                   %{bottom, side, top} edge colors for R joint
pt.P.edgecolor        = {'k','k','k'};                                          %{bottom, side, top} edge colors for P joint
pt.G.edgecolor        = pt.default.edgecolor;                                   %{bottom, side, top} edge colors for G joint
pt.B.edgecolor        = pt.default.edgecolor;                                   %{bottom, side, top} edge colors for B joint (bottom)
pt.a.edgecolor        = pt.default.edgecolor;                                   %{bottom, side, top} edge colors for a link
pt.d.edgecolor        = pt.default.edgecolor;                                   %{bottom, side, top} edge colors for d offset
pt.p.edgecolor        = pt.P.edgecolor;                                         %{bottom, side, top} edge colors for p offset

%make changes if line style selected for links and offsets
if link_type == 'L'                                                             %line style for links and offsets selected
    pt.fr.sh.scale.rad= 3/20;                                                   %ratio of frame arrow shaft radius to R joint radius
    pt.a.scale.rad    = 3/40;                                                   %ratio of a link            radius to R joint radius
    pt.d.scale.rad    = pt.a.scale.rad;                                         %ratio of d offset          radius to R joint radius
    pt.p.scale.rad    = pt.a.scale.rad*pt.P.scale.rad;                          %ratio of p offset          radius to R joint radius
    
    pt.a.color        = [0 0 0];                                                %color of a link  (black)
    pt.d.color        = [0 0 0];                                                %color of d offset(black) 
    pt.p.color        = [0 0 0];                                                %color of p offset(black)
end %if link_type

%END{USER OPTIONS} - DEFAULTS GOOD FOR MOST APPLICATIONS-----------------------                                    

%BEGIN{SET PARAMETERS}*********************************************************
%part types
pt.R.type = 'R';                                                                %R joint
pt.P.type = 'P';                                                                %P joint
pt.G.type = 'G';                                                                %G joint
pt.B.type = 'B';                                                                %B joint
pt.a.type = 'a';                                                                %a link
pt.d.type = 'd';                                                                %d offset
pt.p.type = 'p';                                                                %p offset

%part radii
pt.R.rad  = R_rad;                                                              %R joint  radius
pt.P.rad  = pt.R.rad*pt.P.scale.rad;                                            %P joint  radius
pt.G.rad  = pt.R.rad*pt.G.scale.rad;                                            %G joint  radius
pt.B.rad  = pt.R.rad*pt.B.scale.rad;                                            %B joint  radius (bottom)
pt.a.rad  = pt.R.rad*pt.a.scale.rad;                                            %a link   radius
pt.d.rad  = pt.R.rad*pt.d.scale.rad;                                            %d offset radius
pt.p.rad  = pt.R.rad*pt.p.scale.rad;                                            %p offset radius

%fixed part heights (a and d parts have variable heights)
pt.R.ht   = pt.R.rad*pt.R.scale.ht;                                             %R joint height
pt.P.ht   = pt.R.rad*pt.P.scale.ht;                                             %P joint height
pt.G.ht   = pt.R.rad*pt.G.scale.ht;                                             %G joint height
pt.B.ht   = pt.R.rad*pt.B.scale.ht;                                             %B joint height (bottom)

%generate "circular" cross sections
t         = linspace(0,2*pi,pt.default.sides+1)+(pi/pt.default.sides);          %generate evenly spaced vertices, offset by 1/2 side
c         = cos(t);	                                                            %unit polygon vertex x coords
s         = sin(t);                                                             %unit polygon vertex y coords
%for i = ['R','G','B','a','d','p']                                              %assign default (circular) cross sections
%    pt.(i).x = pt.(i).rad*c;                                                   %x coordinate
%    pt.(i).y = pt.(i).rad*s;                                                   %y coordinate
%end %for i
%following assignments needed for Version 5.3 instead of above code
pt.R.x    = pt.R.rad*c;                                                         %x coordinate
pt.R.y    = pt.R.rad*s;                                                         %y coordinate
pt.G.x    = pt.G.rad*c;                                                         %x coordinate
pt.G.y    = pt.G.rad*s;                                                         %y coordinate
pt.B.x    = pt.B.rad*c;                                                         %x coordinate
pt.B.y    = pt.B.rad*s;                                                         %y coordinate
pt.a.x    = pt.a.rad*c;                                                         %x coordinate
pt.a.y    = pt.a.rad*s;                                                         %y coordinate
pt.d.x    = pt.d.rad*c;                                                         %x coordinate
pt.d.y    = pt.d.rad*s;                                                         %y coordinate

%generate gripper cross sections, looks like --> C
n         = pt.G.sides;                                                         %number of points making up the gripper
n2        = floor(n/4);                                                         %number of points to move in x,y and x,-y quadrants
ypos      = [1:n2];                                                             %points to move in x, y quadrant
yneg      = [n:-1:n-n2+1];                                                      %points to move in x,-y quadrant
pt.G.x(:,ypos) = -pt.G.x(:,ypos)/2;                                             %shift x coords to make inside of gripper (x, y quadrant)
pt.G.y(:,ypos) =  pt.G.y(:,ypos)/2;                                             %shift y coords to make inside of gripper (x, y quadrant)
pt.G.x(:,yneg) = -pt.G.x(:,yneg)/2;                                             %shift x coords to make inside of gripper (x,-y quadrant)
pt.G.y(:,yneg) =  pt.G.y(:,yneg)/2;                                             %shift y coords to make inside of gripper (x,-y quadrant)
pt.G.x(:,n+1)  =  pt.G.x(:,1);                                                  %make first and last point the same to close the polygon
pt.G.y(:,n+1)  =  pt.G.y(:,1);                                                  %make first and last point the same to close the polygon

%generate square cross sections for P joint and p offset
t         = linspace(0,2*pi,pt.P.sides+1)+(pi/pt.P.sides);                      %generate evenly spaced vertices, offset by 1/2 side
c         = cos(t);	                                                            %unit polygon vertex x coords
s         = sin(t);                                                             %unit polygon vertex y coords
pt.P.x    = pt.P.rad*c;                                                         %x coordinate
pt.P.y    = pt.P.rad*s;                                                         %y coordinate
pt.p.x    = pt.p.rad*c;                                                         %x coordinate
pt.p.y    = pt.p.rad*s;                                                         %y coordinate
%END{SET PARAMETERS}-----------------------------------------------------------

