function draw_frame(fr, T, sub, I)

%DESCRIPTION
%   This function draws a fancy or plain coordinate frame 
%   located by a homogeneous transformation matrix T.
%
%INPUT ARGUMENTS
%   fr ------------- frame characteristics                           (struct)
%      .type ------- F/P/N - ith frame (0..n+1) is Fancy/Plain/None  (N character string)
%      .edges ------ Y/N - Edges/None                                (scalar)
%      .height ----- length of axes                                  (scalar)
%      .sh ... ----- shaft parameters                                (struct)
%      .ah ... ----- arrowhead parameters                            (struct)
%   T -------------- homogeneous transformation matrix for frame     (4x4)
%   sub ------------ x,y,z axes subscript label                      (string)
%   I -------------- indexes frames (I = 1...N)/(i = 0...n)          (scalar)
%
%OUTPUT ARGUMENTS
%   (none)
%
%FUNCTION CALLS 
%   draw_cylinder
%   draw_cone

%PROGRAM


%BEGIN{INITIALIZATIONS}********************************************************
%extract location data
O3 = repmat(T(1:3,4),1,3);                                                        %replicate origin coords into 3 cols
R = T(1:3,1:3);                                                                   %extract unit vectors in x,y,z directions
%END{INITIALIZATIONS}----------------------------------------------------------

%BEGIN{SELECT STYLE AND DRAW FRAMES}*******************************************
if fr.type(I) == 'F'                                                              %Fancy 3D style coordinate axes    
    %BEGIN{FANCY FRAMES - DRAW AXES}*******************************************
    Tx = T*rot('y', pi/2);                                                        %rotates x axis into z axis
    Ty = T*rot('x',-pi/2);                                                        %rotates y axis into z axis

    %draw shafts
    fr.sh.edgecolor = fr.edgecolor;                                               %set data field for patch_cylinder compatibility
    fr.sh.color     = fr.sh.xcolor;                                               %set data field for patch_cylinder compatibility
    patch_cylinder(fr,fr.sh,fr.sh.bot,fr.sh.top,Tx);                              %plot x axis shaft
    fr.sh.color     = fr.sh.ycolor;                                               %set data field for patch_cylinder compatibility
    patch_cylinder(fr,fr.sh,fr.sh.bot,fr.sh.top,Ty);                              %plot y axis shaft
    fr.sh.color     = fr.sh.zcolor;                                               %set data field for patch_cylinder compatibility
    patch_cylinder(fr,fr.sh,fr.sh.bot,fr.sh.top,T );                              %plot z axis shaft

    %draw arrowheads
    fr.ah.edgecolor = fr.edgecolor;                                               %set data field for patch_cylinder compatibility
    fr.ah.color     = fr.ah.xcolor;                                               %set data field for patch_cylinder compatibility
    patch_cone(fr,fr.ah,fr.ah.bot,fr.ah.top,Tx);                                  %plot x axis shaft
    fr.ah.color     = fr.sh.ycolor;                                               %set data field for patch_cylinder compatibility
    patch_cone(fr,fr.ah,fr.ah.bot,fr.ah.top,Ty);                                  %plot y axis shaft
    fr.ah.color     = fr.sh.zcolor;                                               %set data field for patch_cylinder compatibility
    patch_cone(fr,fr.ah,fr.ah.bot,fr.ah.top,T );                                  %plot z axis shaft
    %END{FANCY FRAMES -  DRAW AXES}---------------------------------------------
else                                                                              %simple axes using lines
    %BEGIN{PLAIN FRAMES - DRAW AXES}********************************************
    x = [O3(1,:);O3(1,:)+ fr.height*R(1,1:3)];                                    %x coords of x,y,z axis lines
    y = [O3(2,:);O3(2,:)+ fr.height*R(2,1:3)];                                    %y coords of x,y,z axis lines
    z = [O3(3,:);O3(3,:)+ fr.height*R(3,1:3)];                                    %z coords of x,y,z axis lines
    line(x, y, z,'Marker','.','MarkerSize',12,'LineWidth',1)                      %plot axes
    plot3(T(1,4),T(2,4),T(3,4),'Marker','.','MarkerSize',12,'Color','k')          %plot origin
    %END{PLAIN FRAMES -  DRAW AXES}---------------------------------------------    
end %if
%END{SELECT STYLE AND DRAW FRAMES}----------------------------------------------

%BEGIN{PLOT LABELS}*************************************************************
x_l = [O3(1,:)+ fr.height*R(1,1:3)];                                              %x coords of x,y,z axes ends
y_l = [O3(2,:)+ fr.height*R(2,1:3)];                                              %y coords of x,y,z axes ends
z_l = [O3(3,:)+ fr.height*R(3,1:3)];                                              %z coords of x,y,z axes ends
fs  = ['\fontname{times}\fontsize{12}'];                                          %fontspec
ss  = ['_{',sub,'}'];                                                             %add subscript 
text(x_l,y_l,z_l,[[fs,'{\it x}',ss];[fs,'{\it y}',ss];[fs,'{\it z}',ss]])         %plot labels
%END{PLOT LABELS}---------------------------------------------------------------
