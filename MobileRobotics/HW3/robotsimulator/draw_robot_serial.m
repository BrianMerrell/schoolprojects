function draw_robot_serial(dh,jt,fr,pt,varargin)

%DESCRIPTION
% This function draws a serial robot from the Denavit-Hartenberg parameters.
%
% Conventions:
%   joint - refers to revolute(R), prismatic(P), base(B), gripper(G), none (N) "generalized" joints 
%           There are N joints array-indexed from 1 to N or logically indexed from 0 to n (where N = n+1).
%
%INPUT ARGUMENTS
%   dh ------------- Denavit-Hartenberg parameters                   (struct)
%      .t ---------- theta joint angles in radians                   (N vector)
%      .d ---------- joint offsets                                   (N vector)
%      .f ---------- alpha twist angles in radians                   (N vector)
%      .a ---------- common normal link lengths                      (N vector)
%      .T ---------- homogeneous transformation matrices             (N cell array of 4x4 matrices)
%   jt ------------- joint characteristics                           (struct)                     
%      .type ------- B/R/P/G/N - Base/Revolute/Prismatic/Gripper/None(N character string)
%      .centered --- C/T/M - Center/Top/center of joint at           (N character string)
%                            o(i)/o(i)/(Midpoint d(i))(0...n+1)
%   fr ------------- frame characteristics                           (struct)
%      .type ------- F/P/N - ith frame (0..n+1) is Fancy/Plain/None  (N character string)
%      .edges ------ Y/N - Edges/None                                (scalar)
%   pt ------------- part properties for all joint and link types    (struct)
%   varargin ------- for platforms (not currently implemented)
%      (1) Hx ------ x coordinates of platform vertices              (vector)
%      (2) Hy ------ y coordinates of platform vertices              (vector)
%
%OUTPUT ARGUMENTS
%   (none)
%
%FUNCTION CALLS 
%   draw_frame ---- draw coordinate frame
%   draw_part ----- draw joint or link
%   DHtrans ------- form homogeneous transformation matrix from Denavit-Hartenberg parameters

%PROGRAM

%BEGIN{SET PARAMETERS}*********************************************************
N         = length(dh.t);                                   %last joint index; logically indexed from 1 instead of 0 
n         = N-1;                                            %last joint index; logically indexed from 0

%set joint locations along offsets d(i) for visual effect
s = (dh.d>=0)-(dh.d<0);                                     %nonzero sign of d (+1/-1),for properly extending the offsets when negative
for i = 0:n                                                 %center joints (B joint results not used later)
    I = i+1;                                                %I indexes from 1; i (logically) indexes from 0    
    if jt.centered(I) == 'T'                                %joint top at o(i) (better frame visibility)
        jt.bot(I) = s(I)* -pt.R.ht;                         %bottom of joint
        jt.top(I) = s(I)*0;                                 %top    of joint
    elseif jt.centered(I) == 'C'                            %joint centered about o(i)
        jt.bot(I) = s(I)*-pt.R.ht/2;                        %bottom of joint
        jt.top(I) = s(I)* pt.R.ht/2;                        %top    of joint
    else                                                    %joint centered about d(i)
        jt.bot(I) = s(I)*-pt.R.ht/2-dh.d(I)/2;              %bottom of joint
        jt.top(I) = s(I)* pt.R.ht/2-dh.d(I)/2;              %top    of joint
    end %if jt.centered

%correct above centering for G joint
    if jt.type(I) == 'G'                                    %gripper always centered about o(i)
        jt.bot(I) = -pt.G.ht/2;                             %bottom of gripper
        jt.top(I) =  pt.G.ht/2;                             %top    of gripper
    end %if jt.type

%correct above centering for B joint
    if jt.type(I) == 'B'                                    %B joint bottom always centered about o(0)
        jt.bot(I) = 0;                                      %B joint bottom
        jt.top(I) = pt.B.ht;                                %B joint top
    end %if jt.type
end %for I

%extend offset and common normal dimensions for visual effect
%  note: if a(i)=0 or d(i)=0 the parts are not drawn
lk.a.bot    = 0-pt.a.rad*ones(size(dh.a));                  %start of links   (extend by pt.a.rad)
lk.a.top    = dh.a+pt.a.rad;                                %end   of links   (extend by pt.a.rad)
lk.d.bot    = -(dh.d+s*pt.d.rad);                           %start of offsets (extend by pt.d.rad)
lk.d.top    =  (0+s*pt.d.rad);                              %end   of offsets (extend by pt.d.rad)
lk.d.bot(2) = -dh.d(2);                                     %don't adjust start of offset i=1   (I=2)
lk.d.top(N) = 0;                                            %don't adjust end   of offset i=n+1 (I=N)

%alter offset and common normal dimensions for visual effect due to a gripper
%  this is a bit kludgey but seems to get the most common gripper configurations
if jt.type(N) ~= 'G'                                        %no gripper present
    lk.a.top(N-1) =  dh.a(N-1);                             %undo extending end of common normal i=n-1 (I=N-1)
else                                                        %gripper present
    if dh.d(N) == 0                                         %gripper offset is 0, next check if last nonzero link is a(N-1) = a(n)
        if dh.a(N-1) == 0                                   %gripper common normal a(N-1) = a(n) is 0, assume gripper on last joint offset d(N-1)=d(n)
            lk.d.top(N-1) = 0-pt.G.rad*0.75;                %adjust offset for gripper (retract by 75% of pt.G.rad)
            if lk.d.top(N-1)<jt.top(N-1)                    %check if gripper obscured by last joint
                jt.top(N-1) = s(N-1)*lk.d.top(N-1);         %shift end   of joint back
                jt.bot(N-1) = s(N-1)*lk.d.top(N-1)-pt.R.ht; %shift start of joint back
            end %if d_max(N)
        else                                                %gripper mounted on the last common normal
            lk.a.top(N-1) = dh.a(N-1)-pt.G.rad*0.75;        %adjust previous offset for gripper (retract by 75% of pt.G.rad)
        end %if a(N-1)
    else                                                    %gripper offset is nonzero
        lk.d.top(N) = -(s(N)*pt.G.ht*0.5);                  %adjust last offset for gripper (retract by 50% of pt.G.ht)
    end %if d(N)
end %if jt.type(N)
%END{SET PARAMETERS}-----------------------------------------------------------

%BEGIN{DRAW FIGURE}************************************************************
%draw base frame
if (fr.type(1)=='F')|(fr.type(1)=='P');                     %draw base frame (0th)
     draw_frame(fr, dh.T{1}, '0', 1); %draw it
end %if 

%draw pedestal as a lower and an upper B joint
if  jt.type(1)=='B'           %                             %draw a base pedestal (0th joint)
    draw_part(pt.B, jt, 1, dh.T{1}, pt)                     %draw lower B joint
    %store lower B joint values
    holdtop = jt.top(1);                                    %store top     of lower B joint
    holdbot = jt.bot(1);                                    %store bottom  of lower B joint
    holdrad = pt.B.rad;                                     %store radius  of lower B joint
    holdx   = pt.B.x;                                       %store x coord of lower B joint
    holdy   = pt.B.y;                                       %store y coord of lower B joint
    %form  upper B joint values
    jt.top(1) = jt.top(1)+pt.B.ht;                          %move up lower B joint top    by its height
    jt.bot(1) = jt.bot(1)+pt.B.ht;                          %move up lower B joint bottom by its height
    pt.B.rad  = 0.875*pt.B.rad;                             %reduce  lower B joint radius for visual effect
    pt.B.x    = 0.875*pt.B.x;                               %reduce  lower B joint radius for visual effect
    pt.B.y    = 0.875*pt.B.y;                               %reduce  lower B joint radius for visual effect
    draw_part(pt.B, jt, 1, dh.T{1}, pt)                     %draw upper B joint
    %restore lower B joint values
    jt.top(1) = holdtop;                                    %restore top      of lower B joint
    jt.bot(1) = holdbot;                                    %restore bottom   of lower B joint
    pt.B.rad  = holdrad;                                    %restore radius   of lower B joint
    pt.B.x    = holdx;                                      %restore x coords of lower B joint
    pt.B.y    = holdy;                                      %restore y coords of lower B joint
end %if jt.type(1)

%plot robot and frames
for i=1:n                                                   %i counts the RPG joints and frame number beyond base
    I    = i+1;                                             %I indexes vectors logically indexed from 0 instead of 1
    dh.T{I}  = dh.T{I-1}*DHtrans(dh.a(I-1),dh.f(I-1),dh.d(I),dh.t(I));   %ith frame location

    %draw link a(i-1)
    if dh.a(I-1)>0;                                         %draw link if a(i-1) > 0
        draw_part(pt.a, lk.a, I-1, dh.T{I-1}, pt)           %draw a link
    end %if

    %draw offset d(i)
    if dh.d(I)~=0;                                          %don't draw if d(i) = 0
        if jt.type(I) ~= 'P'                                %not a P joint so make cross section circular
            draw_part(pt.d, lk.d, I, dh.T{I}, pt)           %draw d offset
        else                                                %offset on P joint so make cross section square
            draw_part(pt.p, lk.d, I, dh.T{I}, pt)           %draw p offset
        end %if jt.type          
    end %if d(I)

    %draw joint(i)
    switch jt.type(I)
    case{'R'}
       draw_part(pt.R, jt, I, dh.T{I}, pt)                  %draw joint
    case{'P'}
       draw_part(pt.P, jt, I, dh.T{I}, pt)                  %draw joint
    case{'G'}
       draw_part(pt.G, jt, I, dh.T{I}, pt)                  %draw joint
    case{'B'}
       draw_part(pt.B, jt, I, dh.T{I}, pt)                  %draw joint
    end %switch
    
    %draw frame(i)
    if (fr.type(I)=='F')|(fr.type(I)=='P');                 %plot frame(i)
        draw_frame(fr, dh.T{I}, num2str(i),I); %draw frame(i)
    end %if frame_type(i)             
end %for i
%END{DRAW FIGURE}--------------------------------------------------------------

