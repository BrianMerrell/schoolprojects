classdef tfm 
    
    properties
        A;
    end
    
    methods
        function obj = tfm(gamma, beta, alpha, x, y, z)

            B = euler2A(3,2,1,alpha,beta,gamma);
            P = [x; y; z];
            obj.A = [B' P; 0 0 0 1];
        end
        
        function transpose = T(obj)
            R = obj.A(1:3,1:3);
            P = obj.A(1:3,4);
            
            transpose = [R' -R'*P; 0 0 0 1];
        end
    end
end

