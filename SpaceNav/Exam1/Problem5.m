clc
close all
clear all

% Set the initial values for rhat and P
rhat = [0; 0; 90];
drhat = [0 0 0]';
P = eye(3) 

% Set up loop to process each measurement

    switch type
        case 1
            qhat = acos(-dot(i_RL4,i1));
            h = 1/(norm(RL4)*sin(qhat))*(cos(qhat)*i_RL4+i1);
        case 2
            qhat = acos(-dot(i_RL4,i2));
            h = 1/(norm(RL4)*sin(qhat))*(cos(qhat)*i_RL4+i2);
        case 3
            qhat = acos(-dot(i_RL4,i3));
            h = 1/(norm(RL4)*sin(qhat))*(cos(qhat)*i_RL4+i3);
        case 4
            qhat = 2*acos(sqrt(1-(Earth_D/(2*norm(RL4)))^2));
            h = -Earth_D/(norm(RL4)^2*sqrt(1-(Earth_D/(2*norm(RL4)))^2))*i_RL4;
        case 5
            qhat = acos(-dot(i_ML4,i1));
            h = 1/(norm(ML4)*sin(qhat))*(cos(qhat)*i_ML4+i1);
        case 6
            qhat = acos(-dot(i_ML4,i2));
            h = 1/(norm(ML4)*sin(qhat))*(cos(qhat)*i_ML4+i2);
        case 7
            qhat = acos(-dot(i_ML4,i3));
            h = 1/(norm(ML4)*sin(qhat))*(cos(qhat)*i_ML4+i3);
        case 8
            qhat = 2*acos(sqrt(1-(Moon_D/(2*norm(ML4)))^2));
            h = -Moon_D/(norm(ML4)^2*sqrt(1-(Moon_D/(2*norm(ML4)))^2))*i_ML4;
        otherwise
            disp('type error')
    end
    
    % Compute w
    w  = P*h/(sig1^2 + h'*P*h);
    rhat =  rhat + w*(measurement-qhat);
    P = (eye(3) - w*h')*P;
    RL4 = rhat;
    
    rdata(:,i) = rhat-RL40;
end    

drhat = rhat - RL40
plot(rdata','r');