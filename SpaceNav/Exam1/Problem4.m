clc
close all
clear all



data = [1.1; 1.95; 3.05; 3.98];
data = [data [1; 2; 3; 4]];
data = [data [16; 4; 1; 9]];


%% Set up loop to process each measurement
for i=1:4
    measurement = data(i,1);
    q0 = data(i,2);
    sig = data(i,3);
    
    switch i
        case 1
            h = [2 3]';
            dq = measurement - q0;
        case 2
            h = [1 3]'*pi/180;
            dq = (measurement-q0)*pi/180;
            sig = deg2rad(sig);
        case 3
            h = [2 1]';
            dq = measurement - q0;
        case 4
            h = [0 1]'*pi/180;
            dq = (measurement-q0)*pi/180;
            sig = deg2rad(sig);
        otherwise
            disp('type error')
    end
    
    if (i==1) % set up the matrices H, A, and DQ
        HT = h;
        Ainv = 1/sig^2;
        DQ = dq;
    else
        HT = [HT h];
        Ainv = [Ainv 1/sig^2];
        DQ = [DQ dq];
    end
    
end
%compute deltaR here
Ainv = diag(Ainv);
dR = (HT*Ainv*HT')\HT*Ainv*DQ'

sigma = HT*Ainv*HT';
errorBox = sqrt(diag(inv(sigma)));

% -107.2684

