HT = [0 0 1];
qhat = 101;
q = 100;
sig = .1;
rhat = [0; 0; 90];
P = eye(3);

w = P*HT'/(sig^2 + HT*P*HT');

rstar = rhat - w*(q-qhat)
Pstar = (eye(3) - w*HT)*P