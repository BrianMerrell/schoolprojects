%% Problem 7 -- 1.10
clc
clear all
close all

x1 = 1; x2 = 1; x3 = 1;
t = 0:.1:1;
y = x1 +x2*sin(10*t) + x3*exp(2*t.^2);
y6 = fix(y*10^6)/10^6;
y4 = fix(y*10^4)/10^4;
y2 = fix(y*10^2)/10^2;
y1 = fix(y*10)/10;

sig1 = std(y1);
sig2 = std(y2);
sig4 = std(y4);
sig6 = std(y6);



x0 = [.75;.75;.75];
y = @(t,x0)([1 sin(10*t) exp(2*t^2)]*x0);

dr0 = [0;0;0];
P = diag([1e8 1e8 1e8]);

h = @(t)([1 sin(10*t) exp(2*t^2)]');
j = 1;
for i = t
    dqt = y6(j) - y(i,x0);
    dqh = h(i)'*dr0;
    w6 = P*h(i)/(sig6^2 + h(i)'*P*h(i));
    
    dr = dr0 + w6*(dqt-dqh);
    dr0 = dr;
    x = x0 + dr;
    P = (eye(3) - w6*h(i)')*P;
    
    j = j+1;
end
P6 = P;
x6 = x

P = diag([1e8 1e8 1e8]);
dr0 = [0;0;0];
j = 1;
for i = t
    dqt = y4(j) - y(i,x0);
    dqh = h(i)'*dr0;
    w4  = P*h(i)/(sig4^2 + h(i)'*P*h(i));
    
    dr = dr0 + w4*(dqt-dqh);
    dr0 = dr;
    x = x0 + dr;
    P = (eye(3) - w4*h(i)')*P;
    
    j = j+1;
end
P4 = P;
x4 = x

P = diag([1e8 1e8 1e8]);
dr0 = [0;0;0];
j = 1;
for i = t
    dqt = y2(j) - y(i,x0);
    dqh = h(i)'*dr0;
    w2  = P*h(i)/(sig2^2 + h(i)'*P*h(i));
    
    dr = dr0 + w2*(dqt-dqh);
    dr0 = dr;
    x = x0 + dr;
    P = (eye(3) - w2*h(i)')*P;
    
    j = j+1;
end
P2 = P;
x2 = x

P = diag([1e8 1e8 1e8]);
dr0 = [0;0;0];
j = 1;
for i = t
    dqt = y1(j) - y(i,x0);
    dqh = h(i)'*dr0;
    w1  = P*h(i)/(sig6^2 + h(i)'*P*h(i));
    
    dr = dr0 + w1*(dqt-dqh);
    dr0 = dr;
    x = x0 + dr;
    P = (eye(3) - w1*h(i)')*P;
    
    j = j+1;
end
P1 = P;
x1 = x





