clc
close all
clear all
RL4 = [333415  192500  0]';% KM
Earth_D = 2*6378;% KM
Moon_D = 2*1737;% KM

Rm = [0 385000 0]';% KM
i1 = [sqrt(2)/2 0 sqrt(2)/2]';
i2 = [-sqrt(2)/2 sqrt(2)/2 0]';
i3 = [sqrt(3)/3  sqrt(3)/3 sqrt(3)/3]';
load Data1.mat
data=m;

ML4=(RL4-Rm);
i_RL4 = RL4/norm(RL4);
i_ML4 = ML4/norm(ML4);
% Set up loop to process each measurement
for i=1:max(size(data))

    %get type, measurement, and std dev
    type=data(i,2);
    measurement=deg2rad(data(i,3));
    sig1=1;
    % setup switch for each posible type of measurement

    switch type
        case 1
            q0 = acos(-dot(i_RL4,i1));
            h = 1/(norm(RL4)*sin(q0))*(cos(q0)*i_RL4+i1);
            dq = measurement-q0;
        case 2
            q0 = acos(-dot(i_RL4,i2));
            h = 1/(norm(RL4)*sin(q0))*(cos(q0)*i_RL4+i2);
            dq = measurement-q0;
        case 3
            q0 = acos(-dot(i_RL4,i3));
            h = 1/(norm(RL4)*sin(q0))*(cos(q0)*i_RL4+i3);
            dq = measurement-q0;
        case 4
            q0 = 2*acos(sqrt(1-(Earth_D/(2*norm(RL4)))^2));
            h = -Earth_D/(norm(RL4)^2*sqrt(1-(Earth_D/(2*norm(RL4)))^2))*i_RL4;
            dq = measurement-q0;
        case 5
            q0 = acos(-dot(i_ML4,i1));
            h = 1/(norm(ML4)*sin(q0))*(cos(q0)*i_ML4+i1);
            dq = measurement-q0;
        case 6
            q0 = acos(-dot(i_ML4,i2));
            h = 1/(norm(ML4)*sin(q0))*(cos(q0)*i_ML4+i2);
            dq = measurement-q0;
        case 7
            q0 = acos(-dot(i_ML4,i3));
            h = 1/(norm(ML4)*sin(q0))*(cos(q0)*i_ML4+i3);
            dq = measurement-q0;
        case 8
            q0 = 2*acos(sqrt(1-(Moon_D/(2*norm(ML4)))^2));
            h = -Moon_D/(norm(ML4)^2*sqrt(1-(Moon_D/(2*norm(ML4)))^2))*i_ML4;
            dq = measurement-q0;
        otherwise
            disp('type error')
    end

    if (i==1) % set up the matrices H, A, and DQ
        HT = h;
        Ainv = 1/sig1^2;
        DQ = dq;
    else
        HT = [HT h];
        Ainv = [Ainv 1/sig1^2];
        DQ = [DQ dq];
    end

end
%compute deltaR here
Ainv = Ainv.*eye(length(Ainv));
dR= (HT*HT')\HT*DQ'

