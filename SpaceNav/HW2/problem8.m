%% Problem 7 -- 1.10
clc
clear all
close all

x1 = 1; x2 = 1; x3 = 1;
t = 0:.1:1;
y = x1 +x2*sin(10*t) + x3*exp(2*t.^2);
y6 = fix(y*10^6)/10^6;
y4 = fix(y*10^4)/10^4;
y2 = fix(y*10^2)/10^2;
y1 = fix(y*10)/10;

sig1 = std(y1);
sig2 = std(y2);
sig4 = std(y4);
sig6 = std(y6);



x0 = [.75;.75;.75];
y = @(t,x0)([1 sin(10*t) exp(2*t^2)]*x0);

dr0 = [0;0;0];
h = @(t)([1 sin(10*t) exp(2*t^2)]');


for k = 1:1
    j = 1;
    for i = t       
        dq = y6(j)-y(i,x0);
        
        if j == 1
            HT = h(i)';
            Ainv = 1/sig6^2;
            DQ = dq;
        else
            HT = [HT; h(i)'];
            Ainv = [Ainv 1/sig6^2];
            DQ = [DQ; dq];
        end
        j = j+1;
    end
    Ainv = diag(Ainv);
    P = (HT'*Ainv*HT);
    dx = P\HT'*Ainv*DQ;
    x0 = x0+dx;
end
x6 = x0

x0 = [.75;.75;.75];
dr0 = [0;0;0];
for k = 1:1
    j = 1;
    for i = t       
        dq = y4(j)-y(i,x0);
        
        if j == 1
            HT = h(i)';
            Ainv = 1/sig4^2;
            DQ = dq;
        else
            HT = [HT; h(i)'];
            Ainv = [Ainv 1/sig4^2];
            DQ = [DQ; dq];
        end
        j = j+1;
    end
    Ainv = diag(Ainv);
    P = (HT'*Ainv*HT);
    dx = P\HT'*Ainv*DQ;
    x0 = x0+dx;
end
x4 = x0


x0 = [.75;.75;.75];
dr0 = [0;0;0];
for k = 1:1
    j = 1;
    for i = t       
        dq = y2(j)-y(i,x0);
        
        if j == 1
            HT = h(i)';
            Ainv = 1/sig2^2;
            DQ = dq;
        else
            HT = [HT; h(i)'];
            Ainv = [Ainv 1/sig2^2];
            DQ = [DQ; dq];
        end
        j = j+1;
    end
    Ainv = diag(Ainv);
    P = (HT'*Ainv*HT);
    dx = P\HT'*Ainv*DQ;
    x0 = x0+dx;
end
x2 = x0


x0 = [.75;.75;.75];
dr0 = [0;0;0];
for k = 1:1
    j = 1;
    for i = t       
        dq = y1(j)-y(i,x0);
        
        if j == 1
            HT = h(i)';
            Ainv = 1/sig1^2;
            DQ = dq;
        else
            HT = [HT; h(i)'];
            Ainv = [Ainv 1/sig1^2];
            DQ = [DQ; dq];
        end
        j = j+1;
    end
    Ainv = diag(Ainv);
    P = (HT'*Ainv*HT);
    dx = P\HT'*Ainv*DQ;
    x0 = x0+dx;
end
x1 = x0
    
   





