clc
close all
% clear all

% load Data1.mat
% data=m;

data = sortrows(data,2);

k = 1;
j=1;
for i = 1:length(data)
    if data(i,2) == 1
        type1(k,:) = data(i,:);
        k = k+1;
    elseif data(i,2) == 2
        type2(j,:) = data(i,:);
        j=j+1;
    end
end

mean1 = mean(type1(:,3));
std1 = std(type1(:,3));
std1_inst = type1(1,4);

mean2 = mean(type2(:,3));
std2 = std(type2(:,3));
std2_inst = type2(1,4);

hold on
title('Type 1 Data')
plot(type1(:,3),'-*')
plot([1 10], [mean1 mean1]);
plot([1 10], [mean1+std1 mean1+std1]);
plot([1 10], [mean1-std1 mean1-std1]);
plot([1 10], [mean1+std1_inst mean1+std1_inst]);
plot([1 10], [mean1-std1_inst mean1-std1_inst]);
legend('Angle Date', 'Mean', 'Mean + STD', 'Mean - STD', 'Mean + STD_Inst','Mean - STD_Inst', 'location', 'southwest');
xlabel('Measurement Number');
ylabel('Measured Angle (deg)');
hold off

figure
hold on
title('Type 2 Data')
plot(type2(:,3),'-*')
plot([1 10], [mean2 mean2]);
plot([1 10], [mean2+std2 mean2+std2]);
plot([1 10], [mean2-std2 mean2-std2]);
plot([1 10], [mean2+std2_inst mean2+std2_inst]);
plot([1 10], [mean2-std2_inst mean2-std2_inst]);
legend('Angle Date', 'Mean', 'Mean + STD', 'Mean - STD', 'Mean + STD_Inst','Mean - STD_Inst', 'location', 'southwest');
xlabel('Measurement Number');
ylabel('Measured Angle (deg)');
hold off
