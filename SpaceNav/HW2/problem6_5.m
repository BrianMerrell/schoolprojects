clc
RL40 = [333415  192500  0]';% KM
RL4 = [333415  192500  0]';% KM
Earth_D = 2*6378;% KM
Moon_D = 2*1737;% KM

Rm = [0 385000 0]';% KM
i1 = [sqrt(2)/2 0 sqrt(2)/2]';
i2 = [-sqrt(2)/2 sqrt(2)/2 0]';
i3 = [sqrt(3)/3  sqrt(3)/3 sqrt(3)/3]';
load Data2.mat
data = m;

ML4 = (RL4-Rm);
i_RL4 = RL4/norm(RL4);
i_ML4 = ML4/norm(ML4);
% Set the initial values for rhat and P
rhat = RL4;
drhat = [0 0 0]';
P = eye(3)*1e8; 

% Set up loop to process each measurement
for i=1:max(size(m))

    ML4 = (RL4-Rm);
    i_RL4 = RL4/norm(RL4);
    i_ML4 = ML4/norm(ML4);
    % get type, measurement, and std dev
    type=data(i,2);
    measurement=deg2rad(data(i,3));
    sig1=deg2rad(data(i,4));
    
    % setup switch for each posible type of measurement
    switch type
        case 1
            qhat = acos(-dot(i_RL4,i1));
            h = 1/(norm(RL4)*sin(qhat))*(cos(qhat)*i_RL4+i1);
        case 2
            qhat = acos(-dot(i_RL4,i2));
            h = 1/(norm(RL4)*sin(qhat))*(cos(qhat)*i_RL4+i2);
        case 3
            qhat = acos(-dot(i_RL4,i3));
            h = 1/(norm(RL4)*sin(qhat))*(cos(qhat)*i_RL4+i3);
        case 4
            qhat = 2*acos(sqrt(1-(Earth_D/(2*norm(RL4)))^2));
            h = -Earth_D/(norm(RL4)^2*sqrt(1-(Earth_D/(2*norm(RL4)))^2))*i_RL4;
        case 5
            qhat = acos(-dot(i_ML4,i1));
            h = 1/(norm(ML4)*sin(qhat))*(cos(qhat)*i_ML4+i1);
        case 6
            qhat = acos(-dot(i_ML4,i2));
            h = 1/(norm(ML4)*sin(qhat))*(cos(qhat)*i_ML4+i2);
        case 7
            qhat = acos(-dot(i_ML4,i3));
            h = 1/(norm(ML4)*sin(qhat))*(cos(qhat)*i_ML4+i3);
        case 8
            qhat = 2*acos(sqrt(1-(Moon_D/(2*norm(ML4)))^2));
            h = -Moon_D/(norm(ML4)^2*sqrt(1-(Moon_D/(2*norm(ML4)))^2))*i_ML4;
        otherwise
            disp('type error')
    end
    
    % Compute w
    w  = P*h/(sig1^2 + h'*P*h);
    rhat =  rhat + w*(measurement-qhat);
    P = (eye(3) - w*h')*P;
    RL4 = rhat;
    
    rdata(:,i) = rhat-RL40;
end    

drhat = rhat - RL40
plot(rdata','r');

errorBox = sqrt(diag(P))