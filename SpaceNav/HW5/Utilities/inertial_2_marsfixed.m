function T=inertial_2_marsfixed(year, month, day, hour, minute, second)

global WDOT_MARS; %Mars rotation rate

J2000 = 2451545.0; %Epoch of J2000

% get the Julian Date
JD = getJD(year,month,day,hour, minute,second);

%Days past J2000 epoch
D = JD - J2000;

Wmars = 176.630 + WDOT_MARS*D;

w = Wmars*pi/180; % Convert to radians

T = [cos(w) sin(w) 0;
    -sin(w) cos(w) 0;
        0     0    1];