%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xnew = prop_state_rk45(xold,dt);

h=dt;
hh = h/2;
h6 = h/6;

y = xold;
dydx = diffeq(y);
yt = y + hh*dydx;

dyt = diffeq(yt);
yt = y +hh*dyt; 

dym = diffeq(yt);
yt = y +h*dym; 
dym = dyt + dym;

dyt = diffeq(yt);
yout = y + h6*(dydx+dyt+2*dym);
xnew = yout; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%