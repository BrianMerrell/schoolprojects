function T=inertial_2_NED(r)

iz = [0 0 1]';
id = r/norm(r);
E = cross(id, iz); ie = E/norm(E);
in = cross(ie, id);

T = [ in' 
      ie'
      id'];
  