%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function xdot = diffeq(x);
ro_I = x(1:3,1);

%Differential equations (xdot) go here
global MU Q;
xdot(1:3,1) = x(4:6,1);
xdot(4:6,1) = -MU*ro_I/norm(ro_I)^3; %+ randn(3,1)*Q;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%