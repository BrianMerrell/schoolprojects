function [m, h] = estimate_measurement(type, t, x)
%Input: type=measurement type, t = time in second from epoch
%        x = state

%This function returns the measurement, m, and the 
% measurement geometry vector, h.
global YEAR MONTH DAY HOUR MINUTE SECOND
global WDOT_MARS

% syms T11 T12 T13 T21 T22 T23 T31 T32 T33 vx vy vz xo yo zo xb yb zb w real
% 
% T = [T11 T12 T13; T21 T22 T23; T31 T32 T33];
% ro = [xo; yo; zo];
% rb = [xb; yb; zb];
% v = [vx; vy; vz];
% w_vect = [0;0;w];
% 
% range = (v - cross(w_vect,T*rb))'*(ro-T*rb)/norm(ro-T*rb);
% 
% d_dro = jacobian(range,ro);
% d_dvo = jacobian(range,v);
% d_drb = jacobian(range,rb);


ri = x(1:3,1); %inertial position of orbiter
vi = x(4:6,1); %inertial velocity of orbiter
rb_mf = x(7:9,1); %mars-fixed position of beacon

T = inertial_2_marsfixed(YEAR,MONTH,DAY,HOUR,MINUTE,SECOND +t);
T = T';
rb = T*rb_mf;
w = WDOT_MARS*pi/180/(3600*24);
w_vect = [0;0;w];
ir = (ri - rb)/norm(ri - rb);

% T11 = T(1,1);
% T12 = T(2,1);
% T13 = T(3,1);
% T21 = T(2,1);
% T22 = T(2,2);
% T23 = T(2,3);
% T31 = T(3,1);
% T32 = T(3,2);
% T33 = T(3,3);
% 
% xo = x(1);
% yo = x(2);
% zo = x(3);
% vx = x(4);
% vy = x(5);
% vz = x(6);
% xb = x(7);
% yb = x(8);
% zb = x(9);


if (type == 1 ) % range measurement 
    display('Not curently using range measurement')
elseif (type == 2) % range-rate measurement
    
    %code for m=range-rate goes here
%     m = (vi - X(w_vect)*rb)'*(ri-rb)/norm(ri-rb); %A'*B/norm(B);
    
    %code for partial of measurement wrt state goes here
    dr_dvo = ir';   
    dr_dro = (vi - X(w_vect)*rb)'/norm(ri-rb) + (vi - X(w_vect)*rb)'*(ri - rb)/norm(ri-rb)^2*-ir';
    dr_drb = ((ri-rb)'*(-X(w_vect)*T) + (vi - X(w_vect)*rb)'*-T)/norm(ri-rb) + ((vi-X(w_vect)*rb)'*(ri-rb)/norm(ri-rb)^2)*ir'*T;

    
    h = [dr_dro dr_dvo dr_drb];
    m = h*x;
%     h = [double(subs(d_dro)) double(subs(d_dvo)) double(subs(d_drb))];
else
    display('BAD Measurement Type')
end



