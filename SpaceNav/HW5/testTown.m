syms T11 T12 T13 T21 T22 T23 T31 T32 T33 vx vy vz xo yo zo xb yb zb w real

T = [T11 T12 T13; T21 T22 T23; T31 T32 T33];
ro = [xo; yo; zo];
rb = [xb; yb; zb];
v = [vx; vy; vz];
w_vect = [0;0;w];

range = (v - cross(w_vect,T*rb))'*(ro-T*rb)/norm(ro-T*rb);

d_dro = jacobian(range,ro);
d_dvo = jacobian(range,v);
d_drb = jacobian(range,rb);


