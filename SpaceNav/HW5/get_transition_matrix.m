function phi = get_transition_matrix(x,dt);

ro_I = x(1:3,1);
%Compute the transition matrix
global MU;
I9 = eye(9);
var = (3*MU*ro_I*(ro_I/norm(ro_I))' - MU*norm(ro_I)*eye(3))/norm(ro_I)^4;

F = [zeros(3), eye(3), zeros(3);
    var, zeros(3,6); zeros(3,9)];

phi = I9 + F*dt + F*F*dt*dt/2;
