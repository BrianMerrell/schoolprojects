%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [xnew, Pnew]= nav_prop(x,P,Qd,dt);

%Inputs: x=state, P=state covariance, Qd=covariance of proces noise(discrete),
%        dt = delta-time

%First propagate the spacecraft position and velocity (state 1-6)
xnew(1:6,1) = prop_state_rk45(x(1:6,1),dt);

%Now propagate the beacon state (states 7-9)
xnew(7:9,1) = x(7:9,1);

%Next, compute the state transition matrix at the midpoint
xmidpoint = (x + xnew)/2;
phi = get_transition_matrix(xmidpoint, dt);

Pnew = phi*P*phi' + Qd;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%