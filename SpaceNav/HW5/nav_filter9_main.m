%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%PUT ALL CONSTANT DATA AND GLOBAL DATA HERE
%
global MU WDOT_MARS Q;
global YEAR MONTH DAY HOUR MINUTE SECOND;
%
MU = 4.2828376212e13;   % m^3/s^2
%Epoch
YEAR=2009; MONTH=2; DAY=20; HOUR=2; MINUTE=12; SECOND=45;
%
Q = eye(3,3)*1.0e-10; % m^2/s^3 process noise strength
MIN_ELEV = 10*pi/180; %radians
WDOT_MARS = 350.89198226; % deg/day
R_Rangedot = 0.01^2; % range-rate measurement variance (m^2/s^2)
%
Pos_Var = 10^2; %Initial spacecraft position variance (m^2)
Vel_Var = 0.01^2; %Initial spacecraft velocity variance (m^2/s^2)
Pos_B_Var = 10^2; %Initial beacon position variance (m^2)
%
dt = 10; % sec
tfinal = 30000; %sec 

addpath Utilities
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Put All Filter Initial Conditions Here

%Initialize spacecraft position and velocity
a= 4008123; % semimajor axis (m)
e = 0.0011; % eccentricity
inc = 10.89*pi/180;
W =  56.12*pi/180; %Longitude of Ascending Node (rad)
w = 10.71*pi/180; % Argument of perigee (rad)
nu = 30.23*pi/180; % true anomally (rad)
[R_filter,V_filter]=orbel2rv(a,e,inc,W,w,nu,MU); % m, m/s

%Initialize beacon location
r_beacon_filter = 3396842; % m
lat_beacon_filter = 0.034*pi/180; % latitude of radians
long_beacon_filter = 0.121*pi/180; % longitude of beacon
Rbeacon_filter = r_beacon_filter*...
                [cos(long_beacon_filter)*cos(lat_beacon_filter)
                 sin(long_beacon_filter)*cos(lat_beacon_filter)
                 sin(lat_beacon_filter)];
             
%Load up initial filter state
x_filter = [R_filter' V_filter' Rbeacon_filter']';

%Initialize covariance
P=zeros(9,9);
% Problem 3
% P = diag([100^2, 100^2, 100^2, .1^2, .1^2, .1^2, 5000^2, 5000^2, 5000^2]);
% Problem 4
% P = diag([2000^2, 2000^2, 2000^2, 2^2, 2^2, 2^2, 10^2, 10^2, 10^2]);
% Problem 5
P = diag([1000^2 1000^2 1000^2 1 1 1 1000^2 1000^2 1000^2]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tsav=0:dt:tfinal;
Npts = length(tsav);

% Allocate memory for data saved for plotting
rd_res = zeros(Npts,1); %rdot residual
rd_res_sig = zeros(Npts,1); %residual stdev

rb_ned_sig = zeros(3,Npts); %Beacon pos stdev in NED coordinates
r_ned_sig = zeros(3,Npts); %Orbiter pos stdev in NED coordinates
r_i_sig = zeros(3,Npts); %Orbiter pos stdev in inerital coordinates
r_lvlh_sig = zeros(3,Npts); %Orbiter pos stdev in LVLH coordinates

B = [zeros(3); eye(3); zeros(3)];

rdot_data = load('rdot_data5');

i=1;
while (tsav(i) < tfinal)
    %STEP 1: Propagate filter state and state covariance matrix
    t = tsav(i);
    %First compute Qd
    Qd = B*Q*B'*dt;
    %Now propagate
    [x_filter,P] = nav_prop(x_filter,P,Qd,dt);
    t = tsav(i+1);
    disp(t/60)
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %STEP 2: Update filter state and state covariance matrix
    %
    % First get measurement
    rdotm = rdot_data(i,2);
    % Now update state and state covariance and get residual information
    rdot_res =0; rdot_V=0;
    if (rdotm ~= 0)
        [x_filter,P, rdot_res, rdot_V] = nav_update(t, x_filter, P, 2, rdotm, R_Rangedot);
    end
    
    %Save important variables for plotting
    rd_res(i) = rdot_res;
    rd_res_sig(i) = sqrt(rdot_V);
    
    % You should be able to understand the code below
    % These calculations are needed to plot the variables of interest
    T_i_2_mf = ...
        inertial_2_marsfixed(YEAR,MONTH,DAY,HOUR,MINUTE,SECOND+t);
    rb_i = T_i_2_mf'*x_filter(7:9,1);
    T_i_2_ned = inertial_2_NED(rb_i);
    T_mf_2_ned = T_i_2_ned*T_i_2_mf';
    rb_ned_sig(:,i) = sqrt(diag(T_mf_2_ned*P(7:9,7:9)*T_mf_2_ned'));
    %
    T_i_2_lvlh = inertial_2_LVLH(x_filter(1:3),x_filter(4:6));
    T_i_2_ned = inertial_2_NED(x_filter(1:3));
    %
    r_ned_sig(:,i) = sqrt(diag(T_i_2_ned*P(1:3,1:3)*T_i_2_ned'));
    r_i_sig(:,i) = sqrt(diag(P(1:3,1:3)));
    r_lvlh_sig(:,i) = sqrt(diag(T_i_2_lvlh*P(1:3,1:3)*T_i_2_lvlh'));
    
i=i+1;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
