clc
close all

r = [0;7;0];
v = [7;0;0];

h = cross(r,v);

ir = r/norm(r);

ih = h/norm(h);

ix = cross(ih,ir);

T = [ix';ih';ir'];

P = [25 0 0; 0 25 0; 0 0 25];

P_ = T*P*T'