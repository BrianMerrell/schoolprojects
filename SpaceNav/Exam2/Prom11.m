clc
close all

I = eye(3);
dt = 1;

phi = [I I*dt; zeros(3) I];
Q = eye(3);
P = diag([1 1 1 1 1 1]);

A = [zeros(3) I; zeros(3,6)];
B = [zeros(3); I];

P1 = phi*P*phi' + B*Q*B'*dt

P = P + A*P*dt + P*A'*dt + Q*dt
