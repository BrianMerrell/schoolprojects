clc
clear all
clearvars all

syms x y z n1 n2 n3 real


r = [x; y; z];
i = r/norm(r);
n = [n1; n2; n3];

a1 = (r'*n)*n;
a2 = 3*r;
a3 = (i'*n);
jacobian(a3,r)

