clc
clear all
clearvars all



r = [200000; 0; 0];

P = diag([100^2, 100^2, 100^2]);

Q = 10*eye(3);
dt = 3000;

A = [0 0 0; 0 0 0; 0 0 0];
B = eye(3);

phi = eye(3) + A*dt + A^2*dt^2/2;

% for i = 1:3000
%     P = phi*P*phi' + B*Q*B'
% end

% P = phi*P*phi' + B*Q*B'*3000

P = P + Q*dt