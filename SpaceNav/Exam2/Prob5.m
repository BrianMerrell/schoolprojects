r = [200000; 0; 0];

% P = diag([200^2, 200^2, 200^2]);

z = 0.05994;
R = 1e-10;

r_earth = 6000;
d = r_earth*2;

measurement = r_earth/sin(z/2);
h = d/(norm(r)^2*sqrt(1-(d/(2*norm(r)))^2))*[1 0 0];

zhat = h*r;

residual = h*P*h'+R;

K = P*h'*(residual)^-1;

r_new = r + K*(z - zhat)

P = (eye(3) - K*h)*P

