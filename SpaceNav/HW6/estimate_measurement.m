function [m, h] = estimate_measurement(type, t, x)
%Input: type=measurement type, t = time in second from epoch
%        x = state

%This function returns the measurement, m, and the 
% measurement geometry vector, h.
global YEAR MONTH DAY HOUR MINUTE SECOND
global WDOT_MARS

ri = x(1:3,1); %inertial position of orbiter
vi = x(4:6,1); %inertial velocity of orbiter
rb_mf = x(7:9,1); %mars-fixed position of beacon

bias = x(11,1); %measurement bias

T = inertial_2_marsfixed(YEAR,MONTH,DAY,HOUR,MINUTE,SECOND +t);
T = T';
rb = T*rb_mf;
w = WDOT_MARS*pi/180/(3600*24);
w_vect = [0;0;w];
ir = (ri - rb)/norm(ri - rb);



if (type == 1 ) % range measurement 
    display('Not curently using range measurement')
elseif (type == 2) % range-rate measurement
    
    %code for m=range-rate goes here
    m = (vi - X(w_vect)*rb)'*(ri-rb)/norm(ri-rb) + bias; 
    
    %code for partial of measurement wrt state goes here    
    dr_dro = (vi - X(w_vect)*rb)'/norm(ri-rb) + (vi - X(w_vect)*rb)'*(ri - rb)/norm(ri-rb)^2*-ir';
    dr_dvo = ir';
    dr_drb = ((ri-rb)'*(-X(w_vect)*T) + (vi - X(w_vect)*rb)'*-T)/norm(ri-rb) + ((vi-X(w_vect)*rb)'*(ri-rb)/norm(ri-rb)^2)*ir'*T;
    dr_dJ2 = 0;
    dr_dsigb = 1;
    
    h = [dr_dro dr_dvo dr_drb dr_dJ2 dr_dsigb];
    %m = h*x; %Proves the correct h
else
    display('BAD Measurement Type')
end



