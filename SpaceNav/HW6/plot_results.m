close all
%%%%%%%%%%%%%%%% SECOND FIGURE %%%%%%%%%%%%%%%%%
%Orbiter position uncertainty in inertial and LVLH
figure
subplot(211)
plot(tsav/60, +3*r_i_sig)
hold
plot(tsav/60, -3*r_i_sig)
grid
xlabel('Time (min)')
ylabel('Pos Uncertainty 3-\sigma (m)')
title('Orbiter Position Uncertainty (INERTIAL)')
legend('X_i','Y_i','Z_i');

subplot(212)
plot(tsav/60, +3*r_lvlh_sig)
hold
plot(tsav/60, -3*r_lvlh_sig)
grid
xlabel('Time (min)')
ylabel('Pos Uncertainty 3-\sigma (m)')
title('Orbiter Position Uncertainty (LVLH)')
legend('Downrange','Crosstrack','Radial');
%%%%%%%%%%%%% END SECOND FIGURE %%%%%%%%%%%%%%%%

%%%%%%%%%%%%%% THIRD FIGURE %%%%%%%%%%%%%%%%%%
% Orbiter and Beacon position error in NED
figure
subplot(211)
plot(tsav/60, +3*r_ned_sig)
hold
plot(tsav/60, -3*r_ned_sig)
grid
xlabel('Time (min)')
ylabel('Pos Uncertainty 3-\sigma (m)')
title('Orbiter Position Uncertainty (NED)')
legend('N','E','D');

subplot(212)
plot(tsav/60, +3*rb_ned_sig)
hold
plot(tsav/60, -3*rb_ned_sig)
grid
xlabel('Time (min)')
ylabel('Pos Uncertainty 3-\sigma (m)')
title('Beacon Position Uncertainty (NED)')
legend('N','E','D');

%%%%%%%%%%%%%% END THIRD FIGURE %%%%%%%%%%%%%%%

%%%%%%%%%%%%%% FIRST FIGURE %%%%%%%%%%%%%%%%%%
% Residual and residual covariance

figure
plot(tsav/60,-3*rd_res_sig,'k')
hold
plot(tsav/60,+3*rd_res_sig,'k')
plot(tsav/60,rd_res)
grid
axis([0 max(tsav/60)  -0.05 +0.05])
xlabel('Time (min)')
ylabel('Residual 3-\sigma (m)')
title('Measurement Residual')
%%%%%%%%%%%%%% END FIRST FIGURE %%%%%%%%%%%%%%%%