close all

I = eye(3,3);
mu = 9.0072e12;
J2 = 0.01;
Req = 2575*10^3;
n = [0;0;1];
in = n/norm(n);

g = @(rvect)(-mu*rvect/norm(rvect)^3-(mu*J2*Req^2)/(2*norm(rvect)^5)*(6*dot(rvect,n)*n+3*rvect-15*dot(rvect/norm(rvect),n)^2*rvect));

k = (mu*J2*Req^2)/2;
P = @(rvect)(6*dot(rvect,n)*n + 3*rvect - 15*dot((rvect/norm(rvect)),n)^2*rvect);
dgdr = @(rvect)(-mu/norm(rvect)^3 *(I-3*(rvect/norm(rvect))*(rvect/norm(rvect))') + 5*k/norm(rvect)^6*P(rvect)*(rvect/norm(rvect))'-k/norm(rvect)^5*(6*n*n'+3*I-15*dot((rvect/norm(rvect)),n)^2*I-30*rvect*dot((rvect/norm(rvect)),in)*n'*(1/norm(rvect)*(I-(rvect/norm(rvect))*(rvect/norm(rvect))'))));

dx0 = 1.0e-9;
dxf = 100000;

dx = dx0;
i = 1;
drx = [dx 0 0]';
dry = [0 dx 0]';
drz = [0 0 dx]';

Gn = cell(1,15);
error = zeros(1,15);
del = zeros(1,15);
rvect = [2000 2000 2000]'*10^3;
Ga = dgdr(rvect)
while(dx<=dxf)
   temp1 = (g(rvect+drx)-g(rvect))/drx;
   temp2 = (g(rvect+dry)-g(rvect))/dry;
   temp3 = (g(rvect+drz)-g(rvect))/drz;
   
   Gn{1,i} = temp1+temp2+temp3;
   
   error(i) = max(max(abs((Ga-Gn{1,i})./Ga)))*100;
   del(i) = dx;
   
   drx = drx*10;
   dry = dry*10;
   drz = drz*10;
   dx = dx*10;
   i = i+1;
end

loglog(del,error, '-o');
grid on
title('Numerical vs Analytical Derivative')
xlabel('Step Size (m)')
ylabel('Error Percentage')
