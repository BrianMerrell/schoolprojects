%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function xdot = diffeq(x,J2)
ro_I_vect = x(1:3,1);
ro_I = norm(ro_I_vect);
global MU REQ;
I = eye(3,3);
n = [0;0;1];
in = n/norm(n);

k = (MU*J2*REQ^2)/2;
P = @(rvect)(6*dot(rvect,n)*n + 3*rvect - 15*dot((rvect/norm(rvect)),n)^2*rvect);

%Differential equations (xdot) go here
g = @(rvect)(-MU*rvect/norm(rvect)^3-(MU*J2*REQ^2)/(2*norm(rvect)^5)*(6*dot(rvect,n)*n+3*rvect-15*dot(rvect/norm(rvect),n)^2*rvect));

xdot(1:3,1) = x(4:6,1);
xdot(4:6,1) = g(ro_I_vect);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%