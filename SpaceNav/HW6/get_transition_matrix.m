function phi = get_transition_matrix(x,dt)

%Compute the transition matrix
global MU REQ;

ro_I_vect = x(1:3);
ro_I = norm(ro_I_vect);
iro_I = ro_I_vect/ro_I;
n = [0;0;1];


J2 = x(10);

I=eye(3,3);
in = n/norm(n);
k = (MU*J2*REQ^2)/2;
% P=6*dot(ro_I_vect, n)*n+3*ro_I_vect-15*dot(iro_I,n)^2*ro_I_vect;
% m = -MU/ro_I^3*(I-3*iro_I*iro_I')+5*k/ro_I^6*P*iro_I'-k/ro_I^5*(6*n*n'+3*I-15*dot(iro_I,n)^2*I-30*ro_I_vect*dot(iro_I,in)*n'*(1/ro_I*(I-iro_I*iro_I')));

P = @(rvect)(6*dot(rvect,n)*n + 3*rvect - 15*dot((rvect/norm(rvect)),n)^2*rvect);
dgdr = @(rvect)(-MU/norm(rvect)^3 *(I-3*(rvect/norm(rvect))*(rvect/norm(rvect))') + 5*k/norm(rvect)^6*P(rvect)*(rvect/norm(rvect))'-k/norm(rvect)^5*(6*n*n'+3*I-15*dot((rvect/norm(rvect)),n)^2*I-30*rvect*dot((rvect/norm(rvect)),in)*n'*(1/norm(rvect)*(I-(rvect/norm(rvect))*(rvect/norm(rvect))'))));

df_dJ2 = @(rvect)(-(MU*REQ^2)/(2*norm(rvect)^5)*(6*dot(rvect,n)*n+3*rvect-15*dot(rvect/norm(rvect),n)^2*rvect));


O3 = zeros(3,3);
I3 = eye(3,3);

Ftemp = [O3 I3 O3; dgdr(ro_I_vect) O3 O3; O3 O3 O3];
Ftemp2 = [zeros(3,2); df_dJ2(ro_I_vect) zeros(3,1); zeros(3,2)];

F = [Ftemp Ftemp2;zeros(2,11)];
phi = eye(11,11) + F*dt + F*F*dt*dt/2;
