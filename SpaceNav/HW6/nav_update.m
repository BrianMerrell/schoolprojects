%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [xnew, Pnew, residual,V] = nav_update(t, x, P, type, m, R)

%inputs: t=time, x=state, P=state covariance, type=measurement type,
%        m=measurement, R=measurement variance
%
%estimate measuremnts and get measurement partials (geometry vectors)
%based on current estimate of state
[m_est, h] = estimate_measurement(type, t, x);

%Compute the residual
residual = m-m_est;

%Compute the Kalman gain
W = P*h'*inv(h*P*h'+R);

%Update the state
xnew = x+W*(residual);

%Update the covariance matrix using Joseph formulation
I = eye(11); %there are 9 states
Pnew = (I-W*h)*P*(I-W*h)'+W*R*W';

%Symmetrize covariance matrix (just in case)
Pnew = (Pnew+Pnew')/2.0;

%Compute the new residual and residual variance for plotting only
[m_est, h] = estimate_measurement(type, t, xnew);
residual = m - m_est;
V = h*Pnew*h' + R;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%