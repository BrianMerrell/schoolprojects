function T=inertial_2_LVLH(r,v)

ir = r/norm(r);
h = cross(r, v); ih = h/norm(h);
ix = cross(ih, ir);

T = [ ix' 
      ih'
      ir'];
  