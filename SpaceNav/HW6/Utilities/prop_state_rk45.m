%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xnew = prop_state_rk45(xold,dt,J2);

h=dt;
hh = h/2;
h6 = h/6;

y = xold;
dydx = diffeq(y,J2);
yt = y + hh*dydx;

dyt = diffeq(yt,J2);
yt = y +hh*dyt; 

dym = diffeq(yt,J2);
yt = y +h*dym; 
dym = dyt + dym;

dyt = diffeq(yt,J2);
yout = y + h6*(dydx+dyt+2*dym);
xnew = yout; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%